#include "getExecutionTime.h"

vector<char> tables(512, '.');
vector<vector<char> > datas;
unordered_map<string, char> NineCharsToOneChar;

//this must pass in value, want to keep the original data.;
int dotHashStr(string s2)
{
    for (auto& a : s2)
        a = (a == '.') ? '0' : '1';

    int i= stoi(s2, nullptr, 2);
    return i;
}

void applyImageEnhance(char infinitChar)
{
    //added one rows/column on all the sides,
    vector<vector<char> > datas2(datas.size()+2,
        vector<char>(datas[0].size()+2, infinitChar));

    // the 2nd layer will be processed. the outter most no need. see. it start from 1 instead of 0.
    for (int j = 1; j < datas.size() - 1; j++)
    {
        for (int i = 1; i < datas[0].size() - 1; i++)
        {
            string s("");
            s += datas[j - 1][i - 1];
            s += datas[j - 1][i];
            s += datas[j - 1][i + 1];
            s += datas[j ][i - 1];
            s += datas[j][i ];
            s += datas[j ][i + 1];
            s += datas[j + 1][i - 1];
            s += datas[j + 1][i];
            s += datas[j + 1][i + 1];
            if (NineCharsToOneChar.find(s) != NineCharsToOneChar.end())
            {
                datas2[j + 1][i + 1] = NineCharsToOneChar[s];
            }
            else
            {
                NineCharsToOneChar[s] = tables[dotHashStr(s)];
                datas2[j + 1][i + 1] = NineCharsToOneChar[s];
            }
        }
    }

    datas = std::move(datas2);
}

int main(int argc, char *argv[])
{
    {
        cGetTime t;
        std::ifstream inf(argv[1]);
        string s1;
        inf >> s1;
        for (int i=0; i < s1.size(); i++)
            tables[i] = s1[i];

        //tables[0]
        string s;
        bool firstRead = true;

        while (inf >> s && s != "")
        {
            if (firstRead)
            {   //added two rows/column on all the sides, the 2nd layer will be processed. the outter most no need.
                datas.push_back(vector<char>(s.size() + 4, '.'));
                datas.push_back(vector<char>(datas[0].size(), '.'));
                firstRead = false;
            }
            datas.push_back(vector<char>(datas[0].size(), '.'));
            for (int i = 2; i < s.size()+2; i++)
                datas.back()[i] = s[i-2];
        }
        datas.push_back(vector<char>(datas[0].size(), '.'));
        datas.push_back(vector<char>(datas[0].size(), '.'));

        int runs = 50;
        for (int i = 0; i < runs; i++)
        {
            //wasted more than 3-5 hours on this.
            //the all ........ may become #, or . 
            //and the all ########## may become all # or . //most likely will be . or else resulting lit is infinite.
            char c;
            if (i % 2 == 0)
                c = tables[0];
            else
                c = tables[511];

            applyImageEnhance(c);
        }

        int count = 0;
        for (int j= 0;j < datas.size(); j++)
        {
            for (int i = 0; i < datas[0].size(); i++)
            {
                if (datas[j][i] == '#')
                    count++;
            }
        }
        cout << " count: " << count << endl;
    }

    return 0;
}
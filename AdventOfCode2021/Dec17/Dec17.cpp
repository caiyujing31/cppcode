#include "getExecutionTime.h"

pair<int, int> xRange;
pair<int, int> yRange;
pair<int, int> xSpeedRange;

void getRange(const string& s)
{
    size_t posX1 = s.find('=');
    size_t posX2 = s.find(',', posX1);

    stringstream ssX(s.substr(posX1+1, posX2));
    char c1, c2;
    ssX >> xRange.first >> c1 >> c2 >> xRange.second;

    size_t posY1 = s.find('=',posX2);
    size_t posY2 = s.find(',', posY1);
    stringstream ssY(s.substr(posY1+1, posY2));
    ssY >> yRange.first >> c1 >> c2 >> yRange.second;
}

void getXSpeedRange()
{
    int distance = 0;
    int speed = 0;

    distance = (speed+1)*(speed + 0) / 2;
    //from < to >= is the minimum speed required to reach the target's x range.
    while (distance < xRange.first)
    {
        speed++;
        distance = (speed + 1)*(speed + 0) / 2;
    }
    xSpeedRange.first = speed;
    xSpeedRange.second = xRange.second;
}

int getMaxHeightWhenHitTarget(pair<int, int> s)
{
    pair<int, int> pos(0, 0);
    int maxH=0;
    while (true)
    {
        pos.first += s.first;

        if (s.first > 0)
            s.first--;
      
        if (s.second > 0)
            maxH += s.second;

        pos.second += s.second;
        s.second--;

        if ((pos.first <= xRange.second && pos.first >= xRange.first) &&
            (pos.second <= yRange.second && pos.second >= yRange.first))
            return maxH;

        if (pos.second < yRange.first)
            return -1;
    }
    
    return -1;
}

int main(int argc, char *argv[])
{
    {
        cGetTime t;
        std::ifstream inf(argv[1]);
        string s;
        if (getline(inf,s))
            getRange(s);        

        getXSpeedRange();

        //Part 1:
        pair<int, int> speed(xSpeedRange.first, abs(yRange.first)-1);
        cout << getMaxHeightWhenHitTarget(speed) <<endl;

        //Part 2:
        vector<pair<int, int> > results;
        for (int i = xSpeedRange.first; i <= xSpeedRange.second; i++)
        {
            // how negative (y initial speed) can go, it can the as negative as the yRange.first . 
            // So one step, the y Postion is on last row of target area, and the x's speed shall be between xrange.first to xrange.second.

            // How positive (y initial speed)  can go, 
            // whenever y's speed drop to -1 * initial speed (+), the CurrPos of the probe will be on the horizontal (y=0) line, 
            // then the next step , it will decreased by depth of -1* (initial speed +1); so as long as -1* (initial speed +1) is below the y.first.
            // so got to make sure -1* (initial speed +1) is not beyond the y.first.

            for (int j = yRange.first; j <= abs(yRange.first); j++)
            {
                pair<int, int> speed(i,j);
                if (getMaxHeightWhenHitTarget(speed) != -1)
                    results.emplace_back(i, j);
            }
        }

        cout << results.size() << endl;

    }
    return 0;
}
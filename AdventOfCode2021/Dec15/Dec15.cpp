#include "getExecutionTime.h"
vector<vector<int> > datas;
int xFirst;
int xLast;
int yFirst;
int yLast;

struct KeyHasher
{
    std::size_t operator()(const pair<int, int>& k) const
    {
        return ((hash<int>()(10000+k.first) ^ (hash<int>()(10000+k.second) << 1)) >> 1);
    }
};

unordered_map<pair<int, int>, int, KeyHasher> cached;

int findMinSumAtPoint(int xIdx, int yIdx)
{
    pair<int, int> p(xIdx, yIdx);

    if (xIdx< xFirst || yIdx < yFirst)
    {
        //i , j can not be negative, they already got the value, before decrement 
        return 0;
    }

    if (cached.find(p) != cached.end())
    {
        return cached[p];
    }
    
    if (xIdx == xFirst && yIdx == yFirst)
    {
        cached[p] = datas[yIdx][xIdx];
        return cached[p];
    }
    else
    {   //left most column, can only compare to up
        if (xIdx == xFirst)
        {
            cached[p] = datas[yIdx][xIdx] + findMinSumAtPoint(xIdx, yIdx - 1);
            return cached[p];
        }
        else
        {
            //top row y=0, can only compare to left
            if (yIdx == yFirst)
            {
                cached[p] = datas[yIdx][xIdx] + findMinSumAtPoint(xIdx - 1, yIdx);
                return cached[p];
            }
            else
            {
                int up = findMinSumAtPoint(xIdx, yIdx -1);
                int left = findMinSumAtPoint(xIdx-1, yIdx);
                cached[p] = up < left ? up : left;
                cached[p] += datas[yIdx][xIdx];
                return cached[p];
            }
        }
    }

    return 0;
}

void grow(vector<vector<int> >& datas,int x)
{
    vector<vector<int> > org(datas);
    int xNum = datas[0].size();
    int yNum = datas.size();
    vector<vector<int> > res = vector<vector<int> >(yNum * x, vector<int>(xNum*x, -1));

    for (int right = 0; right < x; right++)
    {
        for (int down= 0; down < x; down++)
        {
            for (int j = 0; j < yNum; j++)
            {
                for (int i = 0; i < xNum; i++)
                {
                    res[down*yNum + j][right*xNum+i] = org[j][i] + down + right; //max add is 8.so will not wrap twice .
                }
            }
        }
    }

    for (int j = 0; j < res.size(); j++)
    {
        for (int i = 0; i < res.size(); i++)
        {
            res[j][i] = res[j][i] >= 10 ? res[j][i] - 9 : res[j][i];//max add is 8.so will not wrap twice .
        }
    }

    datas = std::move(res);
}


int main(int argc, char *argv[])
{
    {
        cGetTime t;
        std::ifstream inf(argv[1]);
        string s;
        int sz;

        bool isFirstLine = true;
        while (inf >> s && s!="")
        {
            stringstream ss(s);
            char c;
            if (isFirstLine)
            {
                sz = s.size();
                isFirstLine = false;
            }
            datas.push_back(vector<int>(sz,-1));
            int i = 0;
            auto& curr = datas.back();
            while (ss >> c)
            {
                curr[i]= c-'0';
                i++;
            }
        }

        grow(datas,5);
        xLast = datas[0].size() - 1;
        xFirst = 0;
        yLast = datas.size() - 1;
        yFirst = 0;

        int res = findMinSumAtPoint(xLast, yLast);
        //my answer is 2934, //correct answer is 2927
        cout << "results:" << res - datas[yFirst][xFirst]<< endl; 
    }
    return 0;
}
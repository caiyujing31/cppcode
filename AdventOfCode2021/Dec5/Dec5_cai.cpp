#include "getExecutionTime.h"

struct pointPair
{
    pair<int,int> p1;
    pair<int,int>  p2;
    pointPair(int x1, int y1, int x2, int y2) : p1(pair<int,int>(x1, y1)), p2(pair<int, int>(x2, y2))
    {}
};

int main(int argc, char *argv[])
{
    {
        cGetTime t;
        std::ifstream file(argv[1]);
        vector<pointPair> data;
        //string s;
        int x1, y1, x2, y2;
        char comma;
        char dash;
        char arrow;
        pair<int, int> MaxXY(0, 0);

        while (!file.eof())
        {
            if (file >> x1 >> comma >> y1 >> dash >> arrow>>x2 >>comma >>y2)
            {
                if (x1 > MaxXY.first || x2 > MaxXY.first)
                {
                    if (x1 >= x2)
                        MaxXY.first = x1; //dont really care if x2 is >MaxXY.second
                    else
                        MaxXY.first = x2;
                }

                if (y1 > MaxXY.second || y2 > MaxXY.second)
                {
                    if (y1 >= y2) 
                        MaxXY.second = y1; //dont really care if y2 is >MaxXY.second
                    else
                        MaxXY.second = y2;
                }

                data.emplace_back(x1, y1, x2, y2);
            }
        }

        vector<vector<int> > vv(MaxXY.second + 1, vector<int>(MaxXY.first + 1, 0));
        for (auto a : data)
        {
            if (a.p1.first ==a.p2.first)
            {
                int smaller =a.p1.second <a.p2.second ?a.p1.second :a.p2.second;
                int delta = abs(a.p1.second -a.p2.second);
                for (int i = smaller; i <= smaller + delta; i++)
                    vv[i][a.p1.first] += 1;
            }

            if (a.p1.second ==a.p2.second)
            {
                int smaller =a.p1.first <a.p2.first ?a.p1.first :a.p2.first;
                int delta = abs(a.p1.first-a.p2.first);
                for (int i = smaller; i <= smaller + delta; i++)
                    vv[a.p1.second][i] += 1;
            }

            if (a.p1.first !=a.p2.first 
                && a.p1.second !=a.p2.second)
            {
                float ratio = static_cast<float>(a.p1.first -a.p2.first) / (a.p1.second -a.p2.second);
                if (ratio == 1.0 || ratio == -1.0)
                {
                    int j =a.p1.second;
                    int i =a.p1.first;

                    //==1 , p1 could be right bottom of p2 
                    if (a.p1.first >a.p2.first && 
                       a.p1.second >a.p2.second)
                    {
                        for (; j >=a.p2.second; j--,i--) 
                        {
                            vv[j][i] += 1;
                        }
                    }

                    //==1,  p1 could be left top of  p2 
                    if (a.p1.first <a.p2.first &&
                       a.p1.second <a.p2.second)
                    {
                        for (; j <=a.p2.second; j++, i++) 
                        {
                            vv[j][i] += 1;
                        }
                    }

                    //== -1 , p1 could be left bottom of p2
                    if (a.p1.first <a.p2.first && 
                       a.p1.second >a.p2.second)
                    {
                        for (; j >=a.p2.second; j--, i++) 
                        {
                            vv[j][i] += 1;
                        }
                    }

                    //== -1, p1 could be right top of p2.
                    if (a.p1.first >a.p2.first && 
                       a.p1.second <a.p2.second)
                    {
                        for (; j <=a.p2.second; j++, i--)
                        {
                            vv[j][i] += 1;
                        }
                    }
                }
            }
        }

        int count = 0;
        for (auto& a : vv)
        {
            for (auto& b : a)
            {
                if (b >= 2)
                    count++;
            }
        }
        cout << count << endl;
    }

    return 0;
}
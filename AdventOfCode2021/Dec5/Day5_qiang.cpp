// Day5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cmath>
#include <vector>
#include <cstring>
#include <set>
#include <utility>
using namespace std;

void ProcessPoint(int, int);
void ProcessRange(int, int, int, int);

std::set<pair<int, int>> existingData;
std::set<pair<int, int>> overlapData;
int main()
{
	int x1, y1, x2, y2;

	char comma;
	char dash;
	char arrow;
	std::ifstream file("input5.txt");

	while (!file.eof())
	{
		if (file >> x1 >> comma >> y1 >> dash >> arrow >> x2 >> comma >> y2)
		{
			if (x1==x2 || y1==y2 || abs(x1 - x2) == abs(y1 - y2))
			{
				ProcessRange(x1,x2,y1,y2);
			}
		}
	}

	cout << overlapData.size();
}

void ProcessRange(int x1, int x2, int y1, int y2)
{
	int xStart = 0, xEnd = 0, yStart = 0, yEnd = 0;
	// Process vertical
	if (x1 == x2)
	{
		yStart = (y1 <= y2) ? y1 : y2;
		yEnd = (y1 <= y2) ? y2 : y1;
		for (int i = yStart; i <= yEnd; i++)
		{
			ProcessPoint(x1, i);
		}
	}
	// Process horizontal
	else if (y1 == y2)
	{
		xStart = (x1 <= x2) ? x1 : x2;
		xEnd = (x1 <= x2) ? x2 : x1;
		for (int i = xStart; i <= xEnd; i++)
		{
			ProcessPoint(i, y1);
		}
	}
	// Process diag
	else
	{
		int x = 0, y = 0;
		int xDir = (x2 < x1) ? -1 : 1;
		int yDir = (y2 < y1) ? -1 : 1;
		int length = abs(x1 - x2);
		for (int i = 0; i <= length; i++)
		{
			x = x1 + xDir * i;
			y = y1 + yDir * i;
			ProcessPoint(x, y);
		}
	}

}

void ProcessPoint(int x, int y)
{
	auto value = make_pair(x, y);
	// Points already overlap, do nothing
	if (overlapData.find(value) != overlapData.end())
	{
		return;
	}
	// Points overlap occurs, move the point into overlapData
	else if (existingData.find(value) != existingData.end())
	{
		existingData.erase(value);
		overlapData.insert(value);
	}
	// New points added into existingData
	else
	{
		existingData.insert(value);
	}
}

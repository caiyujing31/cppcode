#include "getExecutionTime.h"

int main(int argc, char *argv[])
{    //moby.txt in the command arguments
    cGetTime t;
    std::ifstream file(argv[1]);
    string oneLine;
    vector<int> v1;
    int i;
    string s;


    //int x=0;
    //int y=0;

    /*part1*/
    //while (file >> s && file >> i)
    //{
    //    if (s == "forward")
    //        x += i;

    //    if (s == "down")
    //        y += i;

    //    if (s == "up")
    //        y -= i;
    //}
    //cout << x * y;

    /*part2*/
    long long x=0;
    long long y=0;
    long long aim = 0;
    int lineNo = 1;
    while (file >> s && file >> i)
    {
        if (s == "forward")
        {
            x += i;
            y += static_cast<long long>(i) * aim;
        }

        if (s == "down")
            aim += i;

        if (s == "up")
            aim -= i;

        lineNo++;
    }

    if (!file.eof())
    {
        cout << "line " << lineNo << " have issue";
        return 0;
    }

    cout << x * y;
    return 0;
}
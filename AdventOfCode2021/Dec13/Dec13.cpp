#include "getExecutionTime.h"
bool pairCompare(pair<int, int>&p1, pair<int, int>& p2)
{
    if (p1.second < p2.second)
        return true;
    if (p1.second == p2.second&& p1.first < p2.first)
        return true;

    return false;
}

void folding(vector<pair<int, int> >& data, pair<char, int>& p)
{
    if (p.first == 'y')
    {
        for (auto& a : data)
        {
            if (a.second > p.second)
            {
                a.second = p.second - (a.second - p.second);
            }
        }
    }
    else
    {       
        for (auto& a : data)
        {
            if (a.first > p.second)
            {
                a.first = p.second - (a.first - p.second);
            }
        }
    }
    sort(data.begin(), data.end(),pairCompare);
    data.erase(unique(data.begin(), data.end()), data.end());
}

void drawTheCode(const vector<pair<int, int> >& data, char c)
{
    int ylast = 0;
    int xlast = 0;
    for (int i = 0; i < data.size(); i++)
    {
        if (data[i].second == ylast)
        {
            for (int k = xlast + 1; k < data[i].first; k++)
                cout << ' ';
            cout << c;
            xlast = data[i].first;
        }
        else
        {
            xlast = 0;
            cout << endl;
            for (int j = ylast + 1; j < data[i].second - ylast; j++)
                cout << endl;

            for (int k = xlast + 1; k < data[i].first; k++)
                cout << ' ';
            cout << c;
            ylast = data[i].second;
        }
    }

    cout << endl;
}

int main(int argc, char *argv[])
{
    {
        cGetTime t;
        std::ifstream inf(argv[1]);
        vector<pair<int, int> > data;
        vector<pair<char, int> > folds;
        string s;
        int x1, y1;
        char comma;
        while (inf >> s && s != "")
        {
            stringstream ss(s);
            if (ss >> x1 >> comma >> y1 )
            {
                data.emplace_back(x1, y1);
            }

            size_t pos = s.find('=');
            if (pos != string::npos)
            {
                string s1 = s.substr(pos + 1);
                int num = atoi(s1.c_str());
                folds.emplace_back(s[pos-1], num);
            }
        }

        for (int i = 0; i < folds.size(); i++)
        {
            folding(data, folds[i]);
            if(i==0) cout << data.size() << endl;
        }

        drawTheCode(data, '#');
    }
    return 0;
}
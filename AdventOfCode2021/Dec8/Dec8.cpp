#include "getExecutionTime.h"
void sortString(string& a)
{
    vector<char> v(7, '-');
    for (auto& b : a)
        v[b - 'a'] = b;

    a = "";
    for (auto& c : v)
        if (c != '-')
            a += c;
}
int findNumOfCommonChar(string& s1, string& s2)
{
    vector<int> v(7, 0);
    for (auto& a : s1)
        v[a - 'a'] += 1;
    for (auto& a : s2)
        v[a - 'a'] += 1;

    int count = 0;
    for (auto& a : v)
        if (a == 2) count++;
    return count;
}

int decode2(vector<string>& in, vector<string>& out)
{
    map<int, string> m1;
    sort(in.begin(), in.end(), [](string& l, string& r) {return l.size() < r.size(); });

    for (auto& a : in)
    {
        sortString(a);
    }

    for (auto& a : in)
    {
        if (a.size() == 2) m1[1] = a;
        else if (a.size() == 3) m1[7] = a;
        else if (a.size() == 4) m1[4] = a;
        else if (a.size() == 7) m1[8] = a;
        else if (a.size() == 5)
        {
            if(findNumOfCommonChar(a, m1[1]) ==m1[1].size()) 
                m1[3]=a;
            else 
                if (findNumOfCommonChar(a, m1[4]) == 3) 
                    m1[5]=a;
                else 
                    m1[2]=a;
        }
        else
        {
            if (findNumOfCommonChar(a, m1[4]) == m1[4].size()) 
                m1[9] = a;
            else 
                if (findNumOfCommonChar(a, m1[7]) == m1[7].size()) 
                    m1[0] =  a;
                else
                    m1[6] = a;
        }
    }
    map<string, int> m2;
    for (auto& a : m1)
        m2[a.second] = a.first;

    for (auto& a : out)
    {
        sortString(a);
    }

    return m2[out[0]]*1000+ m2[out[1]]*100+ m2[out[2]]*10 + m2[out[3]];
}

int main(int argc, char *argv[])
{
    {
        cGetTime t;
        std::ifstream inf(argv[1]);
        string s;
        string s2;
        vector<vector<string> > in;
        vector<vector<string> > out;
        while (std::getline(inf, s) && s != "")
        {
            stringstream ss(s);
            in.push_back(vector<string>());
            while (ss >> s2 && s2 != "|")
            {
                in.back().push_back(s2);
            }
            out.push_back(vector<string>());
            while (ss >> s2 && s2 != "")
            {
                out.back().push_back(s2);
            }
        }

        //int count = 0;
        //for (auto& a : out)
        //{
        //    for (auto&b : a)
        //    {
        //        int sz = b.size();
        //        if (sz == 2 || sz == 3 || sz == 4 || sz == 7)
        //            count++;
        //    }
        //}

        int count = 0;
        for (int i = 0; i < in.size(); i++)
        {
            count += decode2(in[i], out[i]);
        }

        cout << count << endl;
    }
    return 0;
}
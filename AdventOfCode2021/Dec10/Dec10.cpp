#include "getExecutionTime.h"

//part1
map<char, int> unMatchPoints = { 
    pair<char,int>(')',3),
    pair<char,int>(']',57),
    pair<char,int>('}',1197),
    pair<char,int>('>',25137) 
};

//part2;
map<char, int> matchPoints = {
    pair<char,int>(')',1),
    pair<char,int>(']',2),
    pair<char,int>('}',3),
    pair<char,int>('>',4)
};

map<char, char> matchChar ={
    pair<char,char>('(',')'),
    pair<char,char>('[',']'),
    pair<char,char>('{','}'),
    pair<char,char>('<','>')
};

bool isClosing(char c1) 
{
    if (c1 == ')' || c1 == ']' || c1 == '}' || c1 == '>')
        return true;

    return false;
}

long long processLine(const string& s)
{
    vector<char> q;
    for (auto& a : s)
    {
        if (!q.empty() && isClosing(a))
        {
            if (matchChar[q.back()] == a)
                q.pop_back();
            else
                return 0;
                //return unMatchPoints[a]; //part1: sum all in the main(), for incomplete, return 0;
        }
        else
        {
            q.push_back(a);
        }
    }

    long long res=0;
    for (int i = q.size()-1; i >= 0; i--)
    {
        res *= 5;
        res += matchPoints[matchChar[q[i]]];
    }

    return res;
}

int main(int argc, char *argv[])
{
    {
        cGetTime t;
        std::ifstream inf(argv[1]);
        string s;
        long long count = 0;
        vector<long long> res;
        while (inf >>s && s!="")
        {
            count = processLine(s);
            if (count != 0)
                res.push_back(count);
        }

        sort(res.begin(), res.end());
        int sz = res.size();
        if (sz % 2 == 1)
        {
            cout << res[sz / 2] << endl;
        }
    }
    return 0;
}
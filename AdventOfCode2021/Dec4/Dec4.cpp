#include "getExecutionTime.h"

const int TableSize = 5;

struct setFive {
    vector<vector<int> > h;
    vector<vector<int> > v;
    setFive() 
    {
        h = vector<vector<int> >(TableSize, vector<int>());
        for (auto& a : h)
        {
            a = vector<int>(TableSize, -1);
        }
        v = vector<vector<int> >(TableSize, vector<int>());
        for (auto& a : v)
        {
            a = vector<int>(TableSize, -1);
        }
    }

    bool RemoveAndCheck(int r) {
        for (auto& a : h)
        {
            auto it2=remove(a.begin(), a.end(), r);
            a.erase(it2, a.end());
        }
        for (auto& a : v)
        {
            auto it2 = remove(a.begin(), a.end(), r);
            a.erase(it2, a.end());
        }

        for (auto& a : h)
        {
            if (a.size() == 0)
                return true; 
        }

        for (auto& a : v)
        {
            if (a.size() == 0)
                return true;
        }

        return false;
    }

    int calculateResult()
    {
        int sumRemaining = 0;
        for (auto& a : h)
        {
            for (auto& b : a)
                sumRemaining += b;
        }
        return sumRemaining;
    }
};

int main(int argc, char *argv[])
{
    {
        cGetTime t;
        std::ifstream file(argv[1]);

        string drawData;
        file >> drawData;

        string s;
        setFive* tmp=nullptr;
        vector<setFive*> allData;
        while (!file.eof())
        {
            bool pushTable = false;
            
            int row=0;
            while (std::getline(file, s) && s!="")
            {
                if (!pushTable)
                {
                    allData.push_back(new setFive());
                    tmp = allData.back();
                    pushTable = true;
                }

                int i;
                int col = 0;
                stringstream ss(s);
                while (ss >> i)
                {
                    tmp->h[row][col]= i;
                    tmp->v[col][row] = i;
                    col++;
                }
                row++;
            }
            pushTable = false;
        }

        stringstream ss2(drawData);
        int r;
        char c;
        bool done = false;
        while (ss2 >> r && !done)
        {
            /*part2*/
            for (auto it3=allData.begin(); it3 != allData.end();) 
            {
                if ((*it3)->RemoveAndCheck(r))
                {
                    if (allData.size() == 1)
                    {
                        cout << r * (*it3)->calculateResult() << endl;
                        done = true;
                    }
                    delete *it3;
                    it3 = allData.erase(it3);
                }
                else
                {
                    it3++;
                }
            }

            /*part1*/
            //for (auto a : allData)
            //{
            //    if (a->removeItem(r))
            //    {
            //        cout << r * (a->calculateResult()) <<endl;
            //        return 0;
            //    }
            //}
            ss2 >> c;
        }

        /*part1*/
        //for (auto a : allData)
        //{
        //    delete a;
        //}
    }

    return 0;




}
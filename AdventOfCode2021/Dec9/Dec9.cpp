#include "getExecutionTime.h"

vector<vector<int> > data1;
int ylast;
int xlast;
int findSizeOfBasin(int x, int y)
{
    if (x <= 0 || x >= xlast - 1)
        return 0;

    if (y <= 0 || y >= ylast - 1)
        return 0;

    if (data1[y][x] == -1 || data1[y][x] == 9)
        return 0;

    data1[y][x] = -1;//mark as counted;
    return 1 + findSizeOfBasin(x - 1, y) +
        findSizeOfBasin(x + 1, y) +
        findSizeOfBasin( x, y - 1) +
        findSizeOfBasin( x, y + 1);
}

int findTopThreeSizesOfBasins(vector<pair<int, int> >& lowPts)
{
    vector<int> sizes;
    for (int i = 0; i < lowPts.size(); i++)
    {
        int sz=findSizeOfBasin(lowPts[i].first, lowPts[i].second);
        sizes.push_back(sz);
    }

    sort(sizes.begin(), sizes.end(), greater<int>());
    return sizes[0]* sizes[1]* sizes[2];
}


int findLowPoints(vector<pair<int, int> >& lowPts)
{
    int count = 0;
    for (int j = 1; j < ylast - 1; j++)
    {
        for(int i = 1; i < xlast - 1; i++)
        {
            int value = data1[j][i];
            if (value < data1[j][i - 1] &&
                value < data1[j][i + 1] &&
                value < data1[j - 1][i] &&
                value < data1[j + 1][i])
            {
                lowPts.emplace_back(i, j);
                count += value + 1;
            }
        }
    }

    return count;

}

void stringToIntV(const string& s,vector<int>& v,int filling)
{
    v.push_back(filling);
    for (auto& a : s)
        v.push_back(a - '0');
    v.push_back(filling);
}

int main(int argc, char *argv[])
{
    {
        //int filling = INT_MAX;
        int filling = 9;
        cGetTime t;
        std::ifstream inf(argv[1]);
        string s;
        inf >> s;

        int xSizeWithPadding = s.size() + 2;
        data1.push_back(vector<int>(xSizeWithPadding, filling));
        data1.push_back(vector<int>());
        stringToIntV(s, data1.back(),filling);
        while (inf >>s && s!="")
        {
            data1.push_back(vector<int>());
            stringToIntV(s, data1.back(), filling);
        }
        data1.push_back(vector<int>(xSizeWithPadding, filling));

        ylast = data1.size(); //y last index
        xlast = data1[0].size();

        vector<pair<int, int> > lowPts;
        findLowPoints(lowPts);
        cout << findTopThreeSizesOfBasins(lowPts) << endl;

        //findBasinPrep(data);
    }
    return 0;
}
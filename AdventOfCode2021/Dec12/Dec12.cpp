#include "getExecutionTime.h"
unordered_map<string, vector<string> > um;
//vector<vector<string> > allPaths;
string* lowStrTwo;
string startStr="start";
string endStr="end";

//part 1.
bool duplicateAndSmall(const vector<string*>& v, const string& s)
{
    for (auto& a : v)
    {
         if (*a == s && s[0] >= 'a' && s[0] <= 'z')
            return true;
    }
    return false;
}

bool duplicateAndSmall_nottwice(const vector<string*>& v, const string& s)
{
    for (auto& a : v)
    {
        if (*a == s && s[0] >= 'a' && s[0] <= 'z')
        {
            if (lowStrTwo != nullptr)
                return true;

            if (s == startStr || s == endStr)
                return true;

            lowStrTwo = a;
            return false;
        }
    }
    return false;
}

int findPaths(vector<string*>& currPath)
{
    if (*(currPath.back()) == endStr)
    {
        //allPaths.push_back(currPath);
        return 1;
    }

    int count = 0;

    for (auto& a : um[*(currPath.back())])
    {
        if (!duplicateAndSmall_nottwice(currPath,a))
        {
            currPath.push_back(&a);
            count += findPaths(currPath);

            if (lowStrTwo != nullptr && a == *lowStrTwo)
                lowStrTwo = nullptr;

            currPath.pop_back();
        }
    }
    return count;
}

int main(int argc, char *argv[])
{
    {
        cGetTime t;
        std::ifstream inf(argv[1]);
        string s;

        while (inf>>s && s!="")
        {
            size_t pos = s.find('-');
            string s1 = s.substr(0, pos );
            string s2 = s.substr(pos + 1);

            if (um.find(s1) != um.end())
            {
                um[s1].push_back(s2);
            }
            else
            {
                um[s1] = vector<string>{ s2 };
            }

            if (um.find(s2) != um.end())
            {
                um[s2].push_back(s1);
            }
            else
            {
                um[s2] = vector<string>{ s1 };
            }
        }


        vector<string*> currPath{ &startStr };
        lowStrTwo = nullptr;

        int count = findPaths(currPath);
        cout << count << endl;
    }
    return 0;
}
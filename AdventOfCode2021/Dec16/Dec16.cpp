#include "getExecutionTime.h"
using scit = string::const_iterator ;

scit theend;

map<char, string> Char2Str{
    {'0',"0000"},
    {'1',"0001"},
    {'2',"0010"},
    {'3',"0011"},
    {'4',"0100"},
    {'5',"0101"},
    {'6',"0110"},
    {'7',"0111"},
    {'8',"1000"},
    {'9',"1001"},
    {'A',"1010"},
    {'B',"1011"},
    {'C',"1100"},
    {'D',"1101"},
    {'E',"1110"},
    {'F',"1111"}
};

int verSum = 0;
long long getLiteralValues(scit& b, scit& e )
{
    string val = "";
    scit b_start = b;
    for (; b < e; )
    {
        if ( *b == '1')
        {
            val += string(b+1, b+5);
            b = b + 5;
        }
        else 
        {
            val += string(b + 1, b+ 5);
            b = b + 5;
            break;
        }
    }

    //this shall be last few binay number of the entire string. 
    //cannot be literal values in the middle.
    if (b > theend-5)
        b = e;
    
    //the val string can be more than the INT_MAX, so use to long long.
    return stoll(val,nullptr,2);
}

long long process(vector<pair<int, long long> >&res, int typ)
{
    vector<long long> resSec;
    for (auto& p : res)
        resSec.push_back(p.second);

    switch (typ)
    {
    case 0:
        //must use long long as init, it is the 2nd templated arugment, and that is the return type of the accumulate funciton, so won't overflow
        return accumulate(resSec.begin(), resSec.end(), static_cast<long long>(0)); 
    case 1:
        return accumulate(resSec.begin(), resSec.end(), static_cast<long long>(1), std::multiplies<long long>());
    case 2:
        return *min_element(resSec.begin(), resSec.end());
    case 3:
        return *max_element(resSec.begin(), resSec.end());
    case 5:
        return resSec[0] > resSec[1] ? 1 : 0;
    case 6:
        return resSec[0] < resSec[1] ? 1 : 0;
    case 7:
        return resSec[0] == resSec[1] ? 1 : 0;
    }
    return 0;
}

/*
b, is the starting position; pass in by reference, so compare with b_start,can know how many bits been process in the call.
e. is the ending position.

return : pair.first: how many bits been processed, pair.second: the calculate value for that packet.
*/
pair<int, long long> processPacket(scit& b, scit& e)
{
    scit b_start = b;
    long long currVal = 0;
    int ver = stoi(string(b, b+3), nullptr, 2);
    int typ = stoi(string(b+3, b+6), nullptr, 2);

    b += 6;
    cout << "ver: " << ver << endl;
    cout << "typ: "<< typ << endl;

    verSum += ver;
    vector<pair<int, long long> > res;
    if (typ == 4)
    {
        currVal = getLiteralValues(b, e);
    }
    else
    {
        if (*b == '0')
        {
            b++;
            int len = stoi(string(b, b + 15), nullptr, 2);
            cout << "length Type is '0', and total length of sub packets is: " << len << endl;
            b += 15;
            scit  PreviousPos = b;
            while (b < PreviousPos + len)
                res.push_back(processPacket(b, e));
        }
        else
        {
            b++;
            int num = stoi(string(b, b + 11), nullptr, 2);
            cout << "length Type is '1' and number of sub packets is: " << num << endl;
            b += 11;
            for (int i = 0; i < num; i++)
                res.push_back(processPacket(b, e));
        }
        currVal = process(res, typ);
    }

    return pair<int, long long>(b - b_start, currVal);
}

int main(int argc, char *argv[])
{
    {
        cGetTime t;
        std::ifstream inf(argv[1]);
        string s;

        vector<string> packets;
        while (inf >> s && s!="")
            packets.push_back(s);

        for (auto& a : packets)
        {
            string tmps = "";
            for (auto& b : a)
            {
                tmps += string(Char2Str[b]);
            }
            a = tmps;
        }

        for (auto& s : packets)
        {
            scit b= s.begin();
            scit e= s.end();
            theend = s.end();
            pair<int, long long> p = processPacket(b,e);
            cout <<"the result is:"<< p.second << endl;
            cout << "the ver sum is:" << verSum << endl;
        }
    }
    return 0;
}
#include "getExecutionTime.h"
vector<vector<char> > datas;
int cols = 0;
int rows = 0;
bool moveOneStep()
{
    bool changed = false;
    int skipXidx = INT_MIN;
    for (int j = 0; j < rows; j++)
    {
        char x0Org = datas[j][0];
        skipXidx = INT_MIN; //reset at next row.
        for (int i = 0; i < cols; i++)
        {
            int ni = (i + 1)% cols; // last one index is 9. (9+1) % 10 =0;
            if (datas[j][i] == '>' && 
                (i != cols - 1 ?
                datas[j][ni] == '.' :
                (x0Org == '.')) && //need to use the original curr row's col 0 data..
                (i!= skipXidx)
                )
            {
                skipXidx = ni;
                datas[j][i] = '.';
                datas[j][ni] = '>';
                changed = true;
            }

        }
    }
    
    vector<bool> skipNextRowXIdx(cols, false);
    vector<bool> skipCurrRowXIdx;
    vector<char> firstRowOrg = datas[0];
    for (int j = 0; j < rows; j++)
    {
        skipCurrRowXIdx = skipNextRowXIdx; 
        skipNextRowXIdx = vector<bool>(cols, false);
        for (int i = 0; i < cols; i++)
        {
            int nj = (j + 1) % rows;
            if (datas[j][i] == 'v' && 
                (j != rows - 1 ? 
                    datas[nj][i] == '.' :
                    (firstRowOrg[i] == '.' || (firstRowOrg[i] == '>' && datas[nj][i] == '.'))) && //need to use the original row 0 data..
                skipCurrRowXIdx[i] == false
                )
            {
                skipNextRowXIdx[i] = true;
                datas[j][i] = '.';
                datas[nj][i] = 'v';
                changed = true;
            }
        }
    }
    return changed;
}

int main(int argc, char *argv[])
{
    {
        cGetTime t;
        std::ifstream inf(argv[1]);
        string s;
        while (inf >> s && s != "")
        {
            datas.push_back(vector<char>(s.size(), '.'));
            for (int i = 0; i < s.size(); i++)
                datas.back()[i] = s[i];
        }
        cols = datas[0].size();
        rows = datas.size();
        int step = 1;
        while (moveOneStep())
            step++;

        cout << " step: "  << step<< endl; //no need -1 since it asking for first step where is no more movement
    }

    return 0;
}
#include "getExecutionTime.h"

vector<vector<int> > data1;
vector<pair<int, int> > nines;

int xStart;
int xEnd;
int yStart;
int yEnd;

void incrementAdjacent(int x, int y)
{
    data1[y - 1][x - 1]++;
    data1[y][x - 1]++;
    data1[y + 1][x - 1]++;
    data1[y - 1][x]++;
    data1[y + 1][x]++;
    data1[y - 1][x + 1]++;
    data1[y][x + 1]++;
    data1[y + 1][x + 1]++;
}

void processEachItem(void(*func)(int,int))
{
    for (int j = yStart; j < yEnd; j++)
    {
        for (int i = xStart; i < xEnd; i++)
        {
            func(i, j);
        }
    }
}

int countLightsInOneStep()
{
    if (nines.size() == 0)
    {
        processEachItem([](int i, int j) { if(data1[j][i]==9) nines.emplace_back(i, j); });
        return 0;
    }

    int count = nines.size();

    for (auto& p : nines)
    {
        incrementAdjacent(p.first, p.second);
        data1[p.second][p.first] = INT_MIN; //set to so negative, it will not accidently become positive!. in the main() rest to 0;
    }

    nines.clear();

    //new flashed (>=10) need to cleared in this step.when check for moreThanNine, 
    //actually can narrow scope of checking the cells updated by incrementAdjacent().
    processEachItem([](int i, int j) { if (data1[j][i] > 9) nines.emplace_back(i, j); });
    return count + countLightsInOneStep();
}

int main(int argc, char *argv[])
{
    {
        cGetTime t;
        int filling = INT_MIN;

        std::ifstream inf(argv[1]);

        int r = 1; //column and row, or x and y. or i,j;
        int c = 1;
        char ch;

        string s;
        inf >> s;

        //wrap the entire 2d vector with Int_Min, to simply the code
        data1.push_back(vector<int>(s.size()+2,filling));

        while (inf && s!="")
        {
            stringstream ss(s);
            data1.push_back(vector<int>(s.size() + 2, filling));

            c = 1;
            while (ss >> ch)
            {
                data1[r][c] = ch -'0';
                if (ch - '0' == 9)
                    nines.emplace_back(c, r);
                c++;
            }
            r++;
            inf >> s;
        }
        data1.push_back(vector<int>(s.size() + 2, filling));

        xStart=1;
        xEnd = data1[0].size() - 1;
        yStart=1;
        yEnd = data1.size()-1;

        int cnt = 0;
        int noOfOctupus = (xEnd- xStart)*(yEnd- yStart);

        for (int i = 1; i < INT_MAX ;i++)
        {
            processEachItem([](int i, int j) { data1[j][i]++; });
            int currStepCnt = countLightsInOneStep();
            processEachItem([](int i, int j) { if(data1[j][i] < 0) data1[j][i]=0; });
            
            cnt += currStepCnt;
            if (currStepCnt == noOfOctupus)
            {
                cout << "sync flash after step:" << i << endl;
                break;
            }
        }
        cout << "total Flash:" << cnt << endl;
    }
    return 0;
}
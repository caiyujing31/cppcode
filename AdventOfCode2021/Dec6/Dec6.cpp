#include "getExecutionTime.h"
const int totaldays = 256;
vector<long long> Cache(totaldays, -1);

long long calculateTotalFromOne(int days)
{
    if (days <= 0)
        return 1;

    if (Cache[days] != -1)
        return Cache[days];

    Cache[days] = calculateTotalFromOne(days - 7) + calculateTotalFromOne(days - 9);
    return Cache[days];
}

int main(int argc, char *argv[])
{
    {
        cGetTime t;
        std::ifstream file(argv[1]);

        int x;
        char c;
        vector<int> in;

        while (file >> x)
        {
            in.push_back(x);
            file >> c;
        }

        long long  count = 0;
        for (auto a : in)
        {            
            count += calculateTotalFromOne(totaldays-a);
        }
        cout << count << endl;
    }

    return 0;
}
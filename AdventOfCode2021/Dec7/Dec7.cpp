#include "getExecutionTime.h"

int main(int argc, char *argv[])
{
    {
        cGetTime t;
        std::ifstream file(argv[1]);

        int x;
        char c;
        vector<int> in;

        while (file >> x)
        {
            in.push_back(x);
            file >> c;
        }
        
        int smallest = INT_MAX;
        int biggest = INT_MIN;
        for (auto& a : in)
        {
            if (a < smallest)
                smallest = a;
            if (a > biggest)
                biggest = a;
        }

        vector<long long> v(biggest - smallest + 1, 0);

        //vector<long long> moveNstep(biggest - smallest + 1, -1); //for each step
        vector<long long> moveNsteps(biggest - smallest + 1, -1); //accumulative
        //moveNstep[0] = 0;
        //moveNstep[1] = 1;
        //moveNsteps[0] = 0;
        //moveNsteps[1] = 1;
        for (int i = 0; i <= biggest; i++)
        {
            //moveNstep[i] = moveNstep[i - 1] + 1;
            //moveNsteps[i] = moveNsteps[i-1]  + moveNstep[i];

            moveNsteps[i]=(1 + i) / 2.0 * i;
        }


        for (int j = 0; j < in.size(); j++)
        {
            for (int i = smallest; i <= biggest; i++)
            {
                v[i] += moveNsteps[abs(in[j] - i)];
            }
        }

        int min_dist = INT_MAX;
        int min_selection = -1;
        for (int i = smallest; i <= biggest; i++)
        {
            if (v[i] < min_dist)
            {
                min_selection = i;
                min_dist = v[i];
            }
        }

        cout << "min_selection is :" << min_selection << endl;
        cout << "min_dist is :" << min_dist << endl;
    }
    return 0;
}
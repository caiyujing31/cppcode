// Day6.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstring>
#include <vector>
using namespace std;
void split(const string &s, char delim);
long long GetSteps(int dis);

vector<long long int> list;
int small = 0, big = 0;
int main()
{
	std::ifstream file("input7.txt");
	string result;
	getline(file, result);
	split(result, ',');

	long long sum = 0;
	long long temp = 0;
	bool firstTime = true;
	for (int i = small; i <= big; i++)
	{
		for (auto x : list)
		{
			temp += GetSteps(abs(x-i));
		}
		if (firstTime)
		{
			firstTime = false;
			sum = temp;
			temp = 0;
			continue;
		}
		if (temp < sum)
			sum = temp;

		temp = 0;
	}
	cout << sum;
}

void split(const string &s, char delim) {
	stringstream ss(s);
	string item;
	//bool firstTime = true;

    int small = INT_MAX;
    int big = INT_MIN;

	while (getline(ss, item, delim)) 
	{
		int value = stoi(item);
		list.push_back(value);
		//if (firstTime)
		//{
		//	small = value;
		//	big = value;
		//	firstTime = false;
		//	continue;
		//}
		if (value > big)
			big = value;
		if (value < small)
			small = value;
	}
}

long long GetSteps(int distance)
{
	return (1 + distance) / 2.0 * distance;
}
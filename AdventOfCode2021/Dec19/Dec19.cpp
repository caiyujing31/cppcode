#include "getExecutionTime.h"
struct point 
{ 
    int x;
    int y;
    int z;
    
    point(int xx, int yy, int zz) :x(xx), y(yy), z(zz) 
    {}
    
    point() :x(INT_MIN), y(INT_MIN), z(INT_MIN) 
    {}
    
    bool operator<(const point& rhs)
    {
        if (x < rhs.x)
            return true;
        else
        {
            if (x == rhs.x)
            {
                if (y < rhs.y)
                    return true;
                else
                {
                    if (y == rhs.y)
                        return z < rhs.z;
                }
            }
        }
        return false;
    }

    bool operator==(const point& rhs)
    {
        return (x == rhs.x) && (y == rhs.y) && (z == rhs.z);
    }

    int operator%(const point& rhs) {     
        if ((x == rhs.x) && (y == rhs.z) && (z == rhs.y))
            return 1;
        if ((x == rhs.y) && (y == rhs.z) && (z == rhs.x))
            return 2;
        if ((x == rhs.y) && (y == rhs.x) && (z == rhs.z))
            return 3;
        if ((x == rhs.z) && (y == rhs.x) && (z == rhs.y))
            return 4;
        if ((x == rhs.z) && (y == rhs.y) && (z == rhs.x))
            return 5;
        
        return -1;
    }
};

vector<point> scannerOffsets;

struct scanner
{
    vector<point> points;
    vector<point> diff;
    vector<pair<int, int> > diffidx;
    vector<int> overlapidx;
    void updateDiff()
    {
        //clear data
        diff = vector<point>();
        diffidx = vector<pair<int, int> >();

        for (int i = 0; i != points.size(); i++)
        {
            for (int j = i + 1; j != points.size(); j++)
            {
                point& pi = points[i];
                point& pj = points[j];
                diff.emplace_back(
                    abs(pi.x - pj.x),
                    abs(pi.y - pj.y),
                    abs(pi.z - pj.z));
                diffidx.emplace_back(i, j);
            }
        }
    }
    void swapxyz(int swap)
    {
        for (auto& a : points)
        {
            int x1 = a.x;
            int y1 = a.y;
            int z1 = a.z;
            switch (swap)
            {
            case 1:
                a.x = x1; a.y = z1; a.z = y1;
                break;
            case 2:
                a.x = y1; a.y = z1; a.z = x1;
                break;
            case 3:
                a.x = y1; a.y = x1; a.z = z1;
                break;
            case 4:
                a.x = z1; a.y = x1; a.z = y1;
                break;
            case 5:
                a.x = z1; a.y = y1; a.z = x1;
                break;
            }
        }
    }

    void updateTheScanner(scanner& sn, pair<point, point>& offset)
    {
        //get unique index (points) for the scanx, insted of 132 points, 
        //we only need the 12 different point (beacon).
        set<int> overlapPs(sn.overlapidx.begin(), sn.overlapidx.end());

        point& positive = offset.second;
        point& offSet = offset.first;
        for (int i = 0; i < sn.points.size(); i++)
        {
            if (overlapPs.find(i) == overlapPs.end())
            {
                point& p = sn.points[i];
                points.emplace_back
                (
                    positive.x * p.x + offSet.x,
                    positive.y * p.y + offSet.y,
                    positive.z * p.z + offSet.z
                );
            }
        }
        sn = scanner(); //clean it since it already moved.
        updateDiff();
    }
};

pair<point,point> findOffsetAndPositive(scanner& s0, scanner& sn)
{
    //need to refer to book1.sheet1 to understand the logic, 
    //it need to compare two pairs of data,
    vector<point> vp;
    vector<point> posp;

    for (int i = 0; i < 4; i+= 2)
    {
        point p0 = s0.points[s0.overlapidx[i]];
        point p1 = s0.points[s0.overlapidx[i+1]];

        point n0 = sn.points[sn.overlapidx[i]];
        point n1 = sn.points[sn.overlapidx[i+1]];

        int posx = (p0.x - n0.x == p1.x - n1.x) ? 1 : -1;
        int posy = (p0.y - n0.y == p1.y - n1.y) ? 1 : -1;
        int posz = (p0.z - n0.z == p1.z - n1.z) ? 1 : -1;

        int x = p0.x - (posx * n0.x);
        int y = p0.y - (posy * n0.y);
        int z = p0.z - (posz * n0.z);
        vp.emplace_back(x, y, z);
        posp.emplace_back(posx, posy, posz);

        //======let us swap the points in n'th scanner for this pair.
        n1 = sn.points[sn.overlapidx[i]];
        n0 = sn.points[sn.overlapidx[i+1]];

        posx = (p0.x - n0.x == p1.x - n1.x) ? 1 : -1;
        posy = (p0.y - n0.y == p1.y - n1.y) ? 1 : -1;
        posz = (p0.z - n0.z == p1.z - n1.z) ? 1 : -1;

        x = p0.x - (posx * n0.x);
        y = p0.y - (posy * n0.y);
        z = p0.z - (posz * n0.z);

        vp.emplace_back(x, y, z);
        posp.emplace_back(posx, posy, posz);
    }

    if (vp[0] == vp[1] || 
        vp[0] == vp[2] || 
        vp[0] == vp[3])
        return make_pair(vp[0],posp[0]);
    else
        return vp[1] == vp[2] ? make_pair(vp[1], posp[1]) 
                              : make_pair(vp[3], posp[3]);
}

bool moreThanTwelve(scanner& s0, scanner& sn)
{
    int count = 0;
    int count2 = 0;
    int swap = INT_MIN;
    for (int i = 0; i < s0.diff.size(); i++)
    {
        for (int j = 0; j < sn.diff.size(); j++)
        {
            if (s0.diff[i] == sn.diff[j])
            {
                //12*11/2 pairs, and 12*11 =132 points. but only 132/2 =66 into this "if" block.
                s0.overlapidx.push_back(s0.diffidx[i].first);
                s0.overlapidx.push_back(s0.diffidx[i].second);
                sn.overlapidx.push_back(sn.diffidx[j].first);
                sn.overlapidx.push_back(sn.diffidx[j].second);
                count++;
                j = sn.diff.size();
            }
            else
            {
                int isSwappedEqual = s0.diff[i] % sn.diff[j];
                if (isSwappedEqual !=-1)
                {
                    //really need to redo here. need a map<swaptype, howmanycollide point>

                    swap = isSwappedEqual;
                    
                    s0.overlapidx.push_back(s0.diffidx[i].first);
                    s0.overlapidx.push_back(s0.diffidx[i].second);
                    sn.overlapidx.push_back(sn.diffidx[j].first);
                    sn.overlapidx.push_back(sn.diffidx[j].second);
                    count2++;
                    j = sn.diff.size();
                }
            }
        }
    }

    if (count >= 66) // 12* (12-1)/2 
    {
        pair<point, point> offset = findOffsetAndPositive(s0, sn);
        s0.updateTheScanner(sn, offset);
        scannerOffsets.push_back(offset.first);
        return true;
    }

    if (count2 >= 66)
    {
        sn.swapxyz(swap);
        pair<point, point> offset = findOffsetAndPositive(s0, sn);
        s0.updateTheScanner(sn, offset);
        scannerOffsets.push_back(offset.first);
        return true;
    }
    return false;
}

int main(int argc, char *argv[])
{
    {
        cGetTime t;
        std::ifstream inf(argv[1]);
        string s;
        vector<scanner> datas;

        int id = 0;
        while (inf>>s && s!="")
        {
            if (s == "---")
            {
                getline(inf, s);
                datas.push_back(scanner());
                id++;
                continue;
            }
            stringstream ss(s);
            int a, b, c;
            char c1, c2;
            while (ss >> a >> c1 >> b >> c2 >> c)
                datas.back().points.emplace_back(a, b, c);
        }

        for (int k = 0; k < datas.size(); k++)
        {
            datas[k].updateDiff();
        }

        set<int> identified{ 0 };
        while (identified.size() < datas.size())
        {
            for (int i = 0; i < datas.size(); i++)
            {
                if (identified.find(i) == identified.end())
                {
                    if (moreThanTwelve(datas[0], datas[i]))
                    {
                        identified.insert(i);
                    }
                    datas[0].overlapidx.clear();
                    datas[i].overlapidx.clear();
                }
            }
        }
        cout << "how many points:" <<datas[0].points.size() << endl;

        int currentMax = INT_MIN;
        for (int j = 0; j < scannerOffsets.size(); j++)
        {
            for (int i = j + 1; i < scannerOffsets.size(); i++)
            {
                int temp = abs(scannerOffsets[j].x - scannerOffsets[i].x) +
                    abs(scannerOffsets[j].y - scannerOffsets[i].y) +
                    abs(scannerOffsets[j].z - scannerOffsets[i].z);
                if (temp > currentMax)
                    currentMax = temp;
            }
        }

        cout << "max distancess:" << currentMax << endl;

    }
    return 0;
}
#include "getExecutionTime.h"
unordered_map<string, char> data1;
vector<long long> vRes(26, 0);

//int calculateMaxMinusMin(const string& start1)
//{
//    vector<int> v(26, 0);
//    for (auto& c : start1)
//    {
//        v[c - 'A']++;
//    }
//
//    auto it = remove(v.begin(), v.end(), 0);
//    v.erase(it, v.end());
//
//    auto it_pair = minmax_element(v.begin(), v.end());
//    return *(it_pair.second) - *(it_pair.first);
//}
//
//void combineTwoStrings(string& start2,string& toBeInsert1)
//{
//    string results="";
//    int i = 0;
//    for (; i < toBeInsert1.size(); i++)
//    {
//        results += start2[i];
//        results += toBeInsert1[i];
//    }
//    results += start2[i];
//    start2 = results;
//}
//
//void insertCharsInBetweenForN(string& start1, int rounds)
//{
//    for (int i = 0; i < rounds; i++)
//    {
//        string toBeInsert = "";
//        for (int j = 0; j < start1.size() - 1; j++) //last char no need already
//        {
//            toBeInsert += data1[start1.substr(j, 2)]; //
//        }
//        combineTwoStrings(start1, toBeInsert);
//    }
//}

long long calculateMaxMin()
{
    auto it = remove(vRes.begin(), vRes.end(), 0);
    vRes.erase(it, vRes.end());

    auto it_pair = minmax_element(vRes.begin(), vRes.end());
    return *(it_pair.second) - *(it_pair.first);
}

struct KeyHasher
{
    std::size_t operator()(const pair<string, int>& k) const
    {
        return ((hash<string>()(k.first) ^ (hash<int>()(k.second) << 1)) >> 1);
    }
};

unordered_map<pair<string, int>, vector<long long>, KeyHasher> cached;
vector<long long> foundNewChars(char c1, char c2, int n)
{
    if (n == 0)
        return vector<long long>(26,0);

    string s = "";
    s += c1; 
    s += c2;

    if (cached.find(pair<string, int>(s, n)) != cached.end())
        return cached[pair<string, int>(s, n)];

    char cNew = data1[s];
    vector<long long> v1 = foundNewChars(c1, cNew, n - 1);
    vector<long long> v2 = foundNewChars(cNew, c2, n - 1);

    vector<long long> v3(26,0);
    v3[cNew - 'A']++;

    for (int i = 0; i < 26; i++)
    {
        v3[i] += v1[i];
        v3[i] += v2[i];
    }

    cached[pair<string, int>(s, n)] = v3;
    return v3;
}

void initializeCounter(const string& start1)
{
    for (auto& c : start1)
        vRes[c - 'A']++;
}

int main(int argc, char *argv[])
{
    {
        cGetTime t;
        std::ifstream inf(argv[1]);
        string start;
        inf >> start;

        string s1, s2;
        char c;

        while (inf >> s1 >> s2 >> c)
            data1[s1] = c;

        //part2:
        initializeCounter(start);
        int runCount = 40;
        for (int i = 0; i < start.size() - 1; i++)
        {
            vector<long long> vt = foundNewChars(start[i], start[i + 1], runCount);
            for(int i=0; i< 26; i++)
                vRes[i] += vt[i];
        }
        cout << calculateMaxMin() <<endl;
    }
    return 0;
}
#include "getExecutionTime.h"

unordered_map<string, int> um;

unsigned long long findTheOnlyEntry1(string s)
{
    while (true)
    {
        if(um.find(s + "0") != um.end())
            s += "0";
        else
        {
            if (um.find(s + "1") != um.end())
                s += "1";
            else
                break;
        }
    }
    cout << s << endl;
    return std::stoull(s, 0, 2);
}

unsigned long long search1(string left, string right, bool flag)
{
    if (um.find(left) == um.end())
        um[left] = 0;
    if (um.find(right) == um.end())
        um[right] = 0;

    //cout << "um[" << left << "]:" << um[left] << endl;
    //cout << "um[" << right << "]:" << um[right] << endl;

    if (um[left] + um[right] == 1)
    {
        if (um[left] == 1)
            return findTheOnlyEntry1(left);
        else
            return findTheOnlyEntry1(right);
    }

    if (um[left] == 1 && um[right] == 1)
    {
        return findTheOnlyEntry1(left);
    }

    if (flag)
    {
        if (um[left] >= um[right])
        {
            return search1(left + "1", left + "0", flag);
        }

        if (um[left] < um[right])
        {
            return search1(right + "1", right + "0", flag);
        }
    }
    else
    {
        if (um[left] <= um[right])
        {
            return search1(left + "0", left + "1", flag);
        }

        if (um[left] > um[right])
        {
            return search1(right + "0", right + "1", flag);
        }
    }
}


int main(int argc, char *argv[])
{
    {
        cGetTime t;
        std::ifstream file(argv[1]);

        /* part 2*/
        vector<string> data;
        string s;

        file >> s;
        int sz = s.size();
        data.push_back(move(s));

        while (file >> s)
        {
            data.push_back(move(s));
        }

        for (int j = 0; j < data.size(); j++)
        {
            for (int i = 0; i < sz; i++)
            {
                string s1 = data[j].substr(0, i + 1);
                if(um.find(s1)!=um.end())
                    um[s1] += 1;
                else
                    um[s1] = 1;
            }
        }
        unsigned long long OxygenVal = search1("1", "0", true);
        unsigned long long CO2scrubber = search1("0", "1", false);

        cout << OxygenVal * CO2scrubber << endl;
    }

    /* part 1*/

    //int count = 0;
    //file >> s;

    //count++;
    //vector<int> ones(s.size(),0);
    //vector<int> ones(s.size(),0);

    //while (file)
    //{
    //    for (int i = 0; i < s.size(); i++)
    //    {
    //        if (s[i] == '1')
    //            ones[i] += 1;
    //    }
    //    file >> s;
    //    count++;
    //}

    //count--; 
    //string gamma = "";
    //string epsilon = "";
    //for (int i = 0; i < ones.size(); i++)
    //{
    //    if (ones[i] == count / 2 && 
    //        (count%2==0))
    //    {
    //        gamma += '1';
    //        epsilon += '0';
    //        continue;
    //    }

    //    if (ones[i] > count / 2) //
    //    {
    //        gamma += '1';
    //        epsilon += '0';
    //        continue;
    //    }

    //    if (ones[i] <= count / 2) // if it is odd, eg 9. then 9/2 is 4, so == 4 means less.
    //    {
    //        gamma += '0';
    //        epsilon += '1';
    //        continue;
    //    }
    //}

    //unsigned long long gammaVal = std::stoull(gamma, 0, 2);
    //unsigned long long epsilonVal = std::stoull(epsilon, 0, 2);
    //cout << gammaVal * epsilonVal << endl;

    return 0;
}
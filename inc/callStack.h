#pragma once
#include <windows.h>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <dbghelp.h>
//#include <Winbase.h>

//got to include dbghelp.lib in the linker input
void printStack()
{
    std::cout << "print call stack" << std::endl;
    typedef USHORT(WINAPI* CaptureStackBackTraceType)(__in ULONG, __in ULONG, __out PVOID*, __out_opt PULONG);
    CaptureStackBackTraceType func = (CaptureStackBackTraceType)(GetProcAddress(LoadLibrary(L"kernel32.dll"), "RtlCaptureStackBackTrace"));

    if (func == NULL)
        return;

    // Quote from Microsoft Documentation: Windows Server 2003 and Windows XP: 
    //The sum of the FramesToSkip and FramesToCapture parameters must be less than 63.
    
    const int kMaxCallers = 62;
    void* callers_stack[kMaxCallers];
    unsigned short frames;
    std::vector<SYMBOL_INFO> symbolVec(kMaxCallers);
    HANDLE process;
    process = GetCurrentProcess();
    SymInitialize(process, NULL, TRUE);
    frames = (func)(0, kMaxCallers, callers_stack, NULL); //caller_stack is a point (array) to  void*

    //starting at 1, to skip this "printStack()" , starting from it is caller as bottom call.
    for (unsigned int i = 1; i < frames; i++)
    {
        std::stringstream out;
        symbolVec[i].MaxNameLen = 255;
        symbolVec[i].SizeOfStruct = sizeof(SYMBOL_INFO);

        SymFromAddr(process, (DWORD64)(callers_stack[i]), 0, &symbolVec[i]);
        out << "*** " << i << ": ";

        //symbol->name : typedef __possibly_notnullterminated CHAR *PNZCH;
        for (unsigned int j = 0; j <= symbolVec[i].NameLen; j++)
            out << (symbolVec[i].Name)[j];
        out << std::endl;
        std::cout << out.str();
    }
}

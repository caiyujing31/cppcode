#pragma once

class B1
{
  public:
    B1()
    {
        initlize(3, 2, "B1-Obj");
        totalInstance++;
    }

    B1(int val)
    {
        //B1(); //it is totally wrong, calling B1() will simply created another object which does not have anything to do with *this.
        initlize(val + 1, val, "B1-Obj");
        totalInstance++;
    }

    B1(string s)
    {
        initlize(3, 2, s);
        totalInstance++;
    }

    B1(const B1 &A1)
    {
        initlize(A1.mX2, A1.mX1, A1.ObjName);
        totalInstance++;
    }

    B1 &operator=(const B1 &A1)
    {
        initlize(A1.mX2, A1.mX1, A1.ObjName);
        return *this;
    }

    virtual ~B1()
    {
        totalInstance--;
        method(); //not a good practice to call virtual function in this desctructor.. could be undefined behavior.
    }

    void setx1(const int &val)
    {
        mX1 = val;
    }
    
    const int &getx1() const//const (for func) can be used as overloading.
    {     
        //setx1(1); a const member func cannot call a nonconst mem func, it will break the rule.
        return mX1;
    }

    const int &getx1()
    {
        return mX1;
    }
    
    virtual void printInfo(void) const
    {
        cout << "B1 print" << endl;
    }

    virtual void method(void)
    {
        cout << "B1 method" << endl;
    }

    virtual void  methodWithDefault(string s0, string s = "B1 Dflt")
    {
        cout << s0 << s << endl;
    }

    virtual bool operator==(B1 &rhs)
    {
        if (typeid(*this) != typeid(rhs))
            return false;

        return mX2 == rhs.mX2 && mX1 == rhs.mX1;
    }

    void f(int d)
    {
        cout << __FUNCDNAME__;
    }

    void baseMethod(void)
    {
        method();
    }

    B1& operator*(const B1 &a)
    {
        B1 *res = new B1(100 + a.mX2);
        return *res;
    }



    static int gettotalInstance(void)
    {
        //return mX1; comiple error,static func will NOT allowed to ACCESS NON-STATIC member data/func;
        return totalInstance;
    }
    //virtual void methodzero(void) = 0; //if B1=0; D1 implment, D2 missing. both B1/D2 cannot Instantiate object!!!
    
    int mX2;
    string ObjName;

protected:
    void initlize(int x2, int x1, string sObjname)
    {
        mX2 = x2;
        mX1 = x1;
        ObjName = sObjname;
        p_mX1 = &mX1;
    }
    void wait(void)
    {
        cout << __FUNCTION__ << endl;
    }
private:
    int *p_mX1;
    int mX1;
    static int totalInstance;
};
int B1::totalInstance = 0;

class D1 : public B1
{
public:
    D1()
    {
        mX2++;
        mX1D = 0;
        ObjName = "D1-Obj";
    }

    D1(string s)
    {
        mX1D = 0;
        ObjName = s;
    }

    virtual ~D1()
    {
        method();
    }

    void setmX1D(int val)
    {
        mX1D = val;
    }

    int getmX1D()
    {
        return mX1D;
    }

    virtual void printInfo(void) const
    { //both virtual func implemented outside class declaration, take out the word "virtual" at front!!
        cout << "D1 print" << endl;
        //cout << "access B1 private member mX1 from D1 class member function:" << mX1 << endl;
        cout << "access B1 private member mX1 from D1 class member function:" << getx1() << endl;
    }

    virtual void method(void)
    {
        cout << __FUNCTION__ << endl;
    }

    virtual void methodWithDefault(string s0, string s = "D1 Dflt")
    {
        cout << s0 <<  s << endl;
    }

    virtual bool operator==(B1 &rhs)
    {
        if (!B1::operator==(rhs)) //it will check and make sure type of rhs same as *this , ie D1, so below can safely cast...
            return false;

        auto tmp = dynamic_cast<D1*>(&rhs);
        if (tmp != nullptr)
            return mX1D == tmp->mX1D;
        else
            return false;
    }

    //using B1::f;
    void f(double d)
    {
        cout << __FUNCTION__;
    }

    void f(const string& s)
    {
        cout << __FUNCTION__;
    }

    float mX1D_ToFloat()
    {
        return static_cast<float>(mX1D);
    }

  private:
    int mX1D;
};

class D2 : public B1
{
  public:
    D2()
    {
        mX2--;
        ObjName = "D2-Obj";
    }

    D2(string s)
    {
        ObjName = s;
    }

    virtual ~D2()
    {
        method();
    }

    virtual void printInfo(void) const
    {
        cout << "D2 print" << endl;
    }

    virtual void method(void)
    {
        cout << "D2 method" << endl;
    }

    virtual void methodWithDefault(string s = "D2 Dflt")
    {
        cout << s << endl;
    }

    float f(float f)
    {
        cout << "f(float):";
        return static_cast<float>(f + 2.3);
    }

    void iWait()
    {
        wait();
    }

    virtual bool operator==(B1 &rhs)
    {
        if (!B1::operator==(rhs))
            return false;
        return true; //D2 dont have its own data.
    }

    bool operator>(const D2 &rhs)
    {
        return this->mX2 > rhs.mX2;
    }
};

bool operator<(const D2 &lhs, const D2 &rhs)
{
    return lhs.mX2 < rhs.mX2;
}

const D2 operator*(const D2 &lhs, const D2 &rhs)
{
    D2 res;
    res.mX2 = lhs.mX2 * rhs.mX2;
    return res; //return value (copy) for small obj is fine,
}
////even though below also fine, the caller will delete, what if caller do  D2 d = a * b * c;//a,b,c all D2 objects. how to do the delete (hiden between b and c).
//const D2& operator*(const D2& lhs, const D2& rhs) {
//    D2* p=new D2;
//    p->mX2 = lhs.mX2 * rhs.mX2;
//    return *p;
//}

void Report(B1 &rAnimal)
{
    rAnimal.method();
}

//factory design pattern;
B1 *makeObj(int choice)
{
    if (choice == 0)
        return new B1;
    else if (choice == 1)
        return new D1;
    else
        return new D2;
}

class A
{
  public:
    A()
    {
        cout << "A()" << endl;
        mP = new int(100); //-V508
    }

    //A(int a=10){} //by calling, A A1; compiler will give error a error: call of overloaded `A()' is ambiguous
    explicit A(int a, int b = 10)
    {
        mP = new int(a + b);
    }

    A(A &&rhs) : mP(rhs.mP)
    { //move CONSTRUCTOR, no need delete mP; becos it is constructor.
        rhs.mP = nullptr;
    }

    ~A()
    {
        cout << "~A" << endl;
        delete mP;
    }

    A(const A &rhs)
    { //before start first line, mP is already defined, but now instantiated yet. for this new object.
        mP = new int(*(rhs.mP));
    }

    A &operator=(const A &rhs)
    {
        //mP=new int; re-assign(2nd time) new address of int obj on heap!.. old one memory leak!!!
        //since this code will only been executed if "this" object is created!. ie = only used during value re-assingment.
        if (this != &rhs)
        { //address compare is much better than the content compare (you might also need to override the != for the class if use *this != A1)...
            int *p = nullptr;
            try
            {
                p = new int(*(rhs.mP));
                //throw exception("bad alloc?");
            }
            catch (exception e)
            {
                if (p != nullptr)
                    delete p;
                return *this; //internal consistent, either fully done, or nothing done.
                throw;
            }

            delete mP;
            mP = p;
        }
        return *this;
    }

    A operator=(A &&rhs)
    { //move assignment
        if (this == &rhs)
            return *this;
        delete mP;
        mP = rhs.mP;
        rhs.mP = nullptr;
        return *this;
    }

    void printA(void) { cout << "printA:" << *mP << endl; }

    void swap(A &rhs)
    {
        using std::swap;
        swap(mP, rhs.mP);
    }

  private:
    int *mP;
};

//valgrind --leak-check=yes ./class_condes_exe
class B
{
  public:
    B(int i)
    {
        mP_int = new int(i);
        if (i == 202)
            throw invalid_argument("e1"); //even throw and captured at the caller, NO use, the memory for "new int" will not be deleted

        try
        {
            if (i == 203)
                throw invalid_argument("e2");
        }
        catch (const invalid_argument &e)
        {
            cout << "exception at:" << e.what() << "cleaning ..." << endl;
            // the compiler smart engouth to do cleaning of those already constructed.
        }
    }

    ~B()
    {
        //if (*mP_int == 200)
        //throw invalid_argument("e at ~B()");

        try
        {
            if (*mP_int == 201)
                throw invalid_argument("e at ~B()");
        }
        catch (const invalid_argument &arg_expt)
        {
            cout << "exception at:" << arg_expt.what() << "cleaning ..." << endl;
        }
        delete mP_int;
    }

  private:
    int *mP_int;
};

class HTMLElement
{
    string _name;
    string _text;

  public:
    vector<HTMLElement> elements;
    const size_t indent_size = 2;

    string str(int indent = 0) const
    {
        ostringstream oss;
        string i(indent_size * indent, ' '); //nice code.
        oss << i << "<" << _name << ">" << endl;
        if (_text.size() > 0)
            oss << string(indent_size * (indent + 1), ' ') << _text << endl;

        for (const auto &e : elements)
            oss << e.str(indent + 1); //that is nice code

        oss << i << "</" << _name << ">" << endl;
        return oss.str();
    }

    HTMLElement(const string &name, const string &text) : _name(name), _text(text) {}
};

/*const - A compile time constrain that an object can not be modified, or become read only, Guard against unauthorised write to critical data.
self documenting,enable compiler to do optimization,variable can be put in ROM!*/

namespace std
{
    template <>
    void swap<A>(A &lhs, A &rhs)
    {
        lhs.swap(rhs);
    }
}

namespace A_NS
{
    struct X
    {
    };

    void g(X xa)
    {
        cout << "calling A_NSg::g(X xa)" << endl;
    }
}

class C_koenig
{
public:
    void g(A_NS::X xa)
    {
        cout << "calling C_koenig::g(A_NS:: xa)" << endl;
    }

    void j(void)
    {
        A_NS::X x2;
        g(x2);
        //always search matches inside class defination first, if can not find, then resolve to koeig lookup (where its argument defined),
        //if C_koenig::g(A_NS::X xa) NOT defined, it will resolved to A_NS::g(X xa) call
    }
};

class CSingleton
{
    CSingleton() { mString = "Skeleton_string_Value"; }
    string mString;

  public:
    string GetString(void) { return mString; }
    void SetString(const string &newStr) { mString = newStr; }
    static CSingleton &Instance(void)
    {
        static CSingleton *instance = new CSingleton;
        return *instance; // always returns the same instance, dereferencing, then pass by reference! awesome
    }
    ~CSingleton() { delete &(Instance()); }
};


template <class T, class B>
struct Derived_from
{
    static void constrains(T *p) { B *pb = p; }
    Derived_from() { void (*g)(T *) = constrains; } // void(*g)(T*) -- function pointer.
};

class dog /*final, not allowed to create derived class of dog*/
{
  public:
    //all the three initialization method used dog d1{1,1.1..} methods. so it is called uniform initialization!!!
    //3rd, aggregate initialization
    int age = 0;
    int weight = 0;
    std::string m_name = "";
    shared_ptr<dog> m_pfriend;
    weak_ptr<dog> m_pTrueFriend;
    dog()
    {
        //cout << "dog()" << endl;
    }

    //2nd, regular constructor;
    dog(int a, int w) : age(a + 1),
                        weight(w + 1),
                        m_name("dummy")
    {
    }

    //1st ; priority initialization_list
    /*dog(const initializer_list<int>& abc){ 
        for (initializer_list<int>::iterator itr=abc.begin();itr!=end();itr++){
            age=*itr; weight=(*itr)*10;    m_name = "dummy";
        }  
    }*/

    dog(string s) : m_name(s)
    {
        //cout << "dog(string s)" << endl;
    }

    void bark(void)
    {
        cout << "dog " << m_name << " rules!" << endl;
    }

    virtual void func3(void) final{
        /*... */
    }; //NO derived class can allowed to override this func...useful only if this class is mid of class inheritance tree.

    virtual ~dog()
    {
        cout << "~dog()" << endl;
        ;
    }

    void makeFriends(shared_ptr<dog> f) { m_pfriend = f; }

    void makeTrueFriends(shared_ptr<dog> f) { m_pTrueFriend = f; }
    void showTrueFriends()
    { 
        if (!m_pTrueFriend.expired()) //in case it is already expired (associated object is released when come here)/
            cout << "true friend is " << m_pTrueFriend.lock()->m_name << endl;
        //lock will create a temporary shared_ptr<dog> here and it will
        //prevent the object been released (destroyed) when calling ->m_name, so a real mutex lock is necessary here.

    }
    
    dog &operator=(const dog &a) = delete; //effectively disable it.

    bool operator==(const dog &a)
    {
        return (this->m_name == a.m_name);
    }
};


class AA {
public:
    //virtual func pointer, 8 bytes in x64.
    virtual void printVir(void) 
    { 
        cout << "AA::printVir()" << endl; 
    }
    
    void print(void) 
    { 
        cout << "AA::print()" << endl; 
    }
    
    void print1(int a) 
    { 
        cout << "AA::print1(int)" << endl; 
    }
    char c = 'c'; //1byte. but 64 arch, will padding 7 bytes. 
    //int i = 0; //if i is here, the variable c and i will be padded together, 1+4=5, 
    //only need 3 bytes to pad to become 8 bytes. so total struct size will be 16 bytes.
    
    double b = 1.1;
    
    int i = 0; //4byte, but 64 arch, will padding 4 bytes.
    /*virtual*/ ~AA() 
    { 
        std::cout << "AA" << endl; 
    } //take out the virtual keywords.
};

//total struct size will be 24 bytes in such case.!

class BB :public AA {
public:

    virtual void printVir(void) 
    { 
        cout << "BB::printVir()" << endl; 
    }
    
    void print(void) 
    { 
        cout << "BB::print()" << endl; 
    }
    
    void print1(const string& s)
    {
        cout<<"BB::print1(string)" << s <<endl;
    }

    ~BB() 
    { 
        std::cout << "BB" << endl; 
    }
private:
    char c2 = 'q';
};

class cInitList 
{
public:
    operator bool()const {
        return val > 9;
    }

    cInitList operator=(const cInitList& rhs) {
        return cInitList(rhs.val);
    }

    bool operator==(const cInitList& rhs) {
        return val == rhs.val;
    }

    void operator()() {
        val++;
        cout << "functor without argument" << endl;
    }

    void operator()(const double& b) {
        val += int(b);
        cout << "functor with a double&" << endl;
    }

    cInitList():val(1){}

    cInitList(int i) :val(i), str(intToString(i++)), str_sz(str.size()) {}
    //pass in the c(10), it will assing str_sz first, which will end up a rubbish number since str is not initzliaed yet!! not even a 0 value.
    //so the intialization list will always follow the declared order in the class declartion
//private:
    string intToString(int i) { stringstream ss; ss << i; return ss.str(); }
    int str_sz;
    string str;
    int val;
};

class cString
{
public:
    cString() = default;
    cString(const char* cs)
    {
        cout<<"ctor"<<endl;
        m_size = strlen(cs)+1;
        m_Data = new char[m_size];
        memcpy(m_Data, cs, m_size);
    }
    cString(const cString& cs) 
    {
        cout << "cp ctor" << endl;
        m_size = cs.m_size;
        m_Data = new char[m_size];
        memcpy(m_Data, cs.m_Data, m_size);
    }

    cString(cString&& rhs)
    {
        cout << "mv ctor" << endl;
        m_size = rhs.m_size;
        m_Data = rhs.m_Data;
        rhs.m_size = 0;
        rhs.m_Data = nullptr;
    }

    cString operator*(const cString& rhs)
    {
        if (rhs.m_size < 1) 
            return "";
        
        cout << "op *" << endl;
        cString c;
        c.m_size = this->m_size + rhs.m_size;
        c.m_Data = new char[c.m_size];
        memcpy(c.m_Data, rhs.m_Data, rhs.m_size-1); //no need to null termination.
        memcpy(c.m_Data + rhs.m_size-1, this->m_Data, this->m_size);

        return c; //return value optimization.. 
    }

    void print() const // shall not modify anything of this object
    {
        string s(m_Data, m_Data + m_size);
        //cout << m_Data; //not nulltermined. will print rubbish
        cout << s;
    }
    ~cString()
    {
        cout << "destroyed" << endl;
        delete m_Data;
    }
    friend ostream& operator<<(ostream& os, const cString rhs);
private:
    char* m_Data;
    uint32_t m_size;
};

ostream& operator<<(ostream& os, const cString rhs) 
{
        
    os << rhs.m_Data << " with size of " << rhs.m_size << std::endl;
    return os;
}
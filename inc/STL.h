#pragma once
#include "getExecutionTime.h"
#include "classes.h"

template<typename T>
class VEC : public std::vector<T> {
public:
	using vector<T>::vector; //use the constructor from vector (under name of CaiVec)

	T& operator[](int i) 
	{
		return vector<T>::at(i);
	}

	const T& operator[](int i) const 
	{
		return vector<T>::at(i);
	}
};

namespace ExtStd {
	using namespace std;

	template<typename C>
	void sort(C& c) {
		sort(c.begin(), c.end());
	}

	template<typename C, typename Pred>
	void sort(C& c, Pred p) {
		sort(c.begin(), c.end(), p);
	}
}

void VecCapGrowAndReallocation(void){
	vector<long> vec(4l, 100l);
	//vec.reserve(1005); //capacity will be set as 1005, important, with this line,
	//all below expand, realloaction can be avoided !

	long long cap = vec.capacity();
	long long addr = (long long)&(vec[0]);

	vector<long long> capV;
	capV.push_back(cap);
	
	vector<long long> addrV;
	addrV.push_back(addr);

	for (long long i = 0; i < 1000; i++)
	{
		vec.push_back(100 + i);
		
		if (vec.capacity() != capV.back())
			capV.push_back(vec.capacity());
		
		if ((long long)&(vec[0]) != addrV.back())
			addrV.push_back((long long)&(vec[0]));
	}
	//cap from 4,6,9,13,19,28,42,63,94,1421,211,316,.... 50%, addr also changed everytime cap expanded!!!
}

void InitVecPriorC11(vector<int>& v) {
	//vector<Dog> v(6)    // 6 dog created with default constructor, size=6, capacity=6;
	//votr<Dog> v2;		// size=0;capacity=0
	//v2.resize(6);		// ***** 6 dog created with default constructor, size=6, capacity=6;
                        // if current is 4, 2 additional DOg will be created, if current is 8, the last two will be destroyed.
	//vector<Dog> v3;  v3.reserve(6);	//NO dog created, size =0, capacity=6; //reserve only Allocates memory.
	vector<int> v2(4, 100);
	vector<int> v3(v2.begin(), v2.end() - 2);
	vector<int> v4(v2);
	v2.swap(v2); //C++ 03 trim of the reduntand reserved memory. same as v2.shrink_to_fit()
	v2.resize(6); //add two 0 at back
	v2.push_back(50); //100,100,100,100,0,0,50

	int arr2[] = { 4, 5, 7, 8, 5, 1, 12, 7, 8, 11, 1, 2,99 };
	vector<int> v6(arr2, arr2 + sizeof(arr2) / sizeof(int));
	ExtStd::sort(v6);

	vector<int> v7;
	v7.swap(v6);
	v = v7;
}

void VectorErase() {
	vector<int> v{ 1,2,2,4,5,2,7,9,2,11,7,6 };
	vector<int> v2 = v;
	//	if (*it == 2)
	//		v.erase(it); 
	//		"it" if NOT been update when the removal point is itself, "it" will not work as expected, even if it works, the number 3 is skipped!!!...
    
    auto it = v.begin();
	for (; it != v.end();) {
		if (*it == 2)
			it = v.erase(it);
		else
			it++;
	}
    //v.erase(it); //*********exception will be thrown since right now "it" is pointing to v.end()

	auto it2 = std::remove(v2.begin(), v2.end(), 7); //v will point to the new end() suppose to be
	//v2.erase(it); //this is wrong, it only removed one item. got to remove from "it" till end of the vector
	v2.erase(it2, v2.end());	
}

class CObject {
public:
	CObject() { cout << "ctor" << endl; }
	~CObject() { cout << "dtor" << endl; }

};

void VectorErase2() {
	vector<shared_ptr<CObject>> v1;
	v1.emplace_back(new CObject());
	v1.emplace_back(new CObject());
	v1.emplace_back(new CObject());
	vector<shared_ptr<CObject>> v2(v1);
	for (auto it = v1.begin(); it != v1.end(); it++)
	{
		cout << "address of it:" << &it << endl; //"it" is a pointer, iterator through entire container, it's address Never change
		cout << "address of *it:" << &(*it) << endl;
		auto rawPtr = (*it).get();
		cout << "address of object on heap:" << rawPtr << endl;

	}
	cout << "=======" << endl;
	for (auto it = v2.begin(); it != v2.end(); it++)
	{
		cout << "address of it:" << &it << endl;
		cout << "address of *it:" << &(*it) << endl;
		auto rawPtr = (*it).get();
		cout << "address of object on heap:" << rawPtr << endl;
	}
}

void VectorInvalidation() {
	//raw pointer, or iterator, 
	//position before or after target(removed/inserted) position.
	//it will be invalided, so Please re-populate the pointers	
}

void VectorAsSet(void) {
	vector<int> v{ 4, 5, 7, 8, 5, 1, 12, 7, 8, 11, 1, 2,99 };
	std::sort(v.begin(), v.end());
	std::binary_search(v.begin(), v.end(), 11); //it will only return true or false.

    //prefer use below to get exact returned iterator if found!
	//vector<int>::iterator it = lower_bound(v.begin(), v.end(), 7, CmpAsc());
	//if (it != v.end() && *it == price) {
			//means can find!!!
}

void VectorAsQueue(void) {
	int FrontIndex = 0;
	vector<int> v{ 4, 5, 7, 8, 5, 1, 12, 7, 8, 11, 1, 2,99 };

	int j = v[FrontIndex];
	FrontIndex++; //using this variable to track the starting of the q. nice. but lets hope the queue wont be so long
}

//++++++++++++++++++++++++++++MAP+++++++++++++++++++
void ListPopFrontResize(void) {
    list<int> l1;
    int currCap = 0;

    for (int i = 0; i < 1000; i++) {
        l1.push_front(i);
    }
    //the list does not even have a call to capacity (the capcity is unlimited (=system main memory), so there is no ShrinkToFit either
    //it only has size, return current hold size, and each removal of an item (remove/erase/pop) will automagically release the memory back to pool
    cout << "current size:" << l1.size();
    l1.pop_front();
    cout << "current size:" << l1.size();

    for (int i = 0; i < 1000; i++) {
        l1.push_back(i);
    }

    l1.resize(3000);//simply add in another 1001 item with value 0 at back.
}

//void MapPrintEntry(const map<char, int>& map1){
//the subscribe operator provides write ops, so the container can NOT be const; so if want to use it can NOT pass in as const &
//the subscribe operator [ can NOT be used by multimap/set,because they do NOT have unique 'keys'
void MapPrintEntry(map<char, int>& map1){
	cout << map1['c'] << endl;
}

typedef map<string, map<string, string> > Str2StrMap;
typedef map<string, string> Str2Str;
typedef pair<string, string> StrPStr;

void NestedMap(void) {
	Str2Str m1;
	Str2Str::const_iterator it2;
	m1.insert(make_pair("AA", "aa0")); //insert entry to map with three methods, 1st make_pair
	m1.insert(StrPStr("BB", "bb0"));
	m1["CC"] = "cc0";//3rd one. direct assignment.

	Str2StrMap Str2StrMap_1;
	Str2StrMap::const_iterator it1;

	Str2StrMap_1["1"] = m1;
	m1["AA"] = "aa2"; m1["BB"] = "bb2"; m1["CC"] = "cc2";
	Str2StrMap_1["2"] = m1;

	for (it1 = Str2StrMap_1.begin(); it1 != Str2StrMap_1.end(); it1++) {
		cout << "Key = " << it1->first << endl;
		for (it2 = it1->second.begin(); it2 != it1->second.end(); it2++) {
			cout << "\tVal =" << it2->first << "-" << it2->second << endl;
		}
	}
}

void MapSubscribing(void) {
	//case1:
	map<string, int> MagicMap;
	MagicMap.insert(make_pair("abcd", 1234));
    MagicMap.insert(make_pair("waitTime", 3)); //NOT waiTimes
	cout << MagicMap["abcq"] << endl;
	cout << MagicMap.size() << endl;
	cout << "wah, size changed, when did i insert the second pair??" << endl;
	
    int mTimeOUt = MagicMap["waitTimes"]; //mTimeOut is 0,could be wait forever
}

void PropertiesOfUnorderedMap(unordered_map<string, string>& un_map){
	cout << "total bucket #" << un_map.bucket_count() << endl;
	cout << "load_factor = " << un_map.load_factor() << endl;
	cout << "element with key  'ab' is in bucket #" << un_map.bucket("ab") << endl;
}

class c_lsb_lss{
public:
	bool operator()(int x, int y){
		return (x % 10)<(y % 10);
	}
};
//C++11 lambda function, ... function without a name;  [](int x){return x<10;} //x is the parameter pass into function. it equal to below function.
bool smaller_than_10(int x){
	return x<10;
}

bool LargerThanFifteen(double a) {
	return a>15.0;
}

bool mymax(int x, int y){
	return (x % 10)<(y % 10);
}

int X2(int x){
	return x * 2;
}
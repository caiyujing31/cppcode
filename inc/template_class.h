#pragma once
//***************** Template Class1 ****************************//

//rule of 0, if u do not define any of 5, the 5 will be auto generated for you.
//rule of 5, if you defined any of the 5, the rest will NOT be auto generated for you.
//because the compiler think it is unsafe to auto create(may require customized func).
//so in the end you have to define all.

template <typename T1>
class Bo {
	T1* arr;
	int sz;
public:
	Bo() {}
	~Bo() {
		delete[] arr;
	}
	Bo(const initializer_list<T1>& c) {
		arr = new T1[c.size()];
		int i = 0;
		for (typename initializer_list<T1>::iterator itr = c.begin(); itr != c.end(); itr++, i++) {
			arr[i] = *itr;
		}
		sz = i;
	}
	Bo(const Bo<T1>& rhs) {
		arr = new T1[rhs.getSize_Bo()]; //ctor means "this" is empty at begining, so no Delete[] arr
		int i = 0;
		for (; i < rhs.getSize_Bo(); i++) {
			arr[i] = rhs.get_Bo(i);
		}
		sz = i;
	}
	Bo& operator=(const Bo<T1>& rhs) {
        if (this == &rhs)
            return *this;

		delete[] arr;
		arr = new T1[rhs.getSize_Bo()];
		int i = 0;
		for (; i < rhs.getSize_Bo(); i++) {
			arr[i] = rhs.get_Bo(i);
		}
		sz = i;
		return *this;
	}

	Bo(Bo<T1>&& rhs) {//base_type(rhs.mArr).
		arr = rhs.arr; //ctor means "this" is empty at begining, so no Delete[] arr
		sz = rhs.sz;
		rhs.arr = nullptr;
		rhs.sz = 0;
	}

	Bo& operator=(Bo<T1>&& rhs) {
        if (this == &rhs)
            return *this;

		delete[] arr;
		arr = rhs.arr;
		sz = rhs.sz;
		rhs.arr = nullptr;
		rhs.sz = 0;
		return *this;
	}

	void print_Bo(void) const {
		for (int i = 0; i < sz; i++)
			cout << arr[i] << endl;
	}

	Bo<T1> operator*(const Bo<T1>& rhs2) {
		Bo<T1> b;
		b.arr = new int[rhs2.getSize_Bo()];
		int i = 0;
		for (; i < rhs2.getSize_Bo(); i++) {
			b.arr[i] = this->arr[i] * rhs2.get_Bo(i);
		}
		b.sz = i;
		return b;
	}

	T1 get_Bo(int i) const { return arr[i]; }
	int getSize_Bo(void) const { return sz; }
};

template <typename T2>
class Bo2 {
public:
	vector<T2> mV;
	Bo2(vector<T2>&& v) : mV(move(v)) {
		//v is actually become an lvalue in this function.however after the mV(move(v)) 
		//call, it in an undetermined state. so can not use in the body again.
	}
};

//***************** Template Class2, type+value ****************************//
template<typename T, int N>
//typename enable_if< N< 128 && is_integral<T>::value, void>::type
class stdarray {
	T marray[N];
public:
	stdarray(const T& t) {
		marray[0] = t;
	} // a literal can be treated as const T&

	int GetSize() const {
		return N;
	}
};

//***************** Template Class3, template of a template class ****************************//
template<typename t1>
class b {
public:
    b() { cout << "b var1 type of:" << typeid(var1).name() << endl; }
    t1 var1;
};

template<typename t2>
class d1 : public b<t2> {
public:
    d1() { cout << "d1 is called with type:" << typeid(var2).name() << endl; }
    //float -f, int-i, point to const float, p-K-f
    t2 var2;
};

template<typename t1, typename t2>
class d2 : public b<t1> {
public:
    d2() { cout << "d2 is more fancier class with var2 type of:" << typeid(var2).name() << endl; }
    operator string() {
        if (std::is_same<t2, std::vector<typename t2::value_type, typename t2::allocator_type>>::value)
            return "a vector in string"s;
        else
            return "not a vector, so no string"s;
    }
    t2 var2;
};
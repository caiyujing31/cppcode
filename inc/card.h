//deck of cards
const int ALLSIZE = 52;
class card
{
  public:
	card(){};

	card(const string &cardFace, const string &cardSuit)
	{
		face_of_suit = cardFace + "_OF_" + cardSuit;
	}

	void print(void)
	{
		cout << face_of_suit << endl;
	}

  private:
	string face_of_suit;
};

class deckOfCards
{
  public:
	deckOfCards();
	void shuffle();
	card *dealCard();
	~deckOfCards();

  private:
	//user defined copy construcotr and assignment operation, at private. Disable the two; Rule of three.
	deckOfCards operator=(const deckOfCards &a) { return *this; }
	deckOfCards(const deckOfCards &a) {}

	vector<card *> deck;
	int currentCard;
};

deckOfCards::deckOfCards()
{
	vector<string> faces = {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
	vector<string> suits = {"Hearts", "Diamonds", "Clubs", "Spades"};
	currentCard = 0;
	for (size_t i = 0; i < faces.size(); i++)
	{
		for (size_t j = 0; j < suits.size(); j++)
			deck.push_back(new card(faces[i], suits[j]));
	}
}

void deckOfCards::shuffle()
{
	currentCard = 0;
	for (int first = 0; first < ALLSIZE / 2; first++)
	{ //shuffle half is pretty good enough
		int second = static_cast<int>(RndEng() % ALLSIZE);
		card *temp = deck[first];
		deck[first] = deck[second];
		deck[second] = temp;
	}
}

card *deckOfCards::dealCard()
{
	if (currentCard >= (ALLSIZE - 1))
	{
		shuffle();
		return deck[0];
	}

	return (deck[currentCard++]);
}

deckOfCards::~deckOfCards()
{
	vector<card *>::iterator it;
	for (it = deck.begin(); it < deck.end(); it++)
	{ //The C++ language guarantees that delete p will do nothing if p is null
		delete *it;
	}
}

#include "getExecutionTime.h"

int MaxRegInHistogram(vector<int>& v) {
    int res = 0;
    int prev = INT_MIN;
    vit L = v.begin();
    vit R = v.end();
        
    for (vit it = v.begin(); it != v.end(); it++) {
        vit l = it; //moving ptr
        vit r = it;

        if (*it < prev)
            l = L;

        while (*l >= *it && l != v.begin())
            l--;

        if ( *l >= *it && l == v.begin())
            l++;
          
        vit itRskip = it; //skip to the right continously same value to the current *it
        while ( itRskip != v.end() && *itRskip == *it )
            itRskip++;
        itRskip--;

        //moving to right...
        while ( r != v.end() && *r >= *it )
            r++;
        r--;
        
        //calculate the results.
        int ResT= (*it)*(1 + r-l);
        
        if (ResT > res)
            res = ResT;

        L = l;
        R = r;
        prev = *it;

        if (itRskip != it)
            it = itRskip;
    }

    return res;
}

//the max (area) 1 rectangular in the matrix.
int MaximalRectangle(vector<vector<char> >&vv) {
    int res = INT_MIN;

    if (vv.size() == 0)
        return 0;

    int vS = vv[0].size(); //size of elments in x axis

    vector<int> v(vS, 0);

    for (auto& a : vv) {
        for (int i = 0; i < a.size(); i++)
        {
            if (a[i] == '1')
                v[i]++;
            else
                v[i] = 0; //when found 0, reset the accumulative vertical count of 1 on that x positions.
        }
        int r = MaxRegInHistogram(v);
        res = r > res ? r : res;
    }
    return res;
}


int main() {
    vector<int> v(200);
    generate_vector(v, false, 100);
    { 
        cGetTime t1;
        cout << MaxRegInHistogram(v) << endl;;
    }

    //the max (area) 1 rectangular in the matrix.
    vector<vector<char> > vv = {
        {'1', '0', '1', '1', '1', '1', '1', '1'},
        {'1', '1', '0', '1', '1', '1', '1', '0'},
        {'1', '1', '1', '1', '1', '1', '1', '0'},
        {'1', '1', '1', '1', '1', '0', '0', '0'},
        {'0', '1', '1', '1', '1', '0', '0', '0'}
    };
    cout << MaximalRectangle(vv);

    return 0;
}




/*
17-11-2018 Cai YuJing    -    Create
*/
//changed the OrderBook.h to OrderBook.cpp and removed the OrderBook.cpp from the project (but not the file physicall), so no translation unit of orderBook.cpp is created.
//so there is no duplications.
#include "OrderBook.h"

//invoking Orderbook Default contructor(Public), make_unique is exception safe, it supported in C++14.
unique_ptr<OrderBook> p = make_unique<OrderBook>();

void processCmd(string& s) {
    string comment= R"(//)";
    std::size_t found = s.find(comment);
    if (found != std::string::npos)
        s.erase(found);

    stringstream ss(s);
    string cmd1="";
    string cmd2="";
    ss >> cmd1;
    
    int order_id=-1;
    char side='-';
    double price=-1; 
    int size=-1;

    int new_size=-1;
    int level=-1;

    switch (cmd1[0]) {
        case 'a': //add
            ss >> order_id >> side >> price >> size;
            p->add(order_id, side, price, size);
            break;
        case 'm': //modify
            ss >> order_id >> new_size;
            p->modify(order_id, new_size);
            break;
        case 'r': //remove
            ss >> order_id;
            p->remove(order_id);
            break;
        case 'g': //get
            ss >> cmd2 >> side >> level;
            if (cmd2[0] == 'p') //price
                cout<<p->get_price(side, level)<<endl;
            if (cmd2[0] == 's') //size
                cout<<p->get_size(side, level)<<endl;
            break;
        default:
            return;
    }
}

int main(int argc, char* argv[]) {
    openFileAndProcessLine(argc, argv, processCmd);
    return 0;
}
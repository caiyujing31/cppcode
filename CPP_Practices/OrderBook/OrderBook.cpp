//#pragma once 

#include "OrderBook.h"
void OrderBook::add(int order_id, char side, double price, int size) {

    if (order_id == -1 || side == '-' || price == -1 || size < 1) //expect "size" is postive integer
        return;

    order o(side, price, size);
    mOrders[order_id] = o;

    (side == 'B') ? updateAdd(mPSB, price, size) : updateAdd(mPSS, price, size);
}

void OrderBook::modify(int order_id, int new_size) {

    if (order_id == -1 || new_size < 1) //expect "new_size" is postive integer
        return;

    char side = '-';
    double price = -1;
    int sizeChg = -1;
    unordered_map<int, order>::iterator it = mOrders.find(order_id);
    if (it != mOrders.end()) {
        side = (it->second).side;
        price = (it->second).price;
        sizeChg = new_size - (it->second).size;
        (it->second).size = new_size;
    }
    else {
        //come here only if got "remove" same id happened before this command.
        return;
    }

    (side == 'B') ? updateModifyRemove(mPSB, price, sizeChg) : updateModifyRemove(mPSS, price, sizeChg);
}

void OrderBook::remove(int order_id) {
    if (order_id == -1)
        return;

    char side = '-';
    double price = -1;
    int sizeChg = -1;
    unordered_map<int, order>::iterator it = mOrders.find(order_id);
    if (it != mOrders.end()) {    
        sizeChg = -1 * (it->second).size;
        side = (it->second).side;
        price = (it->second).price;
        mOrders.erase(it);
    }
    else {
        //come here only if two or more "remove" same id
        return;
    }

    (side == 'B') ? updateModifyRemove(mPSB, price, sizeChg) : updateModifyRemove(mPSS, price, sizeChg);
}

double OrderBook::get_price(char side, int level) {
    if (side == '-' || level < 1) //expect level is postive integer
        return -1;

    if (side == 'B') {
        //the order book may query level larger than max.
        if (level > static_cast<int>(mPSB.size()))
            return -1;
        
        return (mPSB[mPSB.size() - level]).first;
    }

    if (side == 'S') {
        if (level > static_cast<int>(mPSS.size()))
            return -1;

        return (mPSS[level - 1]).first;
    }

    return -1;
}

int OrderBook::get_size(char side, int level) {
    if (side == '-' || level <1) //expect level is postive integer
        return -1;

    if (side == 'B') {
        if (level > static_cast<int>(mPSB.size()))
            return -1;

        return (mPSB[mPSB.size() - level]).second;
    }

    if (side == 'S') {
        if (level > static_cast<int>(mPSS.size()))
            return -1;

        return (mPSS[level - 1]).second;
    }

    return -1;
}

//will add item in the mPSB and mPSS
void OrderBook::updateAdd(vector<pair<double, int> >& PS, double& price, int& size) {
    //chronoTimer t1;
    vector<pair<double, int> >::iterator it = lower_bound(PS.begin(), PS.end(), make_pair(price, -1), CmpAsc());        

    if (it != PS.end() && it->first == price) {
        it->second += size;
    }
    else
    {
        PS.insert(it, make_pair(price, size));
    }

    //////below method maximize cache hit, but still can NOT above method, tested in Debug/Release mode 
    //vector<pair<double, int> >::iterator it=PS.begin();
    //for (; it != PS.end();it++) {
    //    if (it->first == price){
    //        it->second += size;
    //        return;
    //    }
    //    if (it->first > price) {
    //        PS.insert(it, make_pair(price, size));
    //        return;
    //    }
    //}
    //PS.insert(it, make_pair(price, size));
}

//will remove items in mPSB and mPSS (if the total size of that price level become 0). 
void OrderBook::updateModifyRemove(vector<pair<double, int> >& PS, double& price, int& sizeChg){

    vector<pair<double, int> >::iterator first = lower_bound(PS.begin(), PS.end(), make_pair(price, -1), CmpAsc());

    if (first != PS.end() && first->first == price) {
        first->second += sizeChg;
        if (first->second == 0)
            first=PS.erase(first);
    }
}
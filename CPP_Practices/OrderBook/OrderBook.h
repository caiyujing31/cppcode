#pragma once
#include "getExecutionTime.h"

struct CmpAsc{
    int operator()(pair<double, int> lhs, pair<double, int> rhs){
        return lhs.first < rhs.first;
    }
};

struct order {
    char side = '-';
    double price = -1;
    int size = -1;
    order() {}
    order(char side1, double price1, int size1) : side(side1), price(price1), size(size1) {}
};

class OrderBook {
private:
    unordered_map<int, order> mOrders; //search/add/remove fast, vector add/remove too costly.
    vector<pair<double,int> > mPSB;
    vector<pair<double,int> > mPSS;

public:
    void add(int order_id, char side, double price, int size);
    void modify(int order_id, int new_size);
    void remove(int order_id);
    double get_price(char side, int level);
    int get_size(char side, int level);

private:
    void updateAdd(vector<pair<double, int> >& PS, double& price, int& size);
    void updateModifyRemove(vector<pair<double, int> >& PS, double& price, int& sizeChg);
};
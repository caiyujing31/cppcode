#include "getExecutionTime.h"

struct trie {
    char c;
    bool newC;
    trie* lastP;//not owning pointer.
    vector<trie*> children;

    trie(const char& _c) :c(_c), newC(false),  lastP(this)
    {
    }

    ~trie() 
    {
        for (auto& p : children)
            delete p;
    }

    void insertWord(const string& _s)
    {
        resetCP();

        for (auto& c : _s)
            insertChar(c);
    }

    void insertChar(char _c) 
    {
        if(newC)
        {
            insertNewChar(_c);
            return;
        }

        if(!findChar(_c))
            insertNewChar(_c);
    }

    void insertNewChar(char _c)
    {
        trie* temp = new trie(_c);
        lastP->children.push_back(temp);
        lastP = temp;
        newC = true;
    }
    
    trie* findWord(const string& _s)
    {
        resetCP();
        for (auto& c : _s)
            if (!findChar(c))
                return nullptr;
        return lastP;
    }

    bool findChar(char _c) 
    {
        for (auto& a : lastP->children)
        {
            if (_c == a->c)
            {
                lastP = a;
                return true;
            }
        }
        return false;
    }

    vector<string> readAllWords() {

        if (children.size() == 0)
            return vector<string>{};

        vector<string> vc;
        for (auto& a : children)
        {
            vector<string> vctemp = a->readAllWords();
            
            if (vctemp.empty())
                vc.push_back(string(1,a->c)); //string(size_t n, char c), instantiate a string with n number of char!.

            for (auto& s : vctemp) {
                s = a->c + s;
                vc.push_back(s);
            }
        }

        return vc;
    }
    
    void resetCP() {
        newC = false;
        lastP = this;
    }
};

vector<string> dictionary;
trie t('_');

vector<string> autocomplete(const string& _s) {
    trie* p = t.findWord(_s);

    vector<string> res;

    if (p != nullptr)
        res = p->readAllWords();
    else
        return {};

    if (res.empty()) 
    {
        cout << _s << endl;
        return { _s };
    }

    for (auto& a : res) 
    {
        a = _s + a;
        cout << a << endl;
    }

    return res;
}


int main() {
    dictionary = vector<string>{ "dog","dark", "cat","door", "dodddddj", "dodge","did","elli" };
    for (auto& s : dictionary) 
        t.insertWord(s);

    autocomplete("di");
    return 0;
}
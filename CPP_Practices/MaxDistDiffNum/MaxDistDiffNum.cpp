#include "getExecutionTime.h"
int solution_org(const std::vector<int>& A)
{
	int N = A.size();
	int result = 0;
	for (int i = 0; i < N; i++)
		for (int j = i; j < N; j++)
			if (A[i] != A[j])
				result = max(result, j - i);
	return result;
}

int solution_2a(const std::vector<int>& A)
{
	int N = A.size();
	int result = 0;
	
	for (int i = 0; i < N; i++) {
		for (int j = N-1; j >=i; j--) {
			if (A[i] != A[j])
			{
				result = max(result, j - i);
				j = -1;
				if (j == N-1)
					return result;
			}
		}
	}
	return result;
}

class MyHashFunction {
public:
	size_t operator()(const pair<vit, vit>& l) const {
		return (l.second - l.first); //not a good hash function
	}
};

//stack overflow ! throw error when over 5k items before encounter first non equal pair
unordered_map<pair<vit, vit>, int, MyHashFunction> cache;
int recr(vit b, vit e)
{
	if (e - b <= 0) {
		return 0;
	}

	auto itPair = make_pair(b, e);

	if (cache.find(itPair) != cache.end()) {
		return cache[itPair];
	}

	if (*b != *e) {
		cache[itPair] = e - b;
	}
	else {
		cache[itPair] = max(recr(b + 1, e), recr(b, e - 1));
	}
	return cache[itPair];
}

int solution_2b(std::vector<int>& A)
{
	auto end = A.end() - 1;
	return recr(A.begin(), end);
}

int main(int argc, char* argv[]) {

	vector<int> v(40000,0);
	generate_vector(v, false, 3);
	v[v.size()/2] = 1;
	v[v.size() / 4] = 1;
	v[3*v.size() / 4] = 1;
	vector<int> v2a(v);
	vector<int> v2b(v);

	{
		cGetTime c;
		cout<< solution_org(v) <<endl;
	}
		
	{
		cGetTime c;
		cout << solution_2a(v2a) << endl;
	}

	{
		cGetTime c;
		cout << solution_2b(v2b) << endl;
	}
}
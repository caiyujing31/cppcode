#include "getExecutionTime.h"
map<vit,int> cached;

int solution(vit it1, vit it2) 
{
    if (cached.find(it2) != cached.end())
       return cached[it2];

    int dist = it2 - it1;
    if (dist == 0)
        return 0;
    
    int last = *(it2 - 1);
    
    int mvSvn = 0;
    auto it = lower_bound(it1, it2, last-6);
    if (it != it2) {
        mvSvn = solution(it1, it) + 7;
    }
    
    auto it3 = it2 - 1;
    int mvOne = solution(it1, it3)+2;
    int min = min(mvSvn, mvOne);
    
    if (min >= 25)
        min =25;

    cached[it2] = min;
    return min;
}
int main() {
    int res = 0;
	//vector<int> v = { 1,2,4,5,7,14,15,17,20,21,22,23,25,29,30 };
	vector<int> v = { 1,2,4,28,29,30 };
	int res2 = 0;
	vector<int> v2(v);

    {
        cGetTime t1;

        if (v.size() >= 25) {
            res = 25;
        }
        else {
            res = solution(v.begin(), v.end());
        }

    }
	cout << res << endl;


    return 0;
}

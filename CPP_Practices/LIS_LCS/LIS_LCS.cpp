// Strings.cpp : Defines the entry point for the console application.
//
#include "getExecutionTime.h"

//longest increasing subarray (may not continous)
/*
Optimal Substructure:
Let arr[0..n-1] be the input array and L(i) be the length of the LIS ending at index i such that arr[i] is the last element of the LIS.
Then, L(i) can be recursively written as:
L(i) = 1 + max( L(j) ) where 0 < j < i and arr[j] < arr[i]; or
L(i) = 1, if no such j exists.
To find the LIS for a given array, we need to return max(L(i)) where 0 < i < n.
Thus, we see the LIS problem satisfies the optimal substructure property as the main problem can be solved using solutions to subproblems.
*/

int LIS(vector<int>& v) {
    vector<vector<int> > lis(v.size(), vector<int>());
    for (int i = 0; i != lis.size(); i++) {
        lis[i].push_back(i);
        //for each of lis[i], the size is 1
    }
    
    for (int i = 0; i != v.size(); i++) {
        
        for (int j = 0; j != i; j++) {
            if (v[j] < v[i] && lis[j].size() + 1 > lis[i].size()) {
                lis[i] = lis[j];
                lis[i].push_back(i); //lis[i]  is lis[i1]+1 now
            }
        }
    }
    vector<int> res = *(std::max_element(lis.begin(), lis.end(), [](vector<int>& lhs, vector<int>& rhs) { return lhs.size()<rhs.size();}));

    for (auto& a : res) {
        cout << v[a] << "\t";
    }
    return res.size();
}

//longest common subarray (may not be contious sub array!)
vector<vector<int> > Results;
int LCS(char arr1[], int n, char arr2[], int m){
    int res = 0;

    if (Results[n][m] != -1)
        return Results[n][m];

    if (n == 0 || m == 0) {
        Results[n][m] = 0;
        return 0;
    }

    if (arr1[n - 1] == arr2[m - 1])
    {
        res = 1 + LCS(arr1, n - 1, arr2, m - 1);
    }
    else
    {
        int t1 = LCS(arr1, n - 1, arr2, m );
        int t2 = LCS(arr1, n, arr2, m - 1);
        res = t1 > t2 ? t1 : t2;
    }

    Results[n][m] = res;
    return res;
}

int main(){
    char arr1[] = { 'G', 'T','j', 'B','A','B','A' ,'C','H', 'M', 'L','Z'};
    char arr2[] = { 'G', 'T', 'T', 'X','B','A','B','A', 'Y', 'H','C','B','Z', 'M','L'};
    int n = sizeof(arr1) / sizeof(arr1[0]);
    int m = sizeof(arr2) / sizeof(arr2[0]);
    //cGetTime time1;
    Results = vector<vector<int> >(n + 1, vector<int>(m + 1, -1)); //skip the 0-based.
    cout << "LCS is :" << LCS(arr1, n, arr2, m) << endl;

    vector<int> v(100);
    generate_vector(v,false,200);
    {
        cGetTime t;
        cout << "\ntotal Number is:"<<LIS(v) << endl;
    }

    cin.get();
    return 0;
}


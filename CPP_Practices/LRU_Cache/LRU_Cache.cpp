#include "getExecutionTime.h"

class LRUCache {
public:
    LRUCache(int capacity) :cap(capacity)
	{
        mV.reserve(cap * ResCoef);
    }

    int get(int key) {

        int i = 0;
        if (mV.size() >= cap)
            i = mV.size() - cap;

        for (; i < mV.size(); i++) 
        {
            if (mV[i].first == key) {
                pair<int, int> find = mV[i];
                mV.push_back(find);
                mV.erase(mV.begin() + i);
                return (mV.back()).second;
            }
        }

        return -1;
    }

    void put(int key, int value) {
        if (get(key) != -1) {
            //already found existing, and push-ed to last position. we just need to update the value.
            (mV.back()).second = value;
            return;
        }

        mV.push_back(make_pair(key, value));
        if (mV.size() > cap * (ResCoef - 1)) {
            copy(mV.end() - cap, mV.end(), mV.begin());
            mV.erase(mV.begin() + cap, mV.end());
            mV.reserve(cap * ResCoef);
        }
    }

private:
    vector<pair<int, int>> mV;
    int cap = 0;
    int ResCoef = 10;
};

int main() {
    unique_ptr<LRUCache> p1 = unique_ptr < LRUCache>(new LRUCache(2));
    //put(key, value) - Set*** or insert the value if the key is not already present
    p1->put(1,1);
    p1->put(2,2);
    cout<<p1->get(1)<<endl;  //exepct 2, not 1...
    p1->put(3,3);
    cout<<p1->get(2) << endl;
    p1->put(4,4); //evict 2,22;
    cout<<p1->get(1);
    cout<<p1->get(3);
    cout<<p1->get(4);

    return 0;
}
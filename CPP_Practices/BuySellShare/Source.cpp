#include "getExecutionTime.h"

vector<int> vc; 
int maxProfit(vit b, vit e) {
	if (e - b == 1)
		return 0;
	int lastIdx = e - b - 1;
		
	if (vc[lastIdx] != -1)
		return vc[lastIdx];
	
	for (unsigned int i = 0; i <= lastIdx-1; i++) {
		
		if (vc[i] == -1)
			vc[i] = maxProfit(b, b + i + 1);

		if (i != 0)
			vc[i] = (vc[i] > vc[i - 1]) ? vc[i] : vc[i - 1];
		
		for (unsigned int j = 0; j <= i; j++)
		{
			int tmp;
			if(j != 0)
				tmp = vc[j-1] + *(b+ lastIdx) - *(b + j);
			else
				tmp = vc[j] + *(b + lastIdx) - *(b + j);

			if (tmp > vc[lastIdx]) {
				vc[lastIdx] = tmp;
			}
		}
	}

	return vc[lastIdx];
}


int main()
{
	//vector<int> v2 = vector<int>{ 27,30,43,73,5,37,87,73};
	vector<int> v2(1000, 1);
	generate_vector(v2);

	vector<int> v2b(v2);
	{
		cGetTime t;
		vc = vector<int>(v2.size(), -1);
		maxProfit(v2.begin(), v2.end());
		cout << *max_element(vc.begin(), vc.end()) << endl;
	}
	return 0;
}

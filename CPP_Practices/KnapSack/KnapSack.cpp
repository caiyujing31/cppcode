// KnapSack.cpp : Defines the entry point for the console application.
/*
Given ws and vs of n items, put these items in a knapsack of capacity W to get the maximum total v in the knapsack.
In other words, given two integer arrays val[0..n-1] and wt[0..n-1] which represent vs and ws associated with n items respectively.
Also given an integer W which represents knapsack capacity, 
find out the maximum v subset of val[] such that sum of the ws of this subset is smaller than or equal to W.
YOU CAN TAKE MULTIPLE of same items.
*/
#include "getExecutionTime.h"
vector<int> Knapsacks(100, -1);
vector<pair<int,int> > Sack;

int KnapsackFullRecurDyna(int wRem) {    
    if (wRem <= 0)
        return 0;

    int maxv = INT_MIN;

    if (Knapsacks[wRem] != -1)
        return Knapsacks[wRem];

    for (vector<pair<int,int>>::iterator it = Sack.begin(); it != Sack.end(); it++) {
        if ((*it).first <= wRem) {
            int t = KnapsackFullRecurDyna(wRem - (*it).first); //actually this allowed to take repeative items
            if ((*it).second +t > maxv ) {
                maxv = t + (*it).second;
            }
        }
        ////earlier exit, if *it.first is bigger than wRem, the higher index of *it sure much bigger than wRem,since the Sack is ascending order.
        if ((*it).first > wRem)
            break;
    }

    Knapsacks[wRem] = maxv;
    return maxv;
}

int main(){
    Sack.reserve(100);
    for (int i = 0; i < 100; i++) {
        Sack.emplace_back(RndEng() % 20 + 1, RndEng() % 20 + 1);
    }
    std::sort(Sack.begin(), Sack.end()); 
    vector<pair<int,int>>::iterator it = std::unique(Sack.begin(), Sack.end());
    //unique itself will delete those duplicate ones, how ever the last few items will be duplicate... when move forwards.
    Sack.erase(it, Sack.end());

    //Sack is a sorted (Asceding order with unique w
    {
        cGetTime t1;
        cout << KnapsackFullRecurDyna(90) << endl;
    }
    return 0;
}


#include "getExecutionTime.h"

set<string> comps = { "3", "14", "314","15","92657","82", "6462", "826462","58979","222222","2" };
string s = "314159265758979826462"s;
//it shall be uniques, since we allowed multiple 14 been used in the partition, so effectively same as multiset!
using sit = string::iterator;
sit Start = s.begin();
sit End = s.end(); 

vector<int> cached(s.size(), -1);
int solution(const sit it1) {

    if (it1 == End)
        return 0;

    if (cached[it1 - Start] != -1)
        return cached[it1 - Start];

    vector<string> founds;
    string s = "";

    for (sit it = it1; it != End; it++) 
	{
        s += *it;
        if (comps.find(s) != comps.end())
            founds.push_back(s);
    }

    if (founds.size() == 0)
        return -1;

    int minCnt = INT_MAX;

    for (auto it = founds.begin(); it != founds.end(); it++) 
    {
        int count = solution((*it).length() + it1); //recursively look for the stripped front,
        if (count != -1 &&
            (count + 1) < minCnt)
        {
            minCnt = count + 1;
        }
    }

    cached[it1 - Start] = minCnt;
    return cached[it1 - Start];
}

int main(int argc, char** argv) {
    // std::sort(comps.begin(),comps.End(),[](const string& lhs, const string rhs){ return (lhs[0]-'0')< (rhs[0]-'0');});
    cout<<solution(s.begin())-1;
    return 0;
}
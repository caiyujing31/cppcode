#include "getExecutionTime.h"

int MaximumSubarrayProduct(vit it1, vit it2) {
    bool foundFirst = false;
    int firstNeg=-1;
    int lastNeg=-1;
    int totalNegNums = 0;
    long long products = 1;
    for (vit it=it1 ; it <= it2; it++ ) {
        products *= static_cast<long>(*it);
        if (*it < 0) {
            totalNegNums++;
            if (!foundFirst) {
                firstNeg = it-it1;
                foundFirst = true;
                lastNeg = firstNeg;
            }
            else {
                lastNeg = it - it1;
            }
        }
    }
    //main idea is to check if there is odd or even number of neg number in the array!
    //found if the multiple to -9 is bigger
    //abc-9xxxx-3ef ; if a*b*c*(-9)=-354, and (-3)*e*f = -333
    //we will remove the -3ef!.
    long long productLeft=-1;
    long long productRight = -1;
    if (totalNegNums % 2 == 1) {
        for (auto it = it1; it <= it1 + firstNeg; it++) {
            productLeft *= static_cast<long>(*it);
        }
        for (auto it = it1 + lastNeg; it <= it2; it++) {
            productRight *= static_cast<long>(*it);
        }
        //productLeft and right are negative number, so we choose the bigger value (less neg)
        if (productLeft >= productRight) {
            return products / productLeft * (-1);
        }
        else {
            return products / productRight * (-1);
        }
    }
    else
        return products;
}

int main(int argc, char *argv[])
{
    vector<int> v(30);
    generate_vector(v, true, 10);

    int tmp = 0;
    int res = INT_MIN;
    for (auto it1 = v.begin(); it1 != v.end();) {
        auto it2 = it1;
        
        //basically below block is to find a continously non-zero sub-array, and process.
        while ( it2 != v.end() && *it2 != 0 ) {
            it2++;
        }
        
        if (it2 == v.end()) 
		{
            tmp = MaximumSubarrayProduct(it1, it2 - 1);
        }
        else 
		{
            if (it2 != v.end() && *it2 == 0 && it2 >it1)
                tmp = MaximumSubarrayProduct(it1, it2 - 1);
        }

        if (tmp > res)
            res = tmp;
        //...

        while (it2 != v.end() && *it2 == 0) {
            it2++;
        }

        it1 = it2;
    }

    cout << res << endl;
    return 0;
}
#include "getExecutionTime.h"

bool oneEditAway(const string& s1, const string& s2) {  
    string s;//short
    string l;//long

    if (s1.size() < s2.size()) 
    {
        s = s1;
        l = s2;
    }
    else 
    {
        s = s2;
        l = s1;
    }

    size_t szs = s.size();
    size_t szl = l.size();
    
    if (szs <= szl - 2) 
    {
        return false;
    }

    if (szs + 1 == szl) {
        for (size_t i = 0; i < szs; i++) {
            if (s[i] != l[i]) {
                s = s.substr(i);
                l = l.substr(i+1);
                if (s == l)
                    return true;
                else
                    return false;
            }
        }
        //if compare up to the size of s, still all the same, 
        //the last char of l we dont care. 
        return true;
    }

    // abcd acbd
    bool foundOneDiff = false;
    if (szs == szl) {
        for (size_t i = 0; i < szs; i++) {
            if (s[i] != l[i]){
                if (foundOneDiff) {
                    return false;
                }
                foundOneDiff = true;
            }
        }
        return true;
    }
    return true;
}

int main(int argc, char* argv[]) {
    string longs1 = generateString(10000);
    string longs2 = longs1;
    longs2[5000] = static_cast<char>(longs2[5000] + 1);
    {   
        cout << "cyj:" << endl;
        cGetTime t;
        cout << oneEditAway(longs1, longs2) << endl;
    }    
    
    longs2 += "A"; //with above one replace, here insert, it shall failed now.
    {
        cout << "cyj:" << endl;
        cGetTime t;
        cout << oneEditAway(longs1, longs2) << endl;
    }
    return 0;
}

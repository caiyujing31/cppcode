// bitPopulation.cpp : Defines the entry point for the console application.
/*
18: 0 001 0010   
001 0010->1101101 +1 -> 1101110
-18:1 110 1110
*/
#include "getExecutionTime.h";

vector<int> bitVec(256);
int GetBitsNumber(int i) {
    int count = 0;
    for (int j = 0; j < 8; j++) 
    {
        if (i >> j & 1)
            count++;
    }
    return count;
}

void populateLookUpTable(void) {
    for (int i = 0; i < 256; i++) 
    {
        bitVec[i] = GetBitsNumber(i);
    }
}

void DecimalToBinary(int i) {
    vector<int> BinaryRep;
    while (i / 2 != 0) 
    {
        BinaryRep.push_back(i % 2);
        i = i / 2;
    }
}

int MinFlipAlternative(int i) {
    int target1 = 0xAAAAAAAA; //A 1010
    int target2 = 0x55555555; //5 0101
    int count1 = 0;
    int count2 = 0;
    for (int j = 0; j < 32; j++)
    {
        //if not the same for that bit, just increment count, 
        //till all 32 bits been compared same. such an elegent solution.
        if (((target1 >> j) & 0x1) != ((i >> j) & 0x1))
            count1++;
        if (((target2 >> j) & 0x1) != ((i >> j) & 0x1))
            count2++;
    }

    return (count1 < count2) ? count1 : count2;
}

void NextSparseNumber(int n) {
    //A number is Sparse if there are NO two adjacent 1s in its binary representation.
    //Given a number x, find the smallest Sparse number which greater than or equal to x.
    for (int i = 0; i < 32; i++) { 
        if ( n & (0x1 << i) 
             && 
             n & (0x1 << (i + 1))
            ) {//from right to  left.         
            n += (1 << i);
            n = (n >> i) << i; // zero(verb) all lower bits(incl the i'th)
        }
    }
}

int sumBitDifferences(int arr[], int n)
{   //nice algo, 
    //eg: the 4th bit (from right) for 6 numbers are as follow. got 4 '1's and 2 '0's (6-4);  
    //so the combination will be 4*2
    //......1...
    //......0...
    //......1...
    //......1...
    //......0...
    //......1...

    int ans = 0;
    for (int i = 0; i < 32; i++){
        int count = 0;
        for (int j = 0; j < n; j++)
            if ((arr[j] & (1 << i)))// count 1
                count++;
        ans += (count * (n - count));
    }
    return ans;
}

int getElementOnlyAppearOnce(int arr[], int n)
{
    vector<int> arr_bit(32, 0);

    //below double loops essentially check each bit of the int number in the arr, for entire 32 bits.
    //actuall swap the inner loop with outer loop might be better, since can use temporal caching. (when u 
    //deal with one number in the array, u want finished all the operation for that numbers before move on
    for (int i = 0; i < 32; i++){
        for (int j = 0; j < n; j++){
            if (arr[j] & 1 << i)
                arr_bit[i]++;
        }
    }

    int theOne = 0;
    for (int i = 0; i < 32; i++){
        if (arr_bit[i] % 3 != 0)
            theOne += 1 << i;
    }
    return theOne;
}

/*
Input :   736425
Output :  736524
Swap 4 and 5 to make the largest even number.
*/

/*
TO DO!. actually scan the number from the left (Most significant to left), found the first even/0 number
which is smaller than the single digit pos odd number, if cannot find. just swap to the rightmost even number.
*/
void swapToLargestEvenNumber(int& a) {
    if (a % 2 == 0)
        return;

    int a1 = a;
    int b = a % 10; //5 in the example
    int c = 0; //4 in the example
    int pos = 0;
    int c_pos = 0;
    bool isFirst = true;
    while (a / 10 != 0) {
        a /= 10;
        pos++;
        if ((a % 10) % 2 == 0) { //make sure the swapped is even number.
            if (isFirst) {
                c = a % 10;
                c_pos = pos;
                isFirst = false;
            }
            else {
                if ((a % 10) < b) {
                    c = a % 10;
                    c_pos = pos;
                }
            }
        }
    }

    a = a + (b - c)*pow(10, c_pos) - (b - c);
}


int main()
{
    int k = 345439;

    populateLookUpTable();
    cout << bitVec[k & 255] + bitVec[(k >> 8) & 255] + bitVec[(k >> 16) & 255] + bitVec[(k >> 24) & 255] << endl;

    DecimalToBinary(k);

    cout << MinFlipAlternative(k);    

    int arr[] = { 2,7 };
    int n = sizeof arr / sizeof arr[0];
    cout << sumBitDifferences(arr, n) << endl;

    int A[] = { 1,2,2,2,1,1,19,19,222,10,222,222,19 };
    int A_size = sizeof(A) / sizeof(A[0]);
    getElementOnlyAppearOnce(A, A_size);

    NextSparseNumber(44);

    int anOddNumber = 736425;
    swapToLargestEvenNumber(anOddNumber);//
    cout << anOddNumber << endl;

    return 0;
}


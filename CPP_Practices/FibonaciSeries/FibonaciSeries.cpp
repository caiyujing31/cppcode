// FibonaciSeries.cpp : Defines the entry point for the console application.
/*
dynamic coding, there are two key attributes that a problem can be solved in dynamic programming.
optimal substructure (recursive) AND overlapping sub-problems. (in below case, fibo(4)
will be calll multiple times when calling eg fibo(6,) fibo(7).
it if is non-overlapping sub problem, it will be called "divide and conquer" even though still recursive,
eg, Merge/quick Sort...
*/
#include "getExecutionTime.h"

vector<long> FibV(100, -1);
long FibDyn(int indx) {
    
    if (indx == 0)
        return 0;

    if (indx == 1)
        return 1;

    if (FibV[indx] != -1) {
        return FibV[indx];
    }
    
    FibV[indx] = FibDyn(indx - 1) + FibDyn(indx - 2);
    return FibV[indx];

}

//normal inefficent recursive.
long Fib(int indx) {

    if (indx == 0)
        return 0;

    if (indx == 1)
        return 1;

    return Fib(indx - 1) + Fib(indx - 2);
}

int main()
{
    {
        cGetTime t;
        cout<<Fib(35)<<endl; //45 is limits, 25, 18micro sec
    }
    {
        cGetTime t;
        cout << FibDyn(35) << endl; //45 is limits,25, 2918micro sec;
    }
    return 0;
}


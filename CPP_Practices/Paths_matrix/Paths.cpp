// paths.cpp : 
// 09 feb 2019, rework, a fixed matrix, move only to right or down, find how many diff paths from top left to bottom right
// the 0 is a block, can not pass.for simplicity make sure these two points always with value of 1.
// for example below will give answer of 9

#include "getExecutionTime.h"
vector<vector<int>> matrix = {
    {1, 1, 1, 1, 1},
    {1, 0, 1, 0, 1},
    {1, 1, 1, 1, 1}, //,
    {0, 1, 1, 1, 0},
    {1, 1, 1, 1, 1} //,
                    //{0,1,1,1,1}
};

int _Y = matrix.size();
int _X = matrix[0].size();

vector<vector<int>> cache(_Y, vector<int>(_X, -1));

int findHowManyPath(int y, int x)
{
    if (y >= _Y || x >= _X)
        return 0;

    if (y == _Y - 1 && x == _X - 1)
        return 1;

    if (cache[y][x] != -1)
        return cache[y][x];

    int R = 0;
    if (x < _X - 1 && matrix[y][x + 1] == 1)
    {
        if (cache[y][x + 1] != -1)
            R = cache[y][x + 1];
        else
            R = findHowManyPath(y, x + 1);
    }

    int D = 0;
    if (y < _Y - 1 && matrix[y + 1][x] == 1)
    {
        if (cache[y + 1][x] != -1)
            D = cache[y + 1][x]; 
        else
            D = findHowManyPath(y + 1, x);
    }

    cache[y][x] = R + D;
    return cache[y][x];
}

int main()
{
    {
        cGetTime t1;
        cout << findHowManyPath(0, 0) << endl;
    }
    return 0;
}

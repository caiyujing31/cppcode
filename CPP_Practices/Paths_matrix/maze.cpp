// FindPrimeNumber.cpp : Defines the entry point for the console application.

#include "getExecutionTime.h"

vector<vector<int>> maze={
    {0,1,3},
    {1,0,2},
    {2,1},
    {3,0,4,6},
    {4,3,5,7},
    {5,4,},
    {6,3},
    {7,4,8},
    {8,7}
};
vector<int> visited;
vector<int> current;
void findThePath(void)
{
    current.push_back(0);
    visited.push_back(0);
    int currPos = current.back();

    while(current.back()!=8 && current.empty()==false){
        currPos = current.back();
        if(currPos==8) return;

        vit it =maze[currPos].begin();
        bool foundOutlet=false;
        while(it!=maze[currPos].end()&&!foundOutlet){
            if(find(visited.begin(),visited.end(),*it)!=visited.end())
                it++;
            else{
                foundOutlet=true;
            }    
        }
        if(foundOutlet){
            current.push_back(*it);
            visited.push_back(*it);
        }
        else{
            current.pop_back();
        }
    }
}

int main()
{
    findThePath();
    print_info(current,"the path"s);
    return 0;
}
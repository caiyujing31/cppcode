#include "getExecutionTime.h"

vector<vector<int> > combs;
//this issue is like knapsack dynamic recursive issue. and much simplier 

void insertNew(vector<int>& currV) {
    for (auto it = combs.begin(); it != combs.end(); it++) {
        if (it->size() == currV.size()) {
            if (is_permutation(it->begin(), it->end(), currV.begin()))
                return;
        }
    }
    combs.push_back(currV);
}

void combinationalSum(int target, const vector<int>& v, vector<int>& currV) {

    for (auto it = v.begin(); it != v.end(); it++) {
        if (target > *it) {
            currV.push_back(*it);
            combinationalSum(target-*it, v, currV); 
			//when currV={5,5}; when recursive again, found no able to go futher ,
			//remaining 2 is smaller than smallest item in the array (5)
            currV.pop_back();//so it pop up the 5, and ready to go for 7 , yeah
        }
        else {
            if (target == *it) 
			{
                currV.push_back(*it);
                insertNew(currV);
                currV.pop_back();
            }			
            return; 
            //when smaller or equal, then need to return to the call 
			//in the if(target>*it) and pop up another elements.
			//and proceed to the next bigger number in the hope to find match ==
        }
    }
}

vector<vector<int>> combinationalSum(vector<int>& v, int target) {
    vector<int> currV;
    combinationalSum(target, v, currV);
    return combs;
}

int main() {
    int target = 11;
    vector<int> v = { 8,7,4,3,11,5,6 };
    std::sort(v.begin(), v.end());
    vector<vector<int> > t= combinationalSum(v, target);
    return 0;
}
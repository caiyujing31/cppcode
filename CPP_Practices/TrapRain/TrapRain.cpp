// TrapRain.cpp : Defines the entry point for the console application.

#include "getExecutionTime.h"

int WaterAtLeft(vit b, vit e)
{
  if (e <= b + 1)
    return 0;

  auto Max = std::max_element(b, e);

  int Water = 0; //right side
  for (auto it = Max + 1; it != e; it++)
  {
    Water += (*Max - *it);
  }

  return Water + WaterAtLeft(b, Max);
}

int WaterAtRight(vit b, vit e)
{
  if (e <= b + 1)
    return 0;

  auto Max = std::max_element(b, e);

  int Water = 0; //left side
  for (auto it = b; it != Max; it++)
  {
    Water += (*Max - *it);
  }

  return Water + WaterAtRight(Max + 1, e);
}

int main()
{
  vector<int> vec1(1000);
  generate_vector(vec1, false);
  auto it = std::max_element(vec1.begin(), vec1.end());
  int Left = WaterAtLeft(vec1.begin(), it); //the it is actually exluded out in the func
  int Right = WaterAtRight(it + 1, vec1.end());

  cout << "total is:" << Left + Right << endl;
  return 0;
}

/* Returns true if the there is a subarray of arr[] with sum equal to 'sum'otherwise returns false. Also, prints the result */
#include "getExecutionTime.h"

bool isTriplet(int arr[], int n)
{
    sort(arr, arr + n);
    for (int i = n - 1; i >= 2; i--)
    {
        int l = 0;
        int r = i - 1;
        while (l < r)
        {
            if (arr[l] + arr[r] == arr[i])
            {
                //cout << arr[l] << " " << arr[r] << " " << arr[i] << endl;
                return true;
            }
            (arr[l] + arr[r] < arr[i]) ? l++ : r--; 
        }
    }
    return false;
}

//find the min lengths of continuous elements whose sum is bigger than x.!
int subArraySumGreater(int arr[], int n, int x)
{
    int curr_sum = 0;
    int min_len = INT_MAX;
    int start = 0;
    int end = 0;

    while (end < n)
    {
        while (curr_sum <= x && end < n)
            curr_sum += arr[end++];

        while (curr_sum > x && start < n)
        {
            if ((end - start) < min_len) {
                min_len = end - start;
                //cout << "start:"<<start<<"\t end:" << end << endl;
            }
            curr_sum -= arr[start++];
        }
    }
    return min_len;
}

void subArraySumEqual(vector<int> &vec1, int sum)
{
    int curr_sum = 0;
    vit start = vec1.begin();

    for (auto it = vec1.begin(); it != vec1.end(); it++)
    {
        while (curr_sum > sum && it > start)
        {
            curr_sum -= *start;
            start++;
        }

        if (curr_sum == sum)
        {
            break;
        }
        curr_sum += *it;
    }
}

int bigger(int a, int b)
{
    return (a > b) ? a : b;
}

void getMaxSlice(int arr[], int n)
{
    int max_ending = 0;
    int max_slice = 0;
    for (int i = 0; i < n; i++)
    {
        max_ending = bigger(0, max_ending + arr[i]); 
        //magic, it actually reset the max_ending to value 0 if any subarray sum is less than 0.
        max_slice = bigger(max_slice, max_ending);
    }
}

bool GetTapeEquibriumPoint(int arr[], int n)
{
    int sum = 0;
    for (int i = 0; i < n; i++)
    {
        sum += arr[i];
    }

    int tempSum = 0;
    for (int i = 0; i < n; i++)
    {
        if (sum - tempSum - arr[i] == tempSum)
        {
            return true;
        }
        tempSum += arr[i];
    }
    return false;
}

multimap<int, int> Meetings;
multimap<int, int> Results;
void createMeetings(void)
{
    while (Meetings.size() < 1000)
    {                             //create 1000 meetings with start time in mins
        int s = RndEng() % 1441; //can change to 24*60+1, become minutes!!! awesome
        int e = RndEng() % 1441;
        if (e > s)
        {
            Meetings.emplace(e, s); //sorted by end time
        }
        if (s > e)
        {
            Meetings.emplace(s, e);
        }
    }
}

//arrange as much possible meeting in one room.
void GetMaxMeetingInOneDay(void)
{
    Results.insert(*(Meetings.begin()));
    int end = Meetings.begin()->first;
    for (multimap<int, int>::iterator it = Meetings.begin(); it != Meetings.end(); it++)
    {
        if (it->second > end)
        { 
            Results.insert(*it);
            end = it->first;
        }
    }
}

int GetFirstNoneRepeativeChar(void)
{
    char c = 0;
    int result = -1;
    vector<int> v1(58, 0); //26+ 6+26=58 chars. from A->z
    int i = 1;
    while (c >= 0 && c != 0x5f)
    { 
        std::cout << "value: ";
        std::cin >> c;

        if (!isalpha(c))
            return -1;
        
        switch (v1[c - 'A'])
        {
            case 0:
                v1[c - 'A'] = i; //i could be 1 to any positve number, i is keep incremental , the position of the char
                break;
            case -1: //alrady multiple time
                break;
            default:
                v1[c - 'A'] = -1;
                break;
        }
        i++;
    }
    //find the smallest positive number in the v1. and the number's position +'A' will be the result;
    int Pos = INT_MAX;
    for (int i = 0; i < 58; i++)
    {   //>0 to take out those multiple char, and non-existing char.
        if (v1[i] > 0 && v1[i] < Pos)
        {
            Pos = v1[i];
            result = 'A' + i; //the char
        }
    }
    return result;
}


int main(int argc, char *argv[])
{
    {
        //const int n = 9;
        vector<int> vec1(10);
        generate_vector(vec1, false);
        subArraySumEqual(vec1, 23);
    }

    int arr2[] = {0,5,6,7,2,2,0,1,5,20}; //execpted {5,6,7,2}
    int n2 = sizeof(arr2) / sizeof(arr2[0]);
    cout << subArraySumGreater(arr2, n2, 19) << endl;

    int arr3[] = {9, 1, 16, 36, 25};
    int n3 = sizeof(arr3) / sizeof(arr3[0]);
    isTriplet(arr3, n3) ? cout << "Yes" : cout << "No";
    cout << endl;

    int arr4[] = {-9, 2, 1, -3, 5, 3, 2, -3, 3, 0, 6, 7, -6, -7, 8, 9, 10, 11};
    int n4 = sizeof(arr4) / sizeof(arr4[0]);
    getMaxSlice(arr4, n4);

    int arr5[] = {-9, 4, 0, 1, 3, 2, -11, 5};
    int n5 = sizeof(arr5) / sizeof(arr5[0]);
    GetTapeEquibriumPoint(arr5, n5);

    createMeetings();
    GetMaxMeetingInOneDay();

    GetFirstNoneRepeativeChar();

    return 0;
}

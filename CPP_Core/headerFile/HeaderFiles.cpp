//c# and java don't have header file.
#include "iostream" //c with .h, c++ no h.
#include <string>   //angular bracket search in default include path.
#include "log.h"    //quote normally relative to current file, quotes is more versitle, see above "iostream"
//error C1208, C means Compile error;
//error LNK1516, LNK, linking error;
void Log(const char *message)
{
    std::cout << message << std::endl;
}

//since this case no header file, so we put the duplicate declaration here, so this translation unit
//aware there are defination(implemnentation) of the function somewhere in other translation unit (.o file from the math.cpp)
//if anyhow declear one below "spell wrong" function (no impl), still compile and run without error.
int multiplerrr(int a, int b);
int divide();
//void Log2(const char* message); //since Log2 is static and internal to Math.o only. so can not make it at linking stage.


int main()
{
    banlists b;
    auto banBids = b.getBanBids(); //get ref,
    banBids.push_back("abcd"); 
    b.setBanBids(banBids); //pass in same ref, 

    cout << anExternVariable << endl;
    cout << aStaticVariable << endl;
    aStaticVariable = 90;
    anExternVariable = 9;
    InitLog();
    Log("Hello world!");
    divide();

    //Log2("Math.cpp without hpp got static func log2");
    initLogStatic(); //(an static function defined in a .hpp/cpp pair, it can be built successfully).

    return 0;
}

#pragma once 
//head guard, supported by most popular compiler gcc,clang single include in a single traslation unit..it can be inlcude in multiple translation unit. 
//it mainly used in the chains of include, which end up include certain header file multiple time in a single translation unit.
#include <mutex>
#include <vector>
#include <string>
#include <iostream>
using namespace std;

void Log(const char* message);
//it is allowed in one translation unit, got multiple same declaration, but only one defination (with body) 
void InitLog(void);
//void InitLog(void);

//then static/or inline is the only options to avoid duplicated symbol (func) at linking stage.
//this will create two different internal (for their own translation unit) linkage versions 
//of common_func for log.cpp and headerFiles.cpp respectively
static void initLogStatic(void) {
    cout << "print out static function in a .hpp/cpp pair" << endl;
}

//This is because,when ever you declare a class then,you are declaring a structure for the specific instances of that class,
//BUT in case of static variables in a class,they are the one which can be initialized before any object of the class is created. 
//they need exact size (memory) for the compiler, so got to defined in (.cpp) as well.
class banlists {
public:
    vector<string> getBanBids(void){
        unique_lock<mutex> u(m_mutex);
        return m_banBids;
    }
    void setBanBids(vector<string>& banBids) {
        unique_lock<mutex> u(m_mutex);
        m_banBids = banBids;
    }
private:
    mutex m_mutex;
    vector<string> m_banBids;
    static int m_instCnt;
};

static long aStaticVariable=100;

extern long anExternVariable;
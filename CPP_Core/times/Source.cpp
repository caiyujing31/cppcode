#include "getExecutionTime.h"

/*
clock:
1. system_clock: current time according to system. system_clcok can set by user one, so can adjust forward 1hr, actually not one-hour pass yet!! --NOT stedy!

2. steady_clock: goes at a uniform rate

3. hight_resolution_clock: provides smallest possible tick Period
*/
void distributions(void) {
    long seeds = steady_clock::now().time_since_epoch().count();
    default_random_engine eng3(seeds); //eng3()%6 only provide UNIFORMed distribution

    uniform_int_distribution<int> distr(0, 5); //eng not here
    for (int i = 0; i < 100; i++) distr(eng3); //eng is used when generate number.

    normal_distribution<double> distrN(10.0, 3.0);
    for (int i = 0; i < 100; i++) distrN(eng3);
}

int main(int argc, char* argv[])
{

    //template <class _Clock, class _Duration = typename _Clock::duration>
    //struct steady_clock {
    //    using time_point = time_point<steady_clock>;

    //template <class _Clock, class _Duration = typename _Clock::duration>
    //class time_point { // represents a point in time
    //public:
    //    using clock = _Clock; //can be system_clock, steady_clock, or high-resolution_clock
    //    using duration = _Duration; // the duration type used to represent the time point. it point to template type _Clock's memeber type duration!
    //    using rep = typename _Duration::rep; //the return type by duration::count.
    //    using period = typename _Duration::period;  //the length of period in seconds. eg 1/1000 000 000 second

    steady_clock::time_point t1 = steady_clock::now();


    this_thread::sleep_for(2s);
    steady_clock::time_point t2 = steady_clock::now();
    //    template <class _Rep, class _Period>
    duration<double> time_spans = duration_cast<duration<double>>(t2 - t1);
    cout << "it takes me " << time_spans.count() << "second" << endl;
    duration<double, ratio <1,1000>> time_spans_ms = duration_cast<duration<double>>(t2 - t1);
    cout << "it takes me " << time_spans_ms.count() << "milli, second" << endl;
    
    steady_clock::time_point::duration time_spans_xs = duration_cast<duration<long long>>(t2 - t1);
    cout << "it takes me " << time_spans_xs.count() << "nano second" << endl;

    //steady_clock::time_point::duration d1;  == steady_clock::duration d1; //" based on defination
    //using duration = nanoseconds;
    //using nanoseconds  = duration<long long, nano>;
    //using nano  = ratio<1, 1000000000>;

    cout << system_clock::period::num << "/" << system_clock::period::den << endl; //nominator, and denominator
    microseconds mi(270);                                //270 msec
    nanoseconds na = mi;                                // na=270 000 nanosecond
    milliseconds mil = duration_cast<milliseconds>(mi); // high res to low, need cast! = 2msec, truncated!
    mi += mil;                                            //200+270=470
    this_thread::sleep_for(mi);
    distributions();


    //get date, week day info. ONLY System_clock time has a relationship with the civil calendar.
    system_clock::time_point t3 = system_clock::now();
    time_t tt1 = system_clock::to_time_t(t3);

    //tm utc_tm;
    //gmtime_s(&utc_tm, &tt1);
    
    tm local_tm;
    localtime_s(&local_tm,&tt1);
    
    cout << "year:" <<local_tm.tm_year + 1900 << endl;
    std::cout << "month:" << local_tm.tm_mon + 1 << endl;
    std::cout << "day:" << local_tm.tm_mday << endl;
    std::cout << "weekday:" << local_tm.tm_wday << endl;

    local_tm.tm_mday++;
    local_tm.tm_hour = 18;
    time_t tt2 = mktime(&local_tm);

    system_clock::time_point t4 = system_clock::from_time_t(tt2);
    cout << "there is only " << duration <double, ratio<3600, 1>>(t4 - t3).count() << " hours left";

    cout << currentDateTime();

}
#include "getExecutionTime.h"
#include "classes.h"
#include "card.h"
#include "Sampleclass.h"

using namespace CaiUtilities;

void classInitializatonList(void);
void classFuncOverShadowing(void);
void DemoOfBase_Derived(void);
void shallowCopy_Ownership(void);
void classInstantiate(void);
void fourDefaultFuncs(void);
void ctorDstrThrowExcp(void);
void ClassConstDemo(void);
void classCasting(void);
void polymorphism2(void);
void ObjectSlicing(void);
void koenig_lookup(void);
void Singleton_Design(void);
void Factory_Design(void);
void playCards(void);
void can_copy_derived_test(void);
void equalOpForDerivedClass(void);
void operators(void);

int main(int argc, char* argv[]) {
    {
        vector<A> VA;    
        VA.push_back(A());
    }

    classInitializatonList();
    classFuncOverShadowing();
    DemoOfBase_Derived();
    classInstantiate();
    shallowCopy_Ownership();
    fourDefaultFuncs();
    ctorDstrThrowExcp();
    ClassConstDemo();
    classCasting();
    polymorphism2();
    ObjectSlicing();
    koenig_lookup();
    Singleton_Design();
    Factory_Design();
    playCards();
    can_copy_derived_test();
    equalOpForDerivedClass();
    operators();
    return 0;
}

void classInitializatonList(void) {
    cInitList c(10);
    cInitList c1;
    cout <<c.str_sz<< endl;

    double d = 10.0;
    c(d); //functor with double
    c();  //functor without anything
    if (c)
        cout << "c's val is > 9" << endl;

}

void classFuncOverShadowing(void) {
    BB b;
    
    b.print(); //it will print out B. no polyphism here.

    b.AA::print(); //explicitely calling base's version.
    
    b.print1("abc"); // print1 are two diff signature functions as in AA and BB.
    
    //b.print1(1); //cannot compile, the AA::print1(int) is overshadowed by the B::print1(string). not availabe, not been inheritaed.
    
    b.AA::print1(1);

    //put it simple, when cast either with ref or pointer, as long as the called func is not virtual then No Polymophism happen
    AA& a = static_cast<AA&>(b);
    
    a.print(); // print() is not virtual, so print out 'A'
    
    a.printVir(); //it will print 'B vir'

    AA* aP = dynamic_cast<AA*>(&b);

    aP->print();

    aP->printVir();
    
    AA a2;

    cout << sizeof(AA) << endl;
    
    BB b2;
    
    cout << sizeof(BB) << endl; 

    D1* p1 = new D1();
    double d1 = 2.1;
    p1->f(d1);
    string s1 = "d1";
    p1->f(s1);
    int i1 = 2;
    p1->f(i1); //this will not invoke f(int) if B::f(int) missing in D1 (cannot overload crosing classes, even though they are derived)
    delete p1;
}

void DemoOfBase_Derived(void) {
    {
        vector<unique_ptr<AA> > VMAP;
        VMAP.emplace_back(new AA());
        VMAP.emplace_back(new BB());
        VMAP.back().reset();
    }

    assert(B1::gettotalInstance() == 0);
    D1 d1;
    d1.setmX1D(200);
    d1.mX2 = 300;
    d1.setx1(4); //d1 has mX1 (B1's private member data), but d1 does NOT able to access it even in d1's function (print info). must through B1's member function interface.
    cout << "d1.myx1_child_ToFloat(): " << d1.mX1D_ToFloat() << endl;
    d1.printInfo();

    D2 d2a;
    D2 d2b;
    d2a.mX2 = 100;
    if (d2a < d2b)    cout << "d2a is smaller than d2b" << endl;
    if (d2a > d2b)    cout << "d2a is bigger than d2b" << endl;
    D2 d2c = d2a * "new d2"s; //since D2(string s)is not explicit, so implicate calling ctor, operator*(d2a, D2("new d2"));
    D2 d2d = "new d2"s * d2a; //IF the * is member function, this implicat casting will NOT happend, code error.
    cout << B1::gettotalInstance() << endl;
}

void shallowCopy_Ownership(void) {
    //vector<Birthday_dll> v_Birthday;
    //v_Birthday.push_back(Birthday_dll(11,11,1199));
    //1. an Birthday_dll object is created, 
    //2. vector push_back invoke shallow-copy func of the class (default),
    //3. the actual object is deleted, which also deleted the memory where member pointer "abc" point to, 
    //hence *abc not-determined, ... that is before C++11, SOLUTIONS, use pointers: 

    vector<Birthday_dll*> vec_p;
    vec_p.push_back(new Birthday_dll(11, 12, 1999));
    vec_p.back()->printDate();
    //... after use the vec_p finished. need free the pointer memory... or else use smart pointer.
    for (vector<Birthday_dll*>::iterator it = vec_p.begin(); it < vec_p.end(); it++)
        delete *it;

    // People will took ownership of the Birthday_dll pointer. The auto/stack variable bd1 will deleted (stack pointer variable gone) when out of scope, 
    //the People destructor will call (delete pointer). memory freed here.
    {
        Birthday_dll* bd1 = new Birthday_dll(12, 28, 1986);
        People_dll buckyRoberts("Bucky", bd1); //the best is put the new call in the class's ctor!!
    }

    {   //pass ownership 
        Birthday_dll* p = new Birthday_dll(10, 8, 1983);
        unique_ptr<Birthday_dll> bd2(p);
        People_dll Cai1("Cai", bd2.release());
        Cai1.printInfo();
    }

    //when buckyRoberts out of scope, the bd1 pointer's pointered memory deleted, which is on stack bd1.
    //when stack object bd1 out of scope, it attempted to delete the portion of memory again
    //{
    //    Birthday_dll bd1(12, 28, 1986);
    //    People_dll buckyRoberts("Bucky", &bd1);
    //}
}

void classInstantiate(void){
    assert(B1::gettotalInstance() == 0);

    vector<B1> vB;
    vB.emplace_back();//B1();
    vB.emplace_back(9); //B1(int var)
    vB.emplace_back("abc");
    
    vector<unique_ptr<B1> > vUPB;
    vUPB.push_back(make_unique<B1>());//no need move(...), it is already move bcos it is temporary object.
    vUPB.push_back(make_unique<B1>(9));
    vUPB.push_back(make_unique<B1>("abc"));
    
    vector<B1*> vPB;
    vPB.push_back(new B1());
    vPB.push_back(new B1(9));
    vPB.push_back(new B1("abc"));

    B1* p_arr = new B1[2];    
    delete[] p_arr;
    {
        //B1 C1a(); // this is WRONG, it is treated as FUNC call declaration..C1a is OBJECT name (IT IS NOT CLASSNAME) with empty argument list.. wrong!!!
        B1 C2b = B1(9); //CLASSNAME
        B1 C1b; // B1 C1b = B1();
        B1 C2a(9);
        
        B1& C3 = C2b * C2a; //if B1 C4 = C2b * C2a, it will crashed. bcos * operator return B1&
        delete &C3;

        B1* p = new B1();
        delete p;

        p = new B1;
        delete p;

        p = &C2b;
        p = nullptr;

        B1 C4 = "abcd"s;//implicate casting....calling ctor taking "string s" as arugment
        B1 C5 = 19; //implicate casting.... try avoid this. make the ctor func signature as "Explicite" wherever possible
        B1 C6 = static_cast<B1>(19);
    }

    for (B1* a : vPB) {
        delete a;
    }
    cout << B1::gettotalInstance() << endl;
}

void fourDefaultFuncs(void)
{
    A a2(9);
    A a3(9, 100);
    A a4(a2);
    A a5 = a2; //RUN copy constructor if A3 is just declared, and Skipped Assignment..
    a5 = a2;//RUN Assignment.
    //swap(a2, a3); //it is consistent with other data type swap.that is the reason with the wrapper.
    a2.swap(a3);
}

void ctorDstrThrowExcp(void){
    try {
        //B B1(202);
        B B2(203);
        B B3(200);
        B B4(201); 
    }
    catch (const invalid_argument& e) {
        cout << "exception thrown at constructor:" << e.what() << endl;
    }
}

void ClassConstDemo(void) {
    assert(B1::gettotalInstance() == 0);
    B1 b1;

    //if the func defined as int getx1(){...} still can call  with "const int& b1x1 = b1.getx1(),",, 
    const int& b1x1 = b1.getx1(); 

    int i = 10;
    b1.setx1(i);
    b1.setx1(5); 
    
    const B1 b1c;
    //cosnt B1 will call the const version
    cout << b1c.getx1() << endl;

}

void classCasting(void) {
    assert(B1::gettotalInstance() == 0);
    cout << "\n\n===================Casting / C++ type ID demo===================" << endl;
    B1 b1;
    D1 d1;
    B1* ptr = &d1;
    D1* ptrD = static_cast<D1*>(ptr); //if you already konw it can cast, then just go ahead and cast
    B1& ref = d1;
    cout << typeid(b1).name() << endl;  // B1 
    cout << typeid(d1).name() << endl; // D1
    cout << typeid(ptr).name() << endl;   // B1* 
    cout << typeid(*ptr).name() << endl;  // D1 (looked up dynamically at run-time because it is the dereference of a pointer to a polymorphic class)
    cout << typeid(ref).name() << endl;   // D1 (references can also be polymorphic)
    
    // 1. static_cast, good for Native data type casting, and with low risk; pointer or non-pointer, both ok.
    //    it is templated function. <> and ();you know data type, so safe to use Static Cast
    //    eg: void func(void* data){c_MyClass* c = static_cast<c_MyClass*>(data); ...} 
    int i = 9;
    float f = static_cast<float>(i); f++;                                         

    // 1a. implicit casting.
    //B1* p1 = new D1();    //up_cast, all good.
    //D1* p1_dog = new B1(); // 2 .TRUE down_cast;  create something out of nothing.. Segmentation fault

    //dynamic casting, actually if make all f as virtual functions, we can eliminate the dynamic casting, always good for C++ code.
    B1* p_b2;
    if (rand() % 2 == 0) {//never know what actual derived type it is
        p_b2 = new D1();
    }
    else {
        p_b2 = new D2();
    }
    D1* p_d1 = dynamic_cast<D1*>(p_b2); //if p_b2's pointed object is D1, it returns a D1 pointer; else  nullptr.
    //D1& r_d1 = dynamic_cast<D1&>(*p_b2); //same as pointer cast, can preserver the D1 object....
    //D1 d1 = dynamic_cast<D1>(*p_b2); //slice happen, bad
    D2* p_d2 = dynamic_cast<D2*>(p_b2);
    if (p_d1) //nice feature about dynamic cast, can check cast results, if a valid pointer, do action, else skip
        p_d1->f(double(10.2));
    if (p_d2)
        p_d2->f((float)10.2);
    delete p_b2;


    const B1* p3 = new B1();
    //B1* p4 = p3; //compile error,constness NOT match
    B1* p4 = const_cast<B1*>(p3); //const_cast: only works on pointer/reference, and only on Same type, to cast AWAY constness of pointed object                                     
    delete p3;

    // reinterpret_cast:cast one pointer to any other type of pointer. too risky.. do NOT touch! it is Ultimate Cast, 
    // which discards all kind of type safety, bassically re-assigning the type informtion of the BIT pattern, Equal to C language type cast.
    //int i=100; B1* p=reinterpret_cast<B1*> i;
}

void polymorphism2(void){
    assert(B1::gettotalInstance() == 0);
    B1* base = new D1;
    base->method();
    delete base;

    B1* dP = new D1;
    dP->baseMethod();
    delete dP;


    D1 cFred("Fred"), cTyson("Tyson"), cZeke("Zeke");
    D2 cGarbo("Garbo"), cPooky("Pooky"), cTruffle("Truffle");
    B1* apcAnimals[] = { &cFred, &cGarbo, &cPooky, &cTruffle, &cTyson, &cZeke };
    for (int i = 0; i < 6; i++) {
        cout << apcAnimals[i]->ObjName << " says 'I am calling a ' ";
        apcAnimals[i]->method();
    }

    Report(cGarbo);
    cGarbo.iWait();//want to call base's protected "wait" function, thourhg 

    apcAnimals[0]->methodWithDefault("d1 objects");
    //"B1 Dflt", instead of "D1 Dflt", fixed (default, even if virtual func) parameters is bind-ed in compile time;  
    //while virtual func are bind-ed at run time; so it invoked D1's methodWithDefault(...) but using parameter abc="B1". 
}

void ObjectSlicing(void) {
    cout << "\n---------------- Object Slicing -------------" << endl;
    deque<B1> d1;
    D1 Wolf1("Wolf1");
    d1.push_front(Wolf1);//copy construcotr of B1.... so the object will be B1, all the Wolf parts are Sliced Away.
    d1[0].printInfo();

    deque<B1*> d2;
    D1 Wolf2("Wolf2");
    d2.push_front(&Wolf2);
    d2[0]->printInfo();
}

void koenig_lookup(void) {
    A_NS::X x1;
    g(x1);
    //though g(x1) is not defined in current (main) or global scope, 
    //compiler will auto expand searching scope for g(x1), in namepace A_NS. 
    C_koenig c1;
    c1.g(x1);
    c1.j();
}

void Singleton_Design(void) {
    cout << CSingleton::Instance().GetString() << endl;
    CSingleton::Instance().SetString("Skeleton_string_new");
    cout << CSingleton::Instance().GetString() << endl;
}

void Factory_Design(void) {
    vector<B1*> roles{ makeObj(0),makeObj(1),makeObj(2),makeObj(3) };
    for (unsigned int i = 0; i < roles.size(); i++){
        roles[i]->printInfo();
        delete roles[i];//without this line //valgrind --leak-check=yes ./design_create_factory_exe 
    }
    cout << roles.size(); //size will be 4 still.
}

void playCards(void) {
    deckOfCards deck;
    deck.shuffle();
    for (int i = 0; i <= 100; i++){
        deck.dealCard()->print();
    }
}

void can_copy_derived_test(void) {//constrains check, at compile time. awesome. Can....
    Derived_from<D1, B1>();
    //Derived_from<int, X>();//got compilation error, good
}

void equalOpForDerivedClass(void) {
    B1* one=new B1(10);
    B1* two = new D1("D1");
    B1* three = new D2("D2");
    B1* four = new D1("D4");
    if (*two == *four)
        cout << "two equal to four" << endl;

    vector<B1*> v{one,two,three,four};
    B1* five = new D2("D5");
    for (auto it = v.begin(); it != v.end(); it++) {
        if (**it == *five) {
            cout << "found" << endl;
        }
    }

    for (auto& a : v) {
        delete a;
    }
}

void operators() {
    cInitList c2(11);
    cInitList c3(7);
    c3 = c2;
    c3();
    c2(2.5);
   // bool b1 = c2.operator bool() ;
    if (c2) {
        cout << "c2 is smaller than 10" << endl;
    }
}
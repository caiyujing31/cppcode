#pragma once
//#include "getExecutionTime.h"

#include <iostream>
#include <sstream>
using namespace std;
void SampleClassTest1(void);
void SampleClassTest2(char* c);

 namespace CaiUtilities
{

    class Birthday_dll {
    public:
        Birthday_dll();

        Birthday_dll(int m, int n, int y);

        ~Birthday_dll()
        {
            delete abc;
        }

         Birthday_dll(const Birthday_dll& a) {
             //this is a deep copy... shallow copy will copy a pointer and not allocte new heap for new object, and caused tons of issue .(double delete)
             //or first deleted, second want to access again, memory violation.
            abc = new string(*(a.abc));
        }

         const Birthday_dll& operator=(const Birthday_dll& a) {
            *abc = *(a.abc);
            return *this;
        }

         void printDate() {
            cout << *abc << endl;
        }

    private:
        //declear copy/assignment func will overriden the default (compiler) one, 
        //make it private, so the main can NOT call. Effectively Disable it.
        //Birthday_dll & operator=(const Birthday_dll&) {}
        std::string* abc;
    };

    class  People_dll {
    public:
        People_dll(std::string x, Birthday_dll* bo) : name(x), dateOfBirth(bo) {}
        void printInfo();
        ~People_dll() {
            //delete dateOfBirth; // will have memory leak when taken out.
        }
    private:
        std::string name;
        Birthday_dll* dateOfBirth;//composition
    };

    class SimpleMathOps
    {
    public:
        // Returns a + b  
        static  double Add(double a, double b);

        // Returns a * b  
        static  double Multiply(double a, double b);

        static const int status1 = 99;

        int  get_prev(void) {
            return previous_op;
        }

        int current_op = 1;
    private:
        int previous_op = 0;
    };
}
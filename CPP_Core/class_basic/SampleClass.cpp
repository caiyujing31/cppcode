#include "SampleClass.h" 

namespace CaiUtilities
{
    Birthday_dll::Birthday_dll() {
        std::stringstream ss_abc;
        ss_abc << "01" << "---" << "01" << "---" << "2000";
        abc = new std::string(ss_abc.str());
        cout << "Birthday_dll()" << endl;
    }

    Birthday_dll::Birthday_dll(int m, int n, int y) {
        std::stringstream ss_abc;
        ss_abc << m << "---" << n << "---" << y;
        abc = new std::string(ss_abc.str());
        cout << "Birthday_dll(int m, int n, int y)" << endl;
    }

    void People_dll::printInfo() {
        cout << name << "was born on ";
        dateOfBirth->printDate();
    }


    double SimpleMathOps::Add(double a, double b)
    {
        /*vector<int> v; v[10] = 10;*/
        return static_cast<double>(static_cast<int>(a) + static_cast<int>(b));
    }

    double SimpleMathOps::Multiply(double a, double b)
    {
        return static_cast<double>(static_cast<int>(a) * static_cast<int>(b));
    }
}
void SampleClassTest1(void) {
    std::cout << "func in dll source code dllmain.cpp" << std::endl;
    return;
}

void SampleClassTest2(char* c) {
    std::cout << "func in dll source code SampleClass.cpp. " << c << std::endl;
    return;
}

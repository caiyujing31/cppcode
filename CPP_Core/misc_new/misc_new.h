#pragma once
#include "getExecutionTime.h"
#include "UDL2.h" //demo of c++11 constexpr
#include "classes.h"
constexpr int sz = 100;
std::array<long, sz + 1> FibV;

constexpr long FibDyn(int indx) {
    if (indx == 0)
    {
        FibV[indx] = 0;
        return 0;
    }

    if (indx == 1)
    {
        FibV[indx] = 1;
        return 1;
    }

    if (FibV[indx] != -1) {
        return FibV[indx];
    }

    FibV[indx] = FibDyn(indx - 1) + FibDyn(indx - 2);
    return FibV[indx];
}

//***************** variadic Template func1 ****************************//
void println(ostream& out){
    out << "\n";
}

template <typename T1, typename... Tail>
void println(ostream& out, T1 const& t1, Tail const&... tail){
    out << t1;
    if (sizeof...(tail))
        out << ", ";
    println(out, tail...); //skip the t1, since it is processed.
}

//***************** variadic Template func2****************************//
double sum(void){
    return 0; //like recursive funcs call, got to provide exit call.
}

template <typename H, typename... T>
double sum(H h, T... t){
    return h + sum(t...); //t... will expand to j2,j3,j4 
}

//***************** variadic Template func3 with perfect forwarding.****************************//
struct cWithManyCtor
{
    int i;
    string s;
    cWithManyCtor() : i(-1) {}
    cWithManyCtor(int& _i) : i(_i) {}
    cWithManyCtor(string&& _s) : s(move(_s)) {}
    cWithManyCtor(string& _s) : s(_s) {}
};

vector<cWithManyCtor> vc;

template <typename... T>
void add2Vec(T&&... args)
{
    vc.emplace_back(forward<T>(args)...);
}

template <typename... T>
void createTuple(T... a2) {
    tuple<T...> t1{a2...}; //T... expand to types only (comma separated)
    for (int i = 0; i < tuple_size<tuple<T...>>::value; i++)
        cout <<i<< endl;
}

template <typename... T>
void createTuplePerfect(T&&... a2) {
    tuple<T...> t1{ forward<T>(a2)... };
    for (int i = 0; i < tuple_size<tuple<T...>>::value; i++)
        cout << i << endl;
}

void Tuple_Explore(void) {
    tuple<int, string, char> t1 = make_tuple(1, "aa", 'a');
    tuple<int, string, char> t2 = make_tuple(1, "bb", 'g');
    cout << get<0>(t1) << " " << get<1>(t1) << " " << get<2>(t1) << endl;
    get<0>(t1) = 2; //by ref
    string& s = get<1>(t1);    s = "bb";//by ref
    get<2>(t1) = 'b';// by ref
    if (t1 < t2) //great feature of tuple, comparetor defined!!! unlike struct...
        cout << "t1 <t2, after lexicographical comparison, ie, item1 compare, item 2 compare..." << endl;
    t1 = t2; //member by member copy..

    int x; string y; char z;
    tie(x, y, z) = t2; cout << x << "-" << y << "-" << z << endl;     //great feature of tuple, can do quick data swap.

    string tmp2 = "abcdefg";
    tuple<string&> t3(tmp2); //METHOD3 initializatoin

    //very important features!!
    auto t4 = tuple_cat(t2, t3); //t4 will be a tuple<int, string, char, string&) ..
                                 //type traits.
    cout << tuple_size<decltype(t4)>::value << endl;
    tuple_element<1, decltype(t4)>::type d9; //d2 is string!! the 2nd one!
}

//***************** contract ****************************//
//typename<typename Vec>
//using Scaler = std::decay<decltype(Vec()[0])>::type;
//
//template<typename Vec>
//    requires std::is_floating_point<Scaler<Vec>>
//auto norm(const Vec& vec) -> Scaler<Vec>
//{
//
//}

template <typename T>
auto get_value(T t) {
    //c++17 enable constexpr if. nice
    if constexpr (std::is_pointer_v<T>)
        return *t;
    else
        return t;
}

struct s {
    int id;
    string name;
    double weight;
};

struct sNew
{   //it could be quite useful for .hpp file with struct/class with static member data,
    //usually it need to be defined in the corresponding cpp file, now can be in the same hpp file.
    inline static const int sValue = 777;
};

template <auto value> void f() { cout << value; }

static uint32_t s_AllocCount = 0;
void* operator new(size_t size)
{
    s_AllocCount++;
    std::cout << s_AllocCount << endl;
    return malloc(size);
}

std::optional<string> readStringAsFileName(const string& filepath)
{
    ifstream fs(filepath);
    if (fs)
    {
        string res;
        res = "read first line of the file"; //fs read...
        fs.close();
        return res;
    }
    return {};
}

void printname(string_view sv)
{
    cout << sv << endl;
}


////*******************************user defined literal and constexpr****************////
constexpr long double operator"" _cm(long double  x) { return x * 10; }
constexpr long double operator"" _mm(long double  x) { return x; }

constexpr double pow_int_cpp11(double base, int exp) {
    return (exp == 0) ? 1.0 : base * pow_int_cpp11(base, exp - 1);
}

constexpr double wrapper_pow_int_cpp11(double base, int exp) {
    if (exp > 10 || exp < -10)
        throw range_error("abs(exp) shall be less than 10");

    if (base == 0)
        throw invalid_argument("base can not be zero");

    if (exp >= 0) {
        return pow_int_cpp11(base, exp);
    }
    else {
        return pow_int_cpp11(1.0 / base, -1 * exp); //excellent, simplification of math equation.
    }
}

string SpaceName = "file global s";

auto make_lambda(int c) {
    static int a = 0;
    return [=](int d) {
        static int b = 0;
        a++;
        b++;
        return a + b + c + d;
    };
}
#include "misc_new.h"


int main() {

    Tuple_Explore();

    //*********************lambda*********************

    dog d6{ 6, 5 };
    list<int> l1 = list<int>{ 1, 2, 4, 5, 7, 8, 16, 19, -9, 6, 5, 3, 9, 80 };
    list<int> l2;
    int i9 = 5; //capture changed to [=] or [&], it will capture all, if [] means NO capture.
    int j = [&l1, &l2](dog &d6_tmp, int i_tmp) -> int {
        (d6_tmp.age)++;
        return i_tmp + d6_tmp.age;
    }(d6, i9);

    cout << SpaceName << endl; //global
    string SpaceName = "func local variable";
    cout << SpaceName << endl; //local overshadown global.
    auto lamb1 = [=]() { cout << SpaceName << endl; }; //if not specified,simply get the context scope variabl, which is local
    lamb1();
    auto lamb2 = [SpaceName = ::SpaceName]() { cout << SpaceName << endl; }; //if explicitely, can get global.
    lamb2();
    auto k1 = make_lambda(1); //it will call make_lambda but not the lambda func body. so a and b NO increment yet.
    auto k2 = make_lambda(2);
    cout << k1(20) << " " << k1(30) << endl; //it will evaluate from right to left, so 33 = 30+3 (3=1+1+1, 1-a, 1-b, 1-c), and K1(20) will get 25 =20+5 = 20+3+2(a,b incremnt again)
    cout << k2(20) << " " << k2(30) << endl; //k2(30)=38 = 30+5(based on K1(20)+ 3 (1-a,1-b,2-c)...


    auto l1a = [](auto x, auto y) {return x + y; };//lambda with auto parameter
    
    unique_ptr<double> up1(new double(12.5));
    auto l2a = [up2 = move(up1)]{ cout << *up2; }; //lambda capture with move.
    l2a();

    //******************constexpre *********************

    const double pow = wrapper_pow_int_cpp11(2.5, 9);
    static_assert(wrapper_pow_int_cpp11(-2, -4) == 0.0625, "yippe"); //compile time
    vector<double> v9(4, static_cast<int>(pow));

    using namespace literals;
    cout << "Hello, world"s.size() << '\n'; // 's' is built in user literal to convert const char* to string.
    cout << 1.2 + 2.1i << '\n';
    long double height1 = 34.0_cm;//user defined literal(simple),base is mm(millimeter).
    cout << height1 << " \t" << height1 + 13.1_cm << endl;

    //defined in UDL2.h. user defined literal, SI system
    Mass myMass = 80 * kg;
    cout << "my mass: " << myMass.Convert(pound) << " pounds" << endl;
    constexpr Length distance100 = 100 * kilometre;
    constexpr Time time = 2 * hour;
    constexpr Speed sp1 = distance100 / time;
    cout << "100 km in 2 hours: " << sp1.Convert(mile / hour) << " miles/hour" << endl;

    unique_ptr<string> up3(new string("a string pointed by a pointer"));
    string s1("a string");
    cout << get_value(up3.get()) << endl;
    cout << get_value(s1) << endl;

    std::fill(FibV.begin(), FibV.end(), -1);
    FibDyn(sz);

    // ********************* Move semantic*********************
    //cString c1("cai");
    cString c1 = "cai";  //implicite casting... can only cast one level .. lets ay if the ctor is cString(std::string s). then this could will NOT compile, it requires two level casting.
    cString c2("yujing");
    cString c3 = (c1 * c2); //will go to move constructor since the c3 is not initialized (no call move assingment!! ).
    c3.print();
    cout << c1;
    cout << c2;
    cout << c3;

    //*********************Optional*********************
    optional<string> firstLine = readStringAsFileName("file1.txt"s);
    if (firstLine.has_value())
        cout << "it is a file name passed in" << endl;

    string firstLineStr = firstLine.value_or("not file name passed in");//can skip above "if"...

    //*********************variadic template function/classs*********************
    add2Vec();
    int z = 20;
    add2Vec(z);
    string s = "abc";
    add2Vec(s);          //call cWithManyCtor(string& _s) : s(move(_s)) {}
    add2Vec(move(s)); //call cWithManyCtor(string&& _s) : s(move(_s)) {} ; expiring lvalue
    add2Vec("abc"s);  //call cWithManyCtor(string&& _s) : s(move(_s)) {} ; pure rvalue.

    println(cout, "1", 65, 9.2, 'c');

    int j1 = 2;
    float j2 = 2.1;
    double j3 = 3.1;
    unsigned int j4 = 4;
    cout << sum(j1, j2, j3, j4) << endl;

    createTuple(j1, j2, j3, j4);
    createTuplePerfect(j1, j2, j3,"abcdegs"s);


    //*********************misc features********************* 
    //variable binding
    set<char> set1{ 'a','b','c','d' };
    const auto[iter2, ok2] = set1.insert('g');
    if (ok2)
        cout << *iter2 << endl;
    tuple s3{ 1,"number1",1.1 };
    auto[id, name, weight] = s3;

    std::map<int, char> myMap{ {1,'a'},{3,'c'},{8,'h'} };
    for (const auto&[k, v] : myMap)
        cout << k << "--" << v << endl;

    if (int i = 99; i == 99) {
        cout << "c++17 feature, Init-statement for if or switch";
    }

    //inline static member data.
    sNew snew1;
    sNew snew2;
    cout << snew1.sValue << "vs " << snew2.sValue << endl;

    //template class, no need to include type name when instantiation.
    pair p(1.5, "xyz");  //like templated funciton, templated class can also deduce the types, used to be pair<double, string> s(...
    
    f<10>(); //not type template parameter can be "auto"....since c++17

    //string_view (iterator pairs)
    string myname = "ccccyyyyy jjjjj";

    string_view lastname(myname.c_str(), 3);
    string_view firstname(myname.c_str() + 4, 6); //6 is the lengths;
    printname(lastname);
    return 0;
}
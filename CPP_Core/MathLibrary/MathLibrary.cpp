#include "pch.h"
#include <utility>
#include <limits.h>
#include <iostream>
#include "MathLibrary.h"

static ull previous_;
static ull current_;
static unsigned index_;
static ull runUpto_;

void fibonacci_init(const ull a, const ull b, ull runUpto)
{
    index_ = 0;
    current_ = a;
    previous_ = b; // see special case when initialized
    runUpto_ = runUpto;
}

bool fibonacci_next()
{
    if (runUpto_ - previous_ < current_)
    {
        return false;
    }

    if (index_ > 0)
    {
        previous_ += current_;
    }
    std::swap(current_, previous_);
    ++index_;
    return true;
}

// Get the current value in the sequence.
ull fibonacci_current()
{
    return current_;
}

// Get the current index position in the sequence.
unsigned fibonacci_index()
{
    return index_;
}

//do NOT delete below two function. for c# marshalling.

void cppPrintString(char* str, int sz)
{   
    //std::string s(str);
    std::cout << "print out in cpp dll:";
    for (int i = 0; i < sz; i++)
        std::cout << str[i];
    std::cout << std::endl;

}

void cppPrintInt(int i)
{
    std::cout << "print out in cpp dll:" << i;
    std::cout << std::endl;
}

std::string CencryptedString::getOriginal() {
    std::string s2;
    for (auto& a : _s) {
        s2.push_back(a - 1);
    }
    return s2;
}

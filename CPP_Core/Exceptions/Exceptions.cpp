// Exceptions.cpp : Defines the entry point for the console application.
#include "Exceptions.h"
#include "createDump.h"
#include "classes.h"

struct MyException : public exception
{
    const char * what() const throw()
    {
        return "CaiException";
    }
};

double multiples(int a, int b)
{
    if (b < 0){
        //throw invalid_argument("2nd argument cannot be negativee");
        throw inValidArgs("2nd arg can not be 0", SOURCEINFO);
    }
    return (a * b);
}

void func1(B1 b)
{
    try
    {
        b.ObjName = "func1";
        TakeADumpImpl();
        throw out_of_range("func1 exception");
    }
    catch (const bad_alloc &e1)
    {
        cout << "don't do alloc here in func1" << e1.what() << endl;
    }
}

void func2(B1 b)
{
    b.ObjName = "func2";
    func1(b);
}

void func3(B1 b)
{
    try
    {
        b.ObjName = "func3";
        func2(b);
    }
    catch (const out_of_range & e1)
    {
        cout << "out_of_range catched  @ " << __FUNCTION__ << e1.what() << endl;
        throw;
    }
}

int main()
{
    int i = 1;
    try
    {
        try
        {
            i++;
            throw invalid_argument("abc");
            i++;
        }
        catch (const runtime_error& e)
        {
            cout << "cought internal1:" << e.what() << endl;
        }
        
        try
        {
            i++;
        }
        catch (const invalid_argument & e)
        {
            cout << "cought internal2:" << e.what() << endl;
        }
        i++;
    }
    catch (...)
    {
        cout << "cought @ " << __FUNCTION__ << endl;
    }

    try
    {
        int num2 = 55;
        switch (num2)
        {
        case 25:
            throw "exception caught: num2 ==25";
            break;
        case 35:
            throw MyException();
            break;
        case 45:
            throw bad_exception();
            break;
        case 55:
            {//if u going to define a varialbe in the case statement, must use {} to enclose it.
                int i = 10; 
                multiples(0, -1);
            }
            break;
        case 65:
            throw 1000;
            break;
        default:
            cout << "num2 to the end of 'try' block, NO exception raised!" << endl;
        }
    }
    catch (const int &err)
    {
        cout << err << endl;
    }
    catch (const MyException &e)
    {
        //throw by value, catch by ref, if by value (copying), easily got object slicing happen
        cout << e.what() << endl;
    }
    catch (const bad_exception &e)
    {
        cout << e.what() << endl;
    }
    catch (const invalid_argument &e)
    {
        cout << e.what() << endl;
        raise(SIGSEGV);
        //SIGABRT-abnormal termination, SIGFPE-Floating point exception,
        //SIGILL-invalid instruction, SIGSEGM-Segmentation Fault, SIGTERM...
    }
    catch (const range_error &e)
    {
        cout << e.what() << endl;
    }
    catch (...)
    {
        //this catch(...) must be at last.... and able to capture all types of exceptions
        cout << "Unknown Exception! No need deal with it, Just proceed running" << endl;
    }

    try
    {
        B1 b;
        b.ObjName = "main";
        func3(b);
    }
    catch (const std::exception &e)
    {
        // because most of exception derived from std::exception.
        // std::exception_ptr p = std::current_exception();
        cout << "exception catch @ " << __FUNCTION__ << e.what() << endl;
    }
    return 0;
}

﻿#include "misc.h"

int main(int argc, char *argv[])
{    
    accessRegValue();    
    //system (c:\proddump.exe xxx )    

#ifdef TESTDLL
    fibonacci_init(1, 1,10000);
    // Write out the sequence values until overflow.
    do {
        std::cout << fibonacci_index() << ": "
            << fibonacci_current() << std::endl;
    } while (fibonacci_next());
    
    CencryptedString s1("cai");
    cout << s1.getString() << endl;
    cout << s1.getOriginal() << endl;
#endif

    signal(SIGINT, sigIntHandler);

#ifdef TESTMACRO
    // __TIME__,__DATE__,__LINE__,__FILE__,__cplusplus,_CPPLIB_VER,__FUNCSIG__        //string s = __FUNCSIG__;
    cout << "PI_PLUS_ONE*2 =" << PI_PLUS_ONE * 2 << endl;
    
    PRINT_MSG("TESTMACRO is defined");
    cout << MyMax(RndEng() % 10, 5) << endl; //could be bug. hard to fix, RndEng will be called twice in the expanded code during compile time
    auto idkTypeVar = toStrings(CAI YUJING IS HERE!); //macro stringizer

    int i91 = 10;
    string s91 = toStrings(i91); //string with value of "i91", the #operator will prevent the evaluation !!!
    cout << catNum(10, 20)<<endl; //will print 1020 //macro pasting
    cout << catStr(10, 20) << endl; //will print 1020 //macro pasting, with leading space or trailing space of ## , results same
    cout << catStr("s1"s, "s2"s) << endl;

    defclasses(ABC);
    ABC1 abc1;
#endif

#ifdef DefinedInPropertySettingCai
    FUNCMACRO;
#endif

    f_w_o_p(PRINT_MSG);
    funcRojak();

#ifdef TESTENUM
    my_mood = BLUE;
    cout << "my_mood=" << my_mood << endl;
    weekdays MyDay1 = fri;
    MyDay0 = mon;
    if (MyDay1 == 23 && MyDay0 == mon)
        cout << "can NOT assign integer value directly to enum variable, but can used to compare...weird!" << endl;

    apple apple1 = apple::ROSE;
    if (apple1 == apple::GALA)
        cout << "it is a gala apple" << endl;
    //if (apple1==2) //compile error, NO explicite to int!
#endif

#ifdef TESTCONST
    cout << "\n++++++Const PLD Demo +++++\n" << endl;
    const int a1 = 9; //a1=6, c-error. modify "read-only" location

    int a2 = 7;
    const int *p1 = &a2;// (*p1)++ => compile error, modify "read-only" location
    p1++; //const is left of *, means "pointed data" CONST; pointer is Not, 

    int a3 = 8;
    int *const p2 = &a3; //const is right of *, means pointer CONST ,pointed-Data is NOT, p2++ => c-error;
    (*p2) *= 100;         //can modify pointed-data

    cout << "\n++++++Const class Demo +++++\n" << endl;
    Foo const cF1; 
    const Foo cF2;
    //cF1.a = 10; // Foo const, make the member 'a' const int
    //cF1.pA = &cF1.a2; /pA now a "const" pointer to integer,
    *(cF1.pA) += 10;
    //cF1.pcA = &cF1.a2; //pcA now a "const" pointer of const int!!
    //cF1.cpA = &cF1.a2; //cpA is originally a const pointer, after the Object been const, still a const pointer!!. NOT const pointer to const int!
    *(cF1.cpA) += 10;

    Foo F1;
    Foo const *pcF1 = &F1; //const is added to F1 for pcF1.
    Foo* pF1 = &F1;
    printFoo_Const(pF1);
    printFoo_Const(pcF1);
    printFoo_Const(const_cast<Foo *>(pcF1)); 

    std::string firstname = "yujing";
    std::string lastname = "cai";
    //lvalue ref can be implicitely cast to const lvalue ref 
    printName(firstname);

    //if func signature without the const. it will have compile error since here it is rvalue,
    //rvalue can not be converted to reference of lvalue, while it can be conver to refernce to const lvalue (compiler 
    //secretly create const string tmp = firstname+lastname , and call with printName(tmp) for below line.
    printName(firstname + lastname); 

    printName2(firstname);
    printName2(firstname + lastname);
#endif



#ifdef TESTFILESYS
    string path = "C:\\";
    for (const auto& entry : filesystem::directory_iterator(path))
        cout << entry.path() << endl;
    vector<string> files;
    findFileRecursively("C:\\tmp\\", files, "cai");
#endif

#ifdef TESTITER
    cout << "\n++++++iterator Demo +++++\n" << endl;
    list<int> l1 = list<int>{1, 2, 4, 5, 7, 8, 16, 19, -9, 6, 5, 3, 9, 80};
    list<int>::iterator it1 = l1.begin();    
    *it1 = 10;
    list<int>::const_iterator cit_l1 = l1.begin();//const int *, pointed value is const
    //cit_l1 = 10;//compile error
    cit_l1++; 
    const list<int>::iterator it2 = l1.begin(); //int *const
    *it2 = 100;
    advance(it1, 3);
    list<int>::iterator it3 = next(it1, 2);
    list<int> l99;
    copy(it1, it3, back_inserter(l99)); //std::copy(container1.it1, container1.it2, back_inserter(container2)), can be diff type of container, power!
#endif

#ifdef TESTCSTRING
    const char *msg3 = "Cherno";
    //most of time shall be "const char* msg3". hover mouse over to "Cherno", it shows Const Char[7] as type.
    //it is string literal, it stored in read-only memory of a .exe file, stack allocated memory, no need delete
    //char* msg3p = const_cast<char*>(&msg3[1]);  *msg3p = 'f'; //could pass at build, and failed at run! don'st do that 
    const char msg4[6] = {'C', 'h', 'e', 'r', 'n', 'o'}; //normal char array, NOT null-terminated. 
    const char msg5[8] = "Che\0rno"; // strlen will get 3. Che2\0rno\0 in memory.
    const char msg1[] = "Hello"; //normal char array, from string literal, null-terminated (implicitely).
    char msg2[] = {'H', 'e', 'l', 'l', 'o', 0};//normal char array, null-terminated (explicitely). 
    cout << strlen(msg1) << " " << sizeof(msg1) << " " << sizeof(msg1) / sizeof(char) << endl;
    cout << sizeof(msg2) << " " << sizeof(msg2) / sizeof(char) << endl;

    // both below cases the pointer NOT yet point to any memory location, and we want to read/write the value
    // int* P_int1; *P_int1=55;
    // char* c = nullptr (or NULL);  cout<< c[0];
    char *c = new char('a'); delete c;
    char d = 'z'; 
    void* dp = &d; //cout << *(static_cast<char*>(dp)) << endl;

    string s8 = "helloWorld";
    //LPCTSTR s8c = s8.c_str();  const char* s8c =  s8.c_str(); 
    //string s9 = static_cast<string>(s8c);
    char c8 = 'd';
    s8.append(sizeof(char), c8); //or simply s8 += c8;
    s8.append(msg1); //msg1 must be null-terminated if it is c array type.

    const WCHAR* shi = L"斜阳欲落处一望黯消魂";
    wstring shiWStr;
    for (int i = 0; i < 10; i++)//sizeof(shi) / sizeof(WCHAR) == 4?
        shiWStr.push_back(shi[i]);

#endif



    wchar_t wideChar1 = L'a'; //typically takes 2 bytes, to cover all languages.
    using TString = basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >; //same as wstring
    TString wideCharString = L"this is a wide char String";
    wideCharString += wideChar1;
    unsigned long long arr[5] = {11, 22, 44, 21, 41};
    cout << &arr[0] << endl;    // Print address of first element
    cout << arr << endl;        // Same as above; do NOT use &arr. it get the address OF the pointer.. not much useful
    cout << *arr << endl;        // Same as arr[0] (11)
    cout << *(arr + 1) << endl; // Same as arr[1] (22) ,so its '+' been overriden
    cout << *arr + 2 << endl;   // Same as arr[0]+2 =13;
    plusOne(arr, 5);

    // if(NULL==0) //try avoid NULL, it is an integer type with value 0 in c++,
    // volatile int l=100; //preventing Compiler act smart and replace below if to if(True).. //if(l==100)

    //system("clr");
    Test_Func_Param2(1);
    Test_Func_Param2(1, 5);

    help1();
    help2();
    help2();
    help3();

    Cfunctor o1;
    o1("Hi");

    unsigned char a11 = 254;                         //unsigned int , max is only 255, watch out for overflow.
    int new1 = static_cast<unsigned char>(a11 + 10); //new1 is 8. not 264
    //string s7 = "abdeTGHnk";
    //for (auto i = s7.size() - 1; i >= 0; i--) {
    ////auto derived type from right, .size() return unsigned, so unsinged -1 still unsigned, 
    ////and i will be unsigned, when i=0; and minus -1 , i become huge integer number, will results in segmentation fault.
    //    if (s7[i] > 'i')
    //        cout << s7[i];
    //}

    long long bigNum = 8 << 30;    //eval from right, "8 << 30" (int literal) will evaluted as int, which as 32 bits, could be a bug.
    long long bigNum2 = static_cast<long long>(8) << 32;

    //fork(); //will create an identical "process" to run below three lines of code , fork(), fork(), cout....
    //fork(); // , fork(),  cout....
    //fork();

    //namespace allow to group entities like, objects, functions, variables under one scope.
    cout << "b_NS: a = " << b_NS::a << "\t b_NS: s=" << b_NS::s << endl;

    //auto& bad = min(i20, i23+1); // i23 + 1 is rvalue, can not be used as reference

    int *px = nullptr;
    {
        int i = 10;
        px = &i;
    }
    cout << *px; //unidentified behavior, surprisingly still can print 10, maybe the memory is NOT taken yet.. but do NOT do it...

    int *p20 = new int(1);
    double *pd22 = reinterpret_cast<double *>(p20); 
    //the memory location pointed by p20/p22 expanded from 4 bytes to 8 bytes
    //using delete p20 could potentially leak 4 bytes. try not mess round with object of diff size.
    delete pd22;                                    

    const int i5 = 0;
    p20 = const_cast<int *>(&i5);
    *p20 = 6;

    //object data layout, adrress
    Foo p; //= Foo();
    void *p4 = &p;
    void *p5 = &(p.a);
    if (p4 == p5)
        cout << "nice, good optimizer" << endl;

    cout << " typeid(p4):" << typeid(p4).name() << endl;
    cout <<  "sizeof(p4) :" << sizeof(p4) << endl;
    cout << " sizeof(*p4) :" << sizeof(p) << endl;

    string s6 = "def";
    {
        //outter one. like global vs local!, overshadow the outer one.
        string s6;
        s6 += "abc";
        cout << s6 << endl;
    }
    cout << s6 << endl;

    string(newVarName); //it compiles, equal to "string newVarName;" it is called decoration, can be tricky bugs!!
    unique_lock<mutex>(m1);
    // only RAII type(class) with default constructor will have Decoration!! 
    // proper way is: unique_lock<mutex> M1(m1);

    //quite a number of string allocations..
    vector<char*> v1 = {const_cast<char*>("first const literal string"),
                      const_cast<char*>("second const literal string"),
                      const_cast<char*>(s6.c_str()) };

    //accessRegValue();

    cout << "numeric_limits<std::int8_t>::min:" << (std::numeric_limits<std::int8_t>::min) << endl;

    //int val1 = 100;
    //if (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__) {
    //    cout << "__ORDER_LITTLE_ENDIAN__" << endl; //this is the defaults one
    //    cout << __builtin_bswap32(val1) << endl; //convert default little endian to big endian
    //}
    int i100 = 100;
    bool true1 = true;
    i100 = i100 + true1 ? 2 : -2;//true1 is bool, i100 + true1 will be eval first; it will be 2 , instead of 102.


      //string inFile = generateString(5);
    //string inStr = "A000000B";
    //Write_File(inFile, inStr);
    //Read_File(inFile, inStr.length());

 }
#pragma once
// MathLibrary.h - Contains declarations of math functions
#include <string>
#ifdef MATHLIBRARY_EXPORTS
#define MATHLIBRARY_API __declspec(dllexport)
#else
#define MATHLIBRARY_API __declspec(dllimport)
#endif
using ull = unsigned long long;
extern "C" MATHLIBRARY_API void fibonacci_init(
    const ull a, const ull b, ull runUpto);

extern "C" MATHLIBRARY_API bool fibonacci_next();

extern "C" MATHLIBRARY_API ull fibonacci_current();

extern "C" MATHLIBRARY_API unsigned fibonacci_index();

//do NOT delete below two function. for c# marshalling.
extern "C" MATHLIBRARY_API void cppPrintString(char* str, int sz);

extern "C" MATHLIBRARY_API void cppPrintInt(int i);

class MATHLIBRARY_API CencryptedString {
public:    
    CencryptedString(const std::string& s) {
        for (auto& a : s) {
            _s.push_back(a + 1);
        }
    }
    
    std::string getOriginal();
    std::string getString() {
        return _s;
    }
private:
    std::string _s;
};
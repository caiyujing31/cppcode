#pragma once
#include "getExecutionTime.h"
#include "classes.h"
#include "MathLibrary.h"

#define MyMax(a,b) ( (a<b) ? b :a)
#define TESTDLL
#define TESTENUM
#define TESTITER
#define TESTCONST
#define TESTCSTRING
#define TESTMACRO
#define TESTFILESYS
#define TESTFREEMALLOC

#define toStrings(msg) #msg //conver to string;
#define catNum(a,b) a##b
#define catStr(a,b) (a ## b)
#define defclasses(x) \
    caiclass(x##1) \
    caiclass(x##2) \
    caiclass(x##3)
#define caiclass(y) \
    class y \
    { \
    };

//in C++ coding, lvalue- An object that cocupies some identifiable location/address in memory, otherwise called rvalue.
//preprocessor is invoked at translation unit(sourcefile) before actual compilation happen. Make sure the Name is NOT too short, bcos it is in 
//one single scope, and pls use parenthesis whenever possible.

#define PI_PLUS_ONE 3.14+1 //better use parentheses
#define FUNCMACRO help2(); help2(); help2();
#define int_p_def int* //Define is a preprocessor, the compiler will NEVER see it. Last time C do NOT have const, it use Define to get const.
int_p_def q1, q2, q3; //q2 is not a pointer!
typedef int* int_p_cai; //typedef is a compiler token, the Preprocessor doe NOT care about it, compiler does
int_p_cai p1,p2,p3; //all are pointers

// either one of the following program need to be defined (ie, implemented), proper use as below.
#define func_without_parameter f_w_o_p
void func_without_parameter(void(*func)(const string&));
/*{ //func_without_parameter() can take any number of parameter in C, and means (void) in C++; 
so please explicitly declare parameters are 'void'. }*/

void  f_w_o_p(void(*func)(const string&)) {
    cout << "void  f_w_o_p(void)" << endl; 
    func("abcdefg"); //here the func is variable.
}

std::atomic<bool> running(true);
void sigIntHandler(int param)
{
    //if(running.exchange(false)){...}
    running = false;
    cout << "running is been set true upon receiving of ctrl-c,  wil stop writing soon" << endl;
}

void help1(void) { //with or without (default) extern is same.
    int help1_int = 10;
    help1_int += 2;
}

void help2(void) {
    static int help2_int = 10;     
    // only initilized in the VERY FISRT FUNCTION CALL! just like static member of a class, 
    // for class virtually all the class member can access class static memeber data, 
    // though class static method, might only application for NON instance call.
    help2_int += 2;
}

typedef void(*CFuncPType)(void); //type name, but in the f_w_o_p it used as variable name!!!
CFuncPType funcRojak = &help2;

static void help3(void){
    cout << "void help3(void)" << endl;
}

static int help3_int = 10;
//"static" for free function/variable means this variable like above help2_int,
// is called once (in file scope), Vs func scope of help2_int;
// and this variable will only have internal linkage, means another transaction unit will NOT aware of the existance of this .


namespace b_NS {
    int a = 20;
    string s = "b_NS:: s";
}

inline void PRINT_MSG(const string& s) {
    cout << "PRINT_MSG(const string s):" << s << endl; 
}

void Test_Func_Param1(int a, int b){
    cout << "Test_Func_Param(int a, int b)" << endl; 
}

//Where there is a choice, prefer default arguments over overloading, default Args must on right.
void Test_Func_Param2(int a, int b = 0) { 
    cout << "Test_Func_Param2(int a, int b = 0)" << endl;
}

enum MOODY { EXCITED, MOODY, BLUE, THRILLED } my_mood; //my_mood is a variable. 

//you have no way to get the "name" from the value in C++ because all the symbols are discarded during compilation. 
enum weekdays { mon, tue = 20, wed, thu, fri, sat, sun } MyDay0; //MyDay0 is a variable.

enum class apple :unsigned int { ROSE, GALA, CHESTNUT, GREEN };

class Cfunctor {
public:
    Cfunctor():m(1){}
    void operator()(string a){
        cout << "Cfunctor with argument:" << a << endl;
    }
private:
    int m;    //so can use this variable to remember "state" of the functor. a good choice to pass data around!
};

struct Foo {
    int a = 0;
    int* pA = &a;
    int const * pcA = &a;
    int * const cpA = &a;

    static const int y2 = 10;//cannot be used as lvalue...
    static const int y4 = 20;
    char v4[y4];
    Foo(){}
};
const int Foo::y4;//with this line, Y::y4 can be used as lvalue.

void printFoo_Const(const Foo* pFoo1) {
    cout << " printFooC(const Foo* pFoo1)" << endl;
}
//even though below allowed to compile, but it will never been called. 
void printFoo_Const(Foo* pFoo1) {
    cout << " printFooC(Foo* pFoo1)" << endl;
}

void printName(const std::string& name)
{
    std::cout <<"Only Const lvalue Ref :" << name << std::endl;
}

//even with a const below, it will choose the 2nd overloaded by the compiler .
void printName2(const std::string& name)
{
    std::cout << "[lvalue]:"<<name << std::endl;
}

void printName2(std::string&& name) 
{
    std::cout << "[rvalue]:" << name << std::endl;
}


//They will be treated as int* by the compiler, The size of the array given in [] is ignored, ie 50 will be ignored.
//int max(int*, int);
//int max(int *arr, int size);
//int max(int i[50], int size);
//int max(int arr[], int size);

void plusOne(unsigned long long  arr[], int size) {
    *(arr + 1) += 1;
    *(arr + 4) += 1;
}
#ifdef TESTFREEMALLOC
void* operator new (size_t size) {
    cout << "allocate " << size << endl;
    return malloc(size);
}

void operator delete(void* memory, size_t size) {
    cout << "free :" << size << endl;
    free(memory);
}
#endif

#include "winreg.h"
void accessRegValue() 
{
    //wstring systemInfoPathParentKeyPath = L"Software\\Teradyne\\IG-XL\\10.30.74_uflx\\AllConfig\\Services\\SiteMgr Service\\";  //the target registry entry's parent
    wstring systemInfoPathParentKeyPath = L"SOFTWARE\\Teradyne\\IG-XL\\";
    HKEY systemInfoPathParentKey;
    DWORD error = ::RegOpenKeyExW(HKEY_LOCAL_MACHINE, systemInfoPathParentKeyPath.c_str(), 0, KEY_READ | KEY_WOW64_64KEY, &systemInfoPathParentKey);
    WCHAR systemInfoPathData[1024];
    DWORD dwType = 0; //DWORD, is unsigned (long)
    DWORD dwSize = 1024 * sizeof(WCHAR);;
    //error = ::RegQueryValueExW(systemInfoPathParentKey, L"ShortName", NULL, &dwType, (LPBYTE)systemInfoPathData, &dwSize);
    error = ::RegQueryValueExW(systemInfoPathParentKey, L"CurrentVersion", NULL, &dwType, (LPBYTE)systemInfoPathData, &dwSize);

    ::RegCloseKey(systemInfoPathParentKey);
    wstring result(systemInfoPathData);
}

//can cast anything to 
void DumpToAFile(void* a, size_t bytes) {
    std::ofstream f("dump.bin", std::ios::binary);
    f.write((char*) a, bytes);
    f.close();
    //Test b = *(Test*)a;
}

void Write_File(string& name, string& content)
{
    HANDLE hin;
    hin = CreateFile(name.c_str(), GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, 0);

    ULONG WrittenNum;
    WriteFile(hin, content.c_str(), content.length(), &WrittenNum, NULL);
    CloseHandle(hin);
}

void Read_File(string& name, int toBeRead)
{
    HANDLE hout;
    char* outStr = new char[toBeRead + 1];
    hout = CreateFile(name.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

    ULONG ReadNum;
    ReadFile(hout, outStr, toBeRead, &ReadNum, NULL);
    outStr[toBeRead] = 0;//ending null.
    CloseHandle(hout);
}

void findFileRecursively(const std::string& fullDirPath, vector<string>& foundFiles, const std::string searchStr)
{
    assert(fullDirPath.substr(fullDirPath.length() - 1, 2) == "\\");
    std::string pattern = fullDirPath + "*";
    WIN32_FIND_DATA data;
    HANDLE hFind = FindFirstFile(pattern.c_str(), &data);
    if (hFind != INVALID_HANDLE_VALUE) {
        do
        {
            if ((data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) &&
                (data.cFileName[0] != '.')) //filter out  current (.)  and parent (..)
            {
                findFileRecursively(fullDirPath + data.cFileName + "\\", foundFiles, searchStr);
            }
            string s(data.cFileName);
            if (s.find(searchStr) != string::npos)
            {
                foundFiles.push_back(fullDirPath + s);
            }
        } while (FindNextFile(hFind, &data));

        FindClose(hFind);
    }
}
#include "template.h"

int main(int argc, char *argv[])
{
    double db1 = 11.51;
    double db2 = 11.52;

    if (0.41f - 0.21f == 0.2)
        cout << "true" << endl;

    cout << "smaller:" << smaller(20, 21) << endl;   //literal as int
    cout << "smaller:" << smaller(20l, 21l) << endl; //long
    cout << "smaller:" << smaller(db1, db2) << endl; //double

    print_size<string>("string");
    print_size<double>("double"); // 8 bytes
    print_size<long long>("long long"); // 8 bytes
    print_size<float>("float");  //4 byte
    print_size<long>("long"); //4 byte
    print_size<int>("int"); //4 byte
    print_size<short>("short"); //2 byte
    print_size<char>("char"); // 1 byte

    vector<int> v1 = {3, 4, 5, 6, 8, 6, 9, 19, 13};
    vector<int> v2 = myfilter([](int x) { return (x % 3 == 0); }, v1);    //the template typename can be function type .

    float f5 = CaiTo<float, string>(" 123.4 ");
    char c1 = CaiTo<char, string>("B");
    cout << boolalpha << isEqual(10.1f, 10.2f) << "\t"<<
                         isEqual(static_cast<double>(10.00021), static_cast<double>(10.00022)) << "\t" <<
                         isEqual(10u, 10u) << "\t" <<
                         isEqual(10, 10) << "\t" <<
                         isEqual(10l, 10l) << endl;

    //SFINAR1, try to change the two shared_ptr to unique_ptr, it will NOT allow to compile, since unique_ptr is NOT copiable hence enable_if_t failed.
    vector<shared_ptr<int>> v3;
    v3.emplace_back(new int(100));
    
    v3.emplace_back(new int(101));
    shared_ptr<int> item1 = getFirstCopyValue(v3);

    //SFINAR2. the two commentted out lines,
    vector<int> v4{6, 4, 5};
    vector<string> v5{"ab", "cd", "ef", "gh"};

    cout << boolalpha << same_type<int, float>::value << "\t" <<
                         same_type<unsigned int, size_t>::value << "\t" <<
                         is_integral<long long>::value << "\t" <<
                         same_type<float, double>::value << endl;

    auto bRes0 = findTopItems(v4, 5);
    //bool bRes1 = findTopItems(v4, 5.5);//compile fail, 5.5 is not is_integral<..>
    //bool bRes3 = findTopItems(v5, 5);//compile fail, the vector<string> is not vector<int>

    //SFINAR3. the comment out call, *l1.begin() is the item of container, not an itertor.!
    list<int> l2{1, 2, 3, 5, 6, 8, 9, 0, 12, 14, 15, 18, 17, 18, 17, 6, 8};
    vector<int> v6 = returnValueOf(l2.begin(), l2.end(), [](int &i) -> bool { return i == 17; });

    map<int, string> m1 = { {1,"a"}, {2,"b"}, {3,"c"},{4,"d"} };
    vector<pair<const int, string> > v7 = returnValueOf(m1.begin(), m1.end(), 
            [](const pair<const int, string> &p) { return p.second == "d"; });

    //SFINAE4,
    int i = 99;
    cout << boolalpha << isOdd(i) << endl;
    //cout << boolalpha << isOdd(&i) << endl; //it is a pointer, compile fail!

    //***************** Template Class ****************************//
    Bo<int> b1{2, 5, 8, 6, 9, 19, 13};
    Bo<int> b2 = Bo<int>{1, 4, 7, 5, 8, 18, 12, 11};
    Bo<int> b3 = Bo<int>{1, 4, 11};
    Bo<int> b4(b1);
    Bo<int> b5(move(b2)); //b2 is expiring lvalue, with move, convert to rvalue, if b5(Bo<int>()), then it is called pure rvalue, no need move()..
    b4 = b3;
    b5 = move(b3);

    Bo<int> b6 = b4 * b5;
    b6.print_Bo();
    Bo<int> b7{1, 2, 3, 4, 6};
    
    stdarray<int, 5> array(99); //created on stack instead of heap.
    
    d1<float> d3b;
    d3b.var1 = 10.1;

    d2<vector<int>, map<string, int> > d4;
    d2<int, vector<string>> d4b;
    cout << string(d4b) << endl;
    cout << string(d4) << endl;
    
    string s1 = "my name i cai yujing";
    vector<string::iterator> results = findAll(s1, 'i');

    list<double> l1 = { 12.2, 10.2, 11.3, 14.2, 11.4, 11.3, 11, 8.99, 34.4 };
    vector<list<double>::iterator> findV = findAll(l1, 11.0);

    string::iterator it1 = s1.begin();
    string::iterator it2 = s1.end();
    auto i2 = distance_cai(it1, it2);

    list<double>::iterator it1_l = std::find(l1.begin(), l1.end(), 14.2);
    list<double>::iterator it2_l = std::find(l1.begin(), l1.end(), 99);
    auto j = distance_cai(it1_l, it2_l);

    return 0;
}

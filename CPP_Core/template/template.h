#include "getExecutionTime.h"
#include "template_class.h"

//***************** Template function1 ****************************//
template <typename T1>
void print_size(const string& s)
{
    //static_assert(std::is_integral<T>::value, "print_size expected an integer type");
    cout << "Size of this type -" << s << "- in x64 built is:" << sizeof(T1) << endl;
}

//***************** Template function2 with specializaiton ****************************//
template <typename T1>
T1 smaller(T1 a, T1 b)
{
    return (a < b ? a : b);
}

template <>
double smaller<double>(double a, double b)
{ //template specialization for type double.
    return static_cast<long>(a * 10) < static_cast<long>(b * 10) ? a : b;
}

//***************** Template function3 ****************************//
template <typename funcT> //can use T also.. funcT here is just a name, can be T here
vector<int> myfilter(funcT f, vector<int> &v1)
{
    vector<int> v2;
    for (vector<int>::iterator it = v1.begin(); it != v1.end(); it++)
    {
        if (f(*it))
        {
            cout << *it << "\t" << endl;
            v2.push_back(*it);
        }
    }
    return v2;
}

//***************** Template function4 (with default type) ****************************//
template <typename To = string, typename From = string>
To CaiTo(From arg)
{
    stringstream ss;
    To res;
    if (!(ss << arg) || !(ss >> res) || !(ss >> std::ws).eof()) //last one is to discard all the whitespace, see if can reach to end of the file???
        //this case, formatted input, i.e., the usual input operators using `in >> value, skip leading whitespace and stop whenever the format is filled
        //unformatted input, e.g., std::getline(in, value) does not skip leading whitespace
        throw runtime_error{ "CaiTo failed" };
    return res;
}

//***************** Template function5 type tagging************************//
//tagging the type and dispatching the funcion to appropriate calls.
// all int, long, unsigned, signed, long long, will go to false_type func,
// all float, double, double double, will go to true_type func;
// actually still can do with just below one function, just put if(is_floating_point<T>::value) statement inside func body

template <typename T>
bool isEqual(T lhs, T rhs, true_type)
{ //don't have variable name after true_type, bcos we not going to use it in body.
    return static_cast<long>((lhs * 10000)) / static_cast<T>(10000.0) == static_cast<long>((rhs * 10000)) / static_cast<T>(10000.0);
}

template <typename T>
bool isEqual(T lhs, T rhs, false_type)
{
    return lhs == rhs;
}

template <typename T>
bool isEqual(T lhs, T rhs)
{
    //return isEqual(lhs, rhs, typename conditional<is_floating_point<T>::value, true_type, false_type>::type{}); 
    //typename specified the ::type refer to a type,  not a static member data/function.
    return isEqual(lhs, rhs, conditional_t<is_floating_point<T>::value, true_type, false_type>{});
}

//***************** Template function6 SFINAE (substitution failure is not an error)************************//
template <typename T> //, typename = enable_if_t<is_copy_constructible<T>::value>>
typename T::iterator::value_type getFirstCopyValue(T &v1)
{
    if (v1.size() != 0)
        return *(v1.begin());
    else
        return T::iterator::value_type{}; //shared_ptr<int>{}
}

//sfinae2 , simple utility templated class/struct to check if two types is same; if u look at the is_float_t, it also got general templated class which will alwasy return 
//false, however, when the pass in is float type, it return true through template specialization for float type!
template <typename T, typename U>
struct same_type
{
    static const bool value = false;
};

template <typename T> //specialization for types that are the same,if template<> struct same_type<double,double>{....
struct same_type<T, T>
{
    static const bool value = true;
};

template <typename T, typename U> //enable_if_t at return. the return type is the last one, list<string>
enable_if_t<(is_integral<U>::value) && (same_type<T, vector<int>>::value),
    list<string> > findTopItems(T t1, U u1)
{
    return list<string>();
}

//sfinae3,
template <typename IT, typename Pred> //, typename = enable_if_t<_Is_iterator<IT>::value>>
vector<typename IT::value_type> returnValueOf(IT begin, IT end, Pred predicate)
{
    vector<typename IT::value_type> v; //or value_type<IT>
    for (auto it = begin; it != end; it++)
    {
        if (predicate(*it))
            v.push_back(*it);
    }
    return v;
}

//sfinae4, enable_if_t do not have return value type if in the template list!
template <typename T, typename = enable_if_t<is_integral<T>::value && !is_pointer<T>::value> >
bool isOdd(T t1)
{
    return t1 % 2;
}

template <typename C, typename V>
vector<typename C::iterator> findAll(C &c, V v)
{
    vector<typename C::iterator> res;
    for (auto p = c.begin(); p != c.end(); ++p)
    {
        if (*p == v)
            res.push_back(p);
    }
    return res; //return by value, will use move semantic.
}

//struct input_iterator_tag {};
//struct output_iterator_tag {};
//struct forward_iterator_tag : public input_iterator_tag {};
//struct bidirectional_iterator_tag : public forward_iterator_tag {};
//struct random_access_iterator_tag : public bidirectional_iterator_tag {};

//namespace std {
//    template <class T>
//    struct iterator_traits {
//        typedef typename T::value_type            value_type;
//        typedef typename T::difference_type       difference_type;
//        typedef typename T::iterator_category     iterator_category;
//        typedef typename T::pointer               pointer;
//        typedef typename T::reference             reference;
//    };
//}

//namespace std {
//    template <class T>
//    struct iterator_traits<T*> {
//        typedef T                          value_type;
//        typedef ptrdiff_t                  difference_type;
//        typedef random_access_iterator_tag iterator_category;
//        typedef T*                         pointer;
//        typedef T&                         reference;
//    };
//}

template <typename RA_IT>
typename std::iterator_traits<RA_IT>::difference_type distance_cai(RA_IT it1, RA_IT it2, std::random_access_iterator_tag tag1)
{
    return it2 - it1;
}

template <typename In_IT>
typename std::iterator_traits<In_IT>::difference_type distance_cai(In_IT it1, In_IT it2, std::input_iterator_tag tag1)
{
    typename std::iterator_traits<In_IT>::difference_type diff;
    for (diff = 0; it1 != it2; it1++, diff++) {}
    return diff;
}

template <typename IT>
typename std::iterator_traits<IT>::difference_type distance_cai(IT it1, IT it2)
{
    return distance_cai(it1, it2, typename std::iterator_traits<IT>::iterator_category());
}
#pragma once

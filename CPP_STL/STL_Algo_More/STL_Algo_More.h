#pragma once
#include "STL.h"

uint32_t hash_char(uint32_t hash, char c)
{
	hash += static_cast<int>(c) * 2;
	return hash;
}

struct HashString
{
	void operator()(const string &s)
	{
		hash = accumulate(s.begin(), s.end(), hash, hash_char);
		//based on the accumulate tempalte function, it will return the 3rd parameter type...here it is uint32_t.
		//and it will call hash_char(uint32_t& hash, c) //c is each of char of string s.
	}
	uint32_t hash = 0;
};

template <typename C>
uint32_t hash_all_strings(const C &c)
{
	const auto hasher = for_each(c.begin(), c.end(), HashString());
	//base on the for_each template function definiation, the foreach will return the type of the class,, here HashString...
	//so one instance of HashString is created, and returned and assigned to hasher.
	// and this Object's internal member (hash) is used by each word in container c...
	return hasher.hash;
}

struct Person
{
	string first;
	string middle;
	string last;
};
//with precedence last > first > middle
struct Employee : public Person
{
	enum class EmployeeType
	{
		Executive,
		Manager,
		Architect,
		Senior,
		Junior,
		Management = Manager,
		Individual = Architect
	};
	EmployeeType type;
};

// STL_Algo_Basic.cpp : Defines the entry point for the console application.
//
#include "STL.h"
int main()
{
	vector v{ 9, 60, 90, 8, 45, 87, 90, 69, 69, 55, 46, 2, 77, 29 };
	vector v2{ 90, 8, 45, 87 };

	int n = count(v.begin(), v.end(), 69);
	n = count_if(v.begin(), v.end(), smaller_than_10);

	vit it;
	pair<vit, vit> its;
	its = minmax_element(v.begin(), v.end());// C++11
	it = min_element(v.begin(), v.end());
	it = max_element(v.begin(), v.end(), mymax);

	it = find(v.begin(), v.end(), 55); cout << "find (first) match of 55:" << *it << endl;
	it = find_if(v.begin(), v.end(), smaller_than_10); cout << "find_if (first) smaller_than_10:" << *it << endl; //find_if_not
    //it = find_if(v.begin(), v.end(), greater_equal<int>()); cout << "find_if (first) smaller_than_10:" << *it << endl; //find_if_not
	it = find_first_of(v.begin(), v.end(), v2.begin(), v2.end()); cout << "find first common item in v and v2:" << *it << endl;

    it = find_first_of(v.begin(), v.end(), v2.begin(), v2.end(),[](int x, int y) { return x == y * 2; }); cout << "find first item pair where value in v == 2 * v2:" << *it << endl;
	it = search(v.begin(), v.end(), v2.begin(), v2.end()); cout << "find sub v2 in v: " << *it << endl; //search sub vector
	it = adjacent_find(v.begin(), v.end()); cout << "find adjacent at: " << *it << endl; // 1st duplicate items.
	if (equal(v.begin() + 2, v.begin() + 6, v2.begin())) cout << "v.begin+2->+6 and v2 are equal" << endl;

    vector permut1 = { 1,2,3 };
    vector permut2 = { 3,2,1 };

    if (is_permutation(permut1.begin(), permut1.end(), permut2.begin())) //C++11
        cout << "permuation found!" << endl;


    if (equal(permut1.begin(), permut1.end(), permut2.begin(), [](int x, int y) {return (x + y) < 5; })) //C++11
        cout << "equal found!" << endl;
	//check attributes, C++11 is_permutation/is_sorted/is_heap/all_of/any_of/none_of

    if (all_of(permut1.begin(), permut1.end(), bind(greater_equal<int>(), placeholders::_1, -4)))
        cout << " all number greate than -4" << endl;

    //if (all_of(permut1.begin(), permut1.end(), bind(greater_equal<int>(), 5, placeholders::_2)))
        //cout << " all number less than 5" << endl;


    if (any_of(permut1.begin(), permut1.end(), [](int x) {return x == 2; }))
        cout << " any of number is exactly 2" << endl;


    if (none_of(permut1.begin(), permut1.end(), bind(less<int>(),placeholders::_1, 0)))
        cout << " none of any is less than 0" << endl;

    replace_copy(permut1.begin(), permut1.end(), permut2.begin(), 2, 4); //it will copy everything in permut1 to permut2, only when encounter 2, it will paste as 4.

	vector<int> v3(v);
	vector<int> v4(v3.size() + 10, 0);

	copy(v3.begin(), v3.end(), v4.begin()); //this is very useful, v4 could be list or deque also no problem.!!! 
    //make sure v4 is large enough to contain all in v3; strange, here destination at back

	unique(v3.begin(), v3.end()); //it works, it only removed the Adjacent duplicated items, see, still got two 90 there.

	reverse(v3.begin(), v3.end());

	//random_shuffle(v3.begin(), v3.end());

	transform(v3.begin(), v3.end(), v4.begin(), X2);

    vector<int> tranformOut;
    transform(v3.begin(), v3.end(), back_inserter(tranformOut), bind(multiplies<int>(), placeholders::_1, 5));

	generate(v4.begin(), v4.end(), rand); //overwrite from begin to end with rand generated number, stdlib.h, 

	fill(v4.begin(), v4.end(), 999); //	fill_n(mvec2.begin(),3,999); fill the first three items
	*(v4.begin() + 1) = 17;
	*(v4.begin() + 2) = 18;

	replace(v4.begin(), v4.end(), 999, 888);
	remove(v4.begin(), v4.end(), 17);//generalized form, remove_if(.., .., ....);

	rotate(v4.begin(), v4.begin() + 3, v4.end());
	cout << "\n++++++++Sorting Algorithm (requires randome access iterators,only applicable for vector/ deque/ STL array++++++++++++" << endl;
	vector<int> v5(v);
	sort(v5.begin(), v5.end(), mymax);	//generalized form, can omit mymax, will using default
	bool found = binary_search(v5.begin(), v5.end(), 55); //equality and equavelent...

	/*found=include(bs_vec.begin(),bs_vec.end(), v2.begin(),v2.end());//return true if all elements in v2 found...
	merge(v.begin(),v.end(),v2.begin(),v2.end(),v3.begin());// nothing dropped, even those duplicate ones.
	set_union(v.begin(),v.end(),v2.begin(),v2.end(),v3.begin());// drops duplicate ones, keep one copy only
	v{8,9,9,10}, v2{7,9,10}, results will bec {7,8,9,9,10}. it compared each element in v2 to all elements in vec1,
	if duplicate, then NO copy to new.... so v can preserve two '9'; there is direction!!
	set_intersection(v.begin(),v.end(),v2.begin(),v2.end(),v3.begin());
	the duplicated ones saved.
	set_difference(v.begin(),v.end(),v2.begin(),v2.end(),v3.begin());
	output will be elements that found in v, BUT NOT v2.. there is direction....
	*/

	cout << "\n++++++++ numeric algorithms in <numeric>..++++++++++++" << endl;
	int x = accumulate(v5.begin(), v5.end(), 0); //0 is initial value...part of sum
	int x1 = inner_product(v5.begin(), v5.begin() + 3, v5.end() - 3, 0); //0 is the initial value.. part of sum

	cout << "\n++++++++ Equavalence vs Equality.++++++++++++" << endl;
	//set<int, c_lsb_lss> s{ 21, 22, 26, 29 };
	//set<int, c_lsb_lss>::iterator itr1 = find(s.begin(), s.end(), 36); //alorithem find() look for equality: if (x==y);
	//set<int, c_lsb_lss>::iterator itr2 = s.find(36);//member find() look for equivelance: if (!(x<y) && !(y<x) );

	return 0;
}
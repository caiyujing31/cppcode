#pragma once
#include "STL.h"

template<class _ForwardIT, class _Ty>
_ForwardIT remove_cai(_ForwardIT _First, _ForwardIT _Last, const _Ty& _Val);

template<class _ForwardIT, class _Ty>
_ForwardIT remove_cai1(_ForwardIT _First, _ForwardIT _Last, const _Ty& _Val);

void ReadBackwards(void);

void twoDArr(void);

void vectorErasePerfCompare(void);

struct BigTestStruct {
	int iValue = 1;
	float fValue;
	long lValue;
	double dValue;
	char cNameArr[10];
	int iValArr[100];
};

void FillVector(vector<BigTestStruct>& testVector);

void TestFillVector(void);

void AssignmentVsInsertVsPushBack(void);
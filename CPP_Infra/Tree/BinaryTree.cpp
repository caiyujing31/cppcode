
#include "BinaryTree.h"

//unique_ptr must pass in as a non-const reference (to be used only), or by value(sink, ownership is taken)
void BinaryTree::makeBinaryTree(unique_ptr<node> &curr, int n, bool isFull)
{
    if (n == 0)
        return;

    curr = unique_ptr<node>(new node(RndEng() % 100));
    int offset = isFull ? 999 : 0; //if isFull true, it will only run default in switch
    switch (RndEng() % 10 + offset)
    {
    case 4:
        makeBinaryTree(curr->left, n - 1, isFull);
        break;
    case 3:
        makeBinaryTree(curr->right, n - 1, isFull);
        break;
    case 5:
        break;
    default:
        makeBinaryTree(curr->left, n - 1, isFull);
        makeBinaryTree(curr->right, n - 1, isFull);
        break;
    }
}

//three common ways of traverse through a binary threes.Inorder AND!!! preorder/postorder uniquely identify a binary tree.
//below three ways are all consider as "Depth First Traversal/Search";
// (L) Recursively Traverse its left subtree, when this step is finisehd, back to Node N again
// (R) Recursively Traverse its right subtree, when this step is finished, back to Node N again
// (N) Process Node N itself.

void BinaryTree::printPostOrder(node *curr)
{
    if (curr == nullptr)
        return;
    printPostOrder((curr->left).get());
    printPostOrder((curr->right).get());
    cout << curr->data << ",";
}

void BinaryTree::printInOrder(node *curr)
{
    if (curr == nullptr)
        return;
    printInOrder((curr->left).get());
    cout << curr->data << ",";
    //InOrderArray.push_back(curr->data); // it convert the tree to an vector.. inOrder
    printInOrder((curr->right).get());
}

void BinaryTree::printPreOrder(node *curr)
{
    if (curr == nullptr)
        return;
    cout << curr->data << ",";
    printPreOrder((curr->left).get());
    printPreOrder((curr->right).get());
}

int BinaryTree::countNodes(node *curr)
{
    if (curr == nullptr)
        return (0);

    return (1 + countNodes((curr->left).get()) + countNodes((curr->right).get()));
}

//this will find out the max path start from the root to any of  node,
int BinaryTree::MaxSumPath(node* curr) {
    if (curr == nullptr)
        return 0;

    int sumL=0;
    int sumR=0;

    sumL= MaxSumPath((curr->left).get());
    sumR = MaxSumPath((curr->right).get());

    //actually returned the biggest posible "sum" from this subtree, which must include the root node itself.
    int biggerVal = (sumR > sumL) ? sumR : sumL;

    if(biggerVal>0)
        return curr->data + biggerVal;
    else
        return curr->data;
}

int BinaryTree::MaxHeight(node *curr)
{
    if (curr == nullptr)
        return 0;
    else
    {   //definitely NO overlapping, so will NOT end up like fabinocci series kind of inefficiency
        //why no OverLapping, because each notes only have one parent!
        int lheight = MaxHeight((curr->left).get()) + 1;
        int rheight = MaxHeight((curr->right).get()) + 1;
        return (lheight > rheight) ? lheight : rheight;
    }
}

// iterative, performance much worse compared to recursive ones if trees are complete tree.
int BinaryTree::MaxHeightIte(node *curr)
{
    if (curr == nullptr)
        return 0;

    vector<qItems> vecq;
    vecq.reserve(32000);
    int results;
    vecq.emplace_back(curr, 1);

    int i = 1;
    int j = 0;
    while (j < i)
    {
        qItems qi = vecq[j];
        j++;

        if (qi.qNode->left == nullptr && qi.qNode->right == nullptr)
            results = qi.qDepth;

        if (qi.qNode->left != nullptr)
        {
            vecq.emplace_back((qi.qNode->left).get(), qi.qDepth + 1);
            i++;
        }

        if (qi.qNode->right != nullptr)
        {
            vecq.emplace_back((qi.qNode->right).get(), qi.qDepth + 1);
            i++;
        }
    }
    return results;
}

int BinaryTree::MinHeight(node *curr)
{
    if (curr == nullptr)
        return 0;

    if (curr->left == nullptr && curr->right == nullptr)
        return 1;

    if (curr->left == nullptr)
        return MinHeight((curr->right).get()) + 1;

    if (curr->right == nullptr)
        return MinHeight((curr->left).get()) + 1;

    int l = MinHeight((curr->left).get()) + 1;
    int r = MinHeight((curr->right).get()) + 1;
    return (l > r) ? r : l;
}

int BinaryTree::countLeaves(node *curr)
{
    if (curr == nullptr)
        return 0;

    if (curr->left == nullptr && curr->right == nullptr)
        return 1;

    return countLeaves((curr->right).get()) + countLeaves((curr->left).get());
}

// heap also describes a binary tree in which each node value, is larger than any value in the left or right subtrees.
bool BinaryTree::isMaxHeap(node *curr)
{
    if (curr->left == nullptr && curr->right == nullptr)
        return true;

    if (curr->left == nullptr && curr->right != nullptr)
        return (curr->data > (curr->right)->data) && isMaxHeap((curr->right).get());

    if (curr->left != nullptr && curr->right == nullptr)
        return (curr->data > (curr->left)->data) && isMaxHeap((curr->left).get());

    bool immediateCompareResult = (curr->data > (curr->right)->data) && (curr->data > (curr->left)->data);

    return immediateCompareResult &&
        isMaxHeap((curr->right).get()) &&
        isMaxHeap((curr->left).get());
}

bool BinaryTree::isBinarySearchTree(node *curr)
{
    if (curr->left == nullptr && curr->right == nullptr)
        return true;

    if (curr->left == nullptr && curr->right != nullptr)
        return (curr->data < (curr->right)->data) && isBinarySearchTree((curr->right).get());

    if (curr->left != nullptr && curr->right == nullptr)  
        return (curr->data > (curr->left)->data) && isBinarySearchTree((curr->left).get());

    bool immediateCompareResult = (curr->data < (curr->right)->data) && (curr->data > (curr->left)->data);

    return immediateCompareResult &&
        isBinarySearchTree((curr->right).get()) &&
        isBinarySearchTree((curr->left).get());
}

// check if a tree is a full tree, full tree means the node either have No Children or Have both of them.
bool BinaryTree::isFullTree(node *curr)
{
    if (curr == nullptr)
        return true;

    if (curr->left == nullptr && curr->right == nullptr)
        return true;

    if ((curr->left != nullptr) && (curr->right != nullptr))
        return isFullTree((curr->left).get()) && 
                isFullTree((curr->right).get());

    return false;
}

//check if a tree is a Complete tree, Complete tree means the level must be fully filled up 
//before the next level, and for imcomplete levels must be filled from left to right
bool BinaryTree::isCompleteTree(node *curr, int index, int TotalCount)
{
    if (curr == nullptr)
        return true;

    if (index >= TotalCount)
        return false;

    // Recur for left and right subtrees, fantastic...even though not so efficient.
    // 2*2* will become 2 to the power of "level number"
    return isCompleteTree((curr->right).get(), 2 * index + 2, TotalCount) &&
           isCompleteTree((curr->left).get(), 2 * index + 1, TotalCount);
}

bool BinaryTree::isBalancedTree(node* curr)
{ 
    if (curr->left == nullptr && curr->right == nullptr)
        return true;

    if (curr->left == nullptr && curr->right != nullptr)
    {
        return curr->right->left == nullptr &&
            curr->right->right == nullptr;
    }

    if (curr->left != nullptr && curr->right == nullptr)
        return curr->left->left == nullptr && 
                curr->left->right == nullptr;

    return isBalancedTree((curr->left).get()) &&
        isBalancedTree((curr->right).get());
}

void BinaryTree::printTree(node* curr)
{
    int MaxH = MaxHeight(curr);
    for (int i = 1; i <= MaxH; i++)
    {
        levelDataLayOut(curr, i, (1 << MaxH), MaxH);
        levelDataLayOutPrint(i, MaxH);
    }
}

//breath first traverse.
void BinaryTree::levelDataLayOut(node *curr, int level, int accumIndent, int maxH)
{
    if (curr == nullptr)
        return;

    if (level == 1)
        levelData[accumIndent] = curr->data;
    else
    {
        //init max is 5, so caller is init with 2to the power5..level 1 is root
        //if level is 3, left most leave accumIndent, so it will be 2^5 - 2^4 - 2^3  = 2^3;
        //if level is 5, left most leave accumIndent, so it will be 2^5 - 2^4 - 2^3 - 2^2- 2^1 = 2^1;
        //so level is 5, 2nd level left the rest is right,  2^5 - 2^4 + 2^3 + 2^2 + 2^1 =  2^5 - (2^4 - 2^3 - 2^2 - 2^1)
        //so level is 5, 2nd level right the rest is left,  2^5 + 2^4 - 2^3 - 2^2 - 2^1 =  2^5 + (2^4 - 2^3 - 2^2 - 2^1) 
        //so there is definitely no cross happeninig... 
        levelDataLayOut((curr->left).get(), 
                        level - 1, 
                        accumIndent - (1 << (maxH - 1)), 
                        maxH - 1);
        levelDataLayOut((curr->right).get(), 
                        level - 1, 
                        accumIndent + (1 << (maxH - 1)), 
                        maxH - 1);
    }
}

void BinaryTree::levelDataLayOutPrint(int level, int MaxH)
{
    int prevCtr = 0;
    stringstream ss;

    for (auto it = levelData.begin(); it != levelData.end(); it++)
    {
        cout << setw(it->first - prevCtr) << it->second; //right-aligned. data is printed out already.

        int currLeft = (it->first) - (1 << (MaxH - level));
        int currRight = (it->first) + (1 << (MaxH - level));

        if (prevCtr != 0)
            prevCtr += (1 << (MaxH - level));

        string s1(currLeft - prevCtr, ' ');
        ss << s1;

        string s2(currRight - currLeft, '-');
        ss << s2;

        prevCtr = it->first;
    }
    cout << endl;
    levelData.clear();
    //if (level == MaxH)
        //return;

    cout << ss.str()<< endl;
    return;
}

//we need to print the bottom view from left to right.A node x is there in output 
//if x is the bottommost(Deepest) node at its horizontal distance.
//Horizontal distance of left child of a node x is equal to horizontal
//distance of x minus 1, and that of right child is horizontal distance of x plus 1.

void BinaryTree::printBottomView(node *curr, int hd, int depth, map<int, pair<int, int> >& BottomViewMap)
{
    int horizon = hd;

    if (curr == nullptr)
        return;
    
    if (BottomViewMap.find(hd) != BottomViewMap.end())
    {
        if (depth > BottomViewMap[hd].first)
            BottomViewMap[hd] = make_pair(depth, curr->data); // the map 's pair, the key is uniq, so hd is unique
    }
    else
        BottomViewMap[hd] = make_pair(depth, curr->data);

    if (curr->left != nullptr)
        printBottomView((curr->left).get(), hd - 1, depth + 1, BottomViewMap);

    if (curr->right != nullptr)
        printBottomView((curr->right).get(), hd + 1, depth + 1, BottomViewMap);
}

int BinaryTree::maxPath(node* curr, int& res) {
    if (curr == nullptr)
        return 0;

    int l = maxPath(curr->left.get(),res);
    int r = maxPath(curr->right.get(),res);


    //case one, max(l,r)+curr->data, 
    //case two, curr->data.
    int max_single = max(max(l, r) + curr->data, curr->data); 
    //if both l,r are neg, then only curr->data is retained.

    //case three, l+ r+ curr->data. 
    //in case both l, r are positive, then it win.
    //but it can NOT return TO UP, since it is already a full path. mate
    int max_top = max(max_single, l + r + curr->data);

    res = max(res, max_top);

    return max_single;
}

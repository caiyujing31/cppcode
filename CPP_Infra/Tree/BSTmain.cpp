#include "BinaryTree.h"
#include "BinarySearchTree.h"
int main()
{
    BinaryTree t = BinaryTree(6,false);
    node* rootp = t.root.get();

    cout << t.MaxSumPath(rootp) << endl;
    int MaxH = t.MaxHeight(rootp);
    int MaxH2 = t.MaxHeightIte(rootp);
    int MinH = t.MinHeight(rootp);
    int nodes = t.countNodes(rootp);
    int leaves = t.countLeaves(rootp);

    cout << "PreOrder:";
    t.printPreOrder(rootp);
    cout << endl;
    
    cout << "InOrder:";
    t.printInOrder(rootp);
    cout << endl;
    
    cout << "PostOrder:"; 
    t.printPostOrder(rootp);
    cout << endl;
    
    cout << "isMaxHeap: " <<std::boolalpha << t.isMaxHeap(rootp) << endl;
    cout << "isBinarySearchTree: " << std::boolalpha << t.isBinarySearchTree(rootp)<< endl;
    cout << "isFullTree: " << std::boolalpha << t.isFullTree(rootp)  << endl;
    cout << "isCompleteTree:" << std::boolalpha << t.isCompleteTree(rootp, 0, nodes) << endl;
    cout << "isBalancedTree:" << std::boolalpha << t.isBalancedTree(rootp) << endl;

    t.printTree(rootp);
    map<int, pair<int, int> > BottomViewMap;
    t.printBottomView(rootp, 0, 1, BottomViewMap);

    int maxPathSum = INT_MIN;
    t.maxPath(rootp, maxPathSum);

    //********************* Binary Search Tree*******************
    int pre2[] = {40, 30, 27, 35, 31, 38, 80, 70, 100, 90, 130};
    int n = sizeof(pre2) / sizeof(pre2[0]);
    vector<int> vec1(pre2, pre2 + n);
    BinarySearchTree BST1(vec1);
    cout << boolalpha << BinarySearchTree::ArrayIsPreorderBST(vec1.begin(), vec1.end());

    vector<int> rand2{14, 11, 19, 7, 13, 22, 9, 12, 24, 10};
    BinarySearchTree BST2(rand2);
 
    BST2.DeleteNode(24);
    BST2.printInOrder(BST2.root.get());

    node *temp = BST2.LCA(7, 22);
    if (temp != nullptr)
        cout << "LCA of 7 and 22 is:" << temp->data << endl;


    //vector<int> rand3{ 5,4,2,3 }; //need rotate right
    vector<int> rand3{ 2,3,4,5 }; //need rotate left
    BinarySearchTree BST3(rand3);
    BST3.printTree(BST3.root.get());

    BST3.rebalancing(BST3.root,BST3.root);
    BST3.printTree(BST3.root.get());
    return 0;
}

//Characteristics of RBT(Red-Black Tree)
//1. Root node is Always BLACK in color.
//2. Every new Node inserted is always RED in color at beginning.
//3. Every nullptr child of a node is considered as BLACK in color.
//4. Both children of a RED node are BLACK.
//5. Every path fom a node to a descendent leaf contains same number of black nodes. (left and right height difference <=1)
//6. Every 3-node must store the red node as left child. <------ rule of left leaning Red-Black Tree
//
//According to Sedgewick, an LLRB has the following performance properties :
//Worst case: log2(all 2 - nodes)
//Best case: log4 N = 1 / 2 log2 N(all 4 - nodes)

#include "getExecutionTime.h"

enum Color{BLACK = false, RED = true};
struct Node{
	int key;
	int value;
	shared_ptr<Node> left=nullptr;
	shared_ptr<Node> right = nullptr;
	Color color=RED;

	Node(int _key = 0, int _value = 0):key(_key),value(_value){
	}
};

class LLRBTree{
private:
	shared_ptr<Node> root;
protected:
	shared_ptr<Node> rotateLeft(shared_ptr<Node> myNode);
	shared_ptr<Node> rotateRight(shared_ptr<Node> myNode);
	shared_ptr<Node> BSTinsert(shared_ptr<Node> node, int key = 0, int value = 0);
	bool isRed(shared_ptr<Node> myNode){
		if (myNode == nullptr)
			return false;
		return (myNode->color == RED);
	}

	void swapColors(shared_ptr<Node> node1, shared_ptr<Node> node2){
		Color temp = node1->color;
		node1->color = node2->color;
		node2->color = temp;
	}

public:
	LLRBTree(){
		root = nullptr; 
	}
	
	shared_ptr<Node> getRoot(void) {
		return root; 
	}
	
	void insert(int key = 0, int value = 0);
};

shared_ptr<Node> LLRBTree::rotateLeft(shared_ptr<Node> myNode){
	cout << "\nleft rotation\n";
	shared_ptr<Node> child = myNode->right;
	shared_ptr<Node> childLeft = child->left;

	child->left = myNode;
	myNode->right = childLeft;

	return child;
}

shared_ptr<Node> LLRBTree::rotateRight(shared_ptr<Node> myNode){
	cout << "\nright rotation\n";
	shared_ptr<Node> child = myNode->left;
	shared_ptr<Node> childRight = child->right;

	child->right = myNode;
	myNode->left = childRight;

	return child;
}

//put node into tree, the result is a balanced binary search tree in order, left<self<right, left leaning red black tree
shared_ptr<Node> LLRBTree::BSTinsert(shared_ptr<Node> node, int key, int value){
	if (node == nullptr)
		return shared_ptr<Node>(new Node(key, value));

	if (key < (node->key))
		node->left = BSTinsert((node->left), key, value);//insert into left
	else
		if (key >(node->key))
			node->right = BSTinsert(node->right, key, value);//insert into right
		else
		{
			node->value = value; 
			return node;
		}

	//Left leaning Red-Black tree rotations
	// case 1. when right child is Red but left child is  Black or doesn't exist.  
	if (isRed(node->right) && !isRed(node->left)){
		node = rotateLeft(node);
		// swap the colors as the child node should always be red  
		swapColors(node, node->left);
	}

	// case 2. when left child as well as left grand child in Red  
	if (isRed(node->left) && isRed(node->left->left)){
		node = rotateRight(node);
		swapColors(node, node->right);
	}

	// case 3, when both left and right child are Red in color.  
	if (isRed(node->left) && isRed(node->right)) {
		// invert the color of node and it's left and right child.  
		node->color = RED;
		node->left->color = BLACK;
		node->right->color = BLACK;
	}
	return node;
}

void LLRBTree::insert(int key, int value){
	root = BSTinsert(root, key, value);
	root->color = BLACK;// to make sure root remains black
}

bool containsNode(shared_ptr<Node> node, int key, int&value){
	shared_ptr<Node> X = node;
	while (X != nullptr){
		if (key < X->key) 
			X = X->left;
		else 
			if (key > X->key) 
				X = X->right;
			else
			{
				value = X->value;
				return true;
			}
	}
	return false;
}

//depth first, given a binary tree, print its nodes according to the "bottom-up" postorder traversal. Left->Right->self
void printPostorder(shared_ptr<Node> node){
	if (node == nullptr)
		return;
	printPostorder(node->left);
	printPostorder(node->right);
	cout << node->key << " ";
}

void printInorder(shared_ptr<Node> node){
	if (node == NULL)
		return;
	printInorder(node->left);
	cout << node->key << " ";
	printInorder(node->right);
}

void printPreorder(shared_ptr<Node> node){
	if (node == NULL)
		return;
	cout << node->key << " ";
	printPreorder(node->left);
	printPreorder(node->right);
}

//this function prints the tree in the breadth first / lever order traversal
void breadthFirstTraversal(shared_ptr<Node> root){
	queue<shared_ptr<Node> > q;
	while (root != NULL){
		cout << root->key << " ";
		if (root->left != NULL){
			q.push(root->left); 
		}
		
		if (root->right != NULL){
			q.push(root->right); 
		}
		
		if (!q.empty()){
			root = q.front(); 
			q.pop(); 
		}//print front of queue(q.front()) and remove it(q.pop()).
		else{
			root = NULL; 
		}
	}
}
//Given an array of numbers, return true if given array can represent preorder traversal of a Binary Search Tree, else return false. Expected time complexity is O(n).
//binary search trees (BST), in each node must be greater than or equal to any key stored in the left sub - tree, and less than or equal to any key stored in the right sub tree

#include "BinarySearchTree.h"
BinarySearchTree::BinarySearchTree(vector<int> &vec)
{
    for (int &a : vec)
        InsertNode(a);
}

void BinarySearchTree::insertSortedVec(vit itL, vit itR)
{
    if ((itR - itL) == 0)
        return;

    if ((itR - itL) == 1)
    {
        InsertNode(*itL);
        return;
    }

    if ((itR - itL) == 2 || (itR - itL) == 3)
    {
        InsertNode(*(itL + 1));
        InsertNode(*itL);
        if ((itR - itL) == 3)
            InsertNode(*(itL + 2));
        return;
    }

    for (vit it = itL; it != itR; it++)
    {
        vit itTmpMid = itL + (itR - itL) / 2;
        InsertNode(*itTmpMid);
        insertSortedVec(itL, itTmpMid);
        insertSortedVec(itTmpMid + 1, itR);
        return;
    }
}

// NO re-balancing height implemented!
bool BinarySearchTree::InsertNode(int a)
{
    node *tmp = root.get();
    if (tmp == nullptr)
    {
        root = unique_ptr<node>(new node(a));
        return true;
    }

    while (tmp != nullptr)
    {
        if (a > tmp->data)
        {
            if (tmp->right == nullptr)
            {
                tmp->right = unique_ptr<node>(new node(a));
                return true;
            }
            else
            {
                tmp = (tmp->right).get();
            }
        }

        if (a == tmp->data)
        {
            return false;
        }

        if (a < tmp->data)
        {
            if (tmp->left == nullptr)
            {
                tmp->left = unique_ptr<node>(new node(a));
                return true;
            }
            else
            {
                tmp = (tmp->left).get();
            }
        }
    }

    return false;
}

node *BinarySearchTree::findMaxOnLeftSubTree(node *curr)
{
    if (curr->right != nullptr)
        return findMaxOnLeftSubTree((curr->right).get());
    else
        return curr;
}

void BinarySearchTree::swapNode(node *n1, node *n2)
{
    int temp = n1->data;
    n1->data = n2->data;
    n2->data = temp;
}

bool BinarySearchTree::DeleteNode(int a)// NO re-balancing height implemented!
{
    node* tmp = root.get();

    if (tmp == nullptr)
        return false;

    node *tmpP = tmp; //tmpP is the parent of target 
    bool leftOfParent = false;

    while (tmp != nullptr && tmp->data != a)
    {
        if (a > tmp->data && tmp->right == nullptr)
            return false;

        if (a < tmp->data && tmp->left == nullptr)
            return false;

        tmpP = tmp;
        if (a > tmp->data)
        {
            tmp = (tmpP->right).get();
            leftOfParent = false;
        }
        else
        {
            tmp = (tmpP->left).get();
            leftOfParent = true;
        }
    }

    if (tmp == nullptr)
        return false;

    //come to here, already locate the node with data == a;
    if (tmp->right == nullptr && tmp->left == nullptr)
    {
        if (tmpP == tmp)
            root = nullptr;
        else
            leftOfParent ? tmpP->left = nullptr : tmpP->right = nullptr;
        return true;
    }

    if (tmp->right == nullptr && tmp->left != nullptr)
    {
        if (tmpP == tmp)
            root = move(root->left);
        else
            leftOfParent ? tmpP->left = move(tmp->left) : tmpP->right = move(tmp->left);
        return true;
    }

    if (tmp->right != nullptr && tmp->left == nullptr)
    {
        if (tmpP == tmp)
            root = move(root->right);
        else
            leftOfParent ? tmpP->left = move(tmp->right) : tmpP->right = move(tmp->right);
       
        return true;
    }

    if (tmp->right != nullptr && tmp->left != nullptr)
    {
        swapNode(tmp, findMaxOnLeftSubTree((tmp->left).get()));
        DeleteNode(a);
        return true;
    }

    return false;
}

//Let T be a rooted tree. The lowest common ancestor between two nodes n1 and n2 is defined 
//as the lowest node in T that has both n1 and n2 as descendants (where we allow a node to be a descendant of itself).
//if node->value is in between num1 and num2 and if it is the first time into such condition, so for sure it will 
//be the lowest possible common acestor

node *BinarySearchTree::LCA(node *node, int num1, int num2)
{
    if (node == nullptr)
        return nullptr;

    if (node->data > num2 && node->data > num1)
        return LCA((node->left).get(), num1, num2);

    if (node->data < num2 && node->data < num1)
        return LCA((node->right).get(), num1, num2);

    return node;
}

bool BinarySearchTree::ArrayIsPreorderBST(vit s, vit e)
{
    if (e - s <= 1)
        return true;

    vit center = s;
    vit it = center + 1;

    while (it != e && *center > * it)
        it++;
    //allLeftSmallerThanCenter

    vit minRight = it;
    if (it == e)
        return true;
    else
        minRight = min_element(it, e);

    bool allRightGreaterThanCenter = (*minRight > * center) ? true : false;

    return allRightGreaterThanCenter &&
        ArrayIsPreorderBST(s + 1, it) &&
        ArrayIsPreorderBST(it, e);
}


//it will only do one action, either rotate left, or rotate right,
//or did NOT do anything.
// TBD

void BinarySearchTree::rebalancing(unique_ptr<node>& curr, unique_ptr<node>& currParent, bool isLeft)
{
    if (curr == nullptr)
        return;

    int hL = MaxHeight(curr->left.get());
    int hR = MaxHeight(curr->right.get());

    bool isRoot = root.get() == curr.get() ? true : false;
    unique_ptr<node> newCurr;

    if (std::abs(hL - hR) <= 1)
        return;

    if (hL - hR > 1)
    {
        //if(MaxHeight(curr->left->right.get()) > MaxHeight(curr->left->left.get()))
            //turn left

        //turn right
        newCurr = move(curr->left);
        if (newCurr->right != nullptr)
            curr->left = move(newCurr->right);
        newCurr->right = move(curr);
    }

    if (hR - hL > 1)
    {
        //if(MaxHeight(curr->right->left.get()) > MaxHeight(curr->right->right.get()))
            //turn right

        //turn left
        newCurr = move(curr->right);
        if (newCurr->left != nullptr)
            curr->right = move(newCurr->left);
        newCurr->left = move(curr);
    }

    if (isRoot)
        root = move(newCurr);
    else
        isLeft ? currParent->left = move(newCurr) :
            currParent->right = move(newCurr);

    return;
}
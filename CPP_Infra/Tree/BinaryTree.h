#pragma once
#include "getExecutionTime.h"

struct node {
    int data;
    unique_ptr<node> left;
    unique_ptr<node> right;
    node() :data(0), left(nullptr), right(nullptr) {}
    node(int a) :data(a), left(nullptr), right(nullptr) {}
};

struct qItems {
    qItems(node* np, int qD) :qNode(np), qDepth(qD) {}
    node* qNode;
    int qDepth;
};

class BinaryTree {
private:
    map<int, int> levelData; //pair of (AbsolutePositiion, ValueOfNode).
    void makeBinaryTree(unique_ptr<node>& curr, int n, bool isFull);

public:
    unique_ptr<node> root;
    BinaryTree():root(nullptr){}

    BinaryTree(int n, bool isFull){ makeBinaryTree(root, n, isFull);}

    void printPostOrder(node* curr);

    void printInOrder(node* curr);

    void printPreOrder(node* curr);

    int countNodes(node* curr);

    int countLeaves(node* curr);
    
    int MaxSumPath(node* curr);

    int MaxHeight(node* curr);

    int MaxHeightIte(node* curr);

    int MinHeight(node* curr);

    bool isMaxHeap(node* curr);

    bool isBinarySearchTree(node* curr);

    bool isFullTree(node* curr);

    bool isCompleteTree(node* curr, int index, int TotalCount);
    
    bool isBalancedTree(node* curr);

    void printBottomView(node* curr, int hd, int depth, map<int, pair<int, int> >& BottomViewMap);

    void printTree(node* curr);

    void levelDataLayOut(node* curr, int level, int accumIndent, int maxH);

    void levelDataLayOutPrint(int level, int MaxH);
    
    int maxPath(node* curr, int& res);
};

#include "tlist.h"

int main()
{
    tList<int> mL1;
    mL1.addNode(0); //transfer this main root's ownership to the List. later delete by clear the list;

    for (int i = 1; i < 9; i = i + 2)
    { //upon the destructin of the list, or the clearlist(), too many calls, Stack OverFlow happened.
        mL1.addNode(i);
    }
    tList<int> mL2(mL1);
    vector<double> vec{1.0, 1.2, 1.3, 1.1, 1.4, 1.1};
    tList<double> mL3(vec);
    mL1.removeFirstVal(1);

    auto f1 = mL3.findVal(1.4);
    auto f2 = mL3.findAllVal(1.1);

    cout << mL3.size;
    mL1.clearlist();

    return 0;
}

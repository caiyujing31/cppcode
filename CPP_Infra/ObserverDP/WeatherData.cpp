#include "WeatherData.h"

void WeatherData::registerObserver(Observer *observer) {
    //we can have two different or multiple containers to hold 
    //totally unrelated classes, even if 
    observers.push_back(observer);
}

void WeatherData::removeObserver(Observer *observer) {

    auto iterator = std::find(observers.begin(), observers.end(), observer);

    if (iterator != observers.end()) {
        observers.erase(iterator);
    }
}

void WeatherData::notifyObservers() {
    for (Observer *observer : observers) {
        observer->update(temp, humidity, pressure);
    }
}

void WeatherData::setState(float temp, float humidity, float pressure) {
    this->temp = temp;
    this->humidity = humidity;
    this->pressure = pressure;
    std::cout << std::endl;
    notifyObservers();
}
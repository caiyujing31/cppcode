#pragma once
#include "getExecutionTime.h"

template <typename CVec>
class VectorIterator
{
public:
    using ValueType = typename CVec::ValueType;  //eg int, string
    using PointerType = ValueType*; //eg int*
    using ReferenceType = ValueType&; //eg int&

    VectorIterator(PointerType ptr) 
        : m_Ptr(ptr){}

    VectorIterator& operator++() //prefix ... you do not have "const" func verions, because these func meant to change the object itself.!
    {
        m_Ptr++;
        return *this; //this is permanent, so return by ref
    }

    VectorIterator operator++(int) //postfix
    {
        VectorIterator it = *this;
        m_Ptr++; //or ++(*this);
        return it; //it is temporary variable of pointer, return by value is good.
    }

    VectorIterator& operator--() //prefix
    {
        m_Ptr--;
        return *this; //this is permanent, so return by ref
    }

    VectorIterator operator--(int) //postfix
    {
        VectorIterator it = *this;
        m_Ptr--;
        return it; //it is temporary variable of pointer, return by value is good.
    }

    ReferenceType operator[](int idx)
    {
        return *(m_Ptr+idx);
    }

    PointerType operator->()
    {
        return m_Ptr;
    }

    ReferenceType operator*()
    {
        return *m_Ptr;
    }

    bool operator==(const VectorIterator& rhs)
    {
        return m_Ptr == rhs.m_Ptr;
    }

    bool operator!=(const VectorIterator& rhs)
    {
        return m_Ptr != rhs.m_Ptr;
    }    
private:
    PointerType m_Ptr;
};

template <typename T>
class CVector
{
public:
    using ValueType = T;
    using Iterator = VectorIterator<CVector<T> >;

public:
    CVector(size_t size)
    {
        ReAlloc(size);
        //print_info(v, s);
    }

    CVector()
    {
        ReAlloc(2);
    }

    CVector(const CVector& rhs)
    {
        ReAlloc(rhs.Size());
        cout<<"copy constructor"<<endl;
        for (size_t i = 0; i < rhs.Size(); i++)
            m_Data[i] = rhs[i];
    }

    ~CVector() 
    { 
        Clear();
    }
    
    Iterator begin()
    {
        return Iterator(m_Data);
    }

    Iterator end()
    {
        return Iterator(m_Data + m_Size);
    }

    void PushBack(const T& rhs)
    {
        if (m_Capacity <= m_Size)
            ReAlloc(m_Capacity * 1.5);
        m_Data[m_Size] = rhs;
        m_Size++;
    }

    void PushBack(T&& rhs)
    {
        if (m_Capacity <= m_Size)
            ReAlloc(m_Capacity * 1.5);

        m_Data[m_Size] = std::move(rhs);
        m_Size++;
    }

    template<typename... ArgsType>
    //return or not doesnt really matters.. a    
    T& EmplaceBack(ArgsType&&... args)
    {
        if (m_Capacity <= m_Size)
            ReAlloc(m_Capacity * 1.5);

        new(&m_Data[m_Size]) T(std::forward<ArgsType>(args)...); //construct in-place...

        m_Size++;
        return m_Data[m_Size];
    }

    void PopBack()
    {
        if (m_Size > 0)
        {
            m_Data[m_Size - 1].~T();
            m_Size--;
        }
    }

    void Clear()
    {
        for(size_t i=0; i<m_Size; i++) //m_Size.. not m_Capacity
        {
            m_Data[i].~T();
        }
        m_Size = 0;
    }

    const T& operator[](size_t index) const //this is for const object, the v[i] in the print_Vec...
    {
        if (index < m_Size && index >= 0)
            return m_Data[index];
    }

    T& operator[](size_t index)
    {
        if (index < m_Size && index >= 0)
            return m_Data[index];
    }

    size_t Size() const { return m_Size; }
private:
    void ReAlloc(size_t newCap)
    {
        T* newBlock = new T[newCap];

        if (newCap < m_Size)
            m_Size = newCap;//downSizing

        for (size_t i = 0; i < m_Size; i++)
            newBlock[i] = std::move(m_Data[i]);
        //cast to rvalue referencing, making this move able-to , does not have any downside.
        //memcopy will not works, if T is complicated class with own copy construtor. memcopy is a shawllo copy

        delete[] m_Data;
        m_Data = newBlock;
        m_Capacity = newCap;
    }

    T* m_Data = nullptr;
    size_t m_Size = 0;
    size_t m_Capacity = 0;
};
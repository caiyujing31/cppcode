#include "CVector.h"

template <typename ttt>
void print_CVec(const CVector<ttt>& v)
{
    for (size_t i = 0; i < v.Size(); i++)
    {
        cout << v[i] << endl;
    }
}


struct CTypex
{
    //the copy/ctors/dtor/move op/move assign are really not really taking long time to run. so no print out to confuse 
    //the main objective, which is to less copy/...
    float x, y, z;
    CTypex() :x(0), y(0), z(0) {};
    CTypex(float scaler) :x(scaler), y(scaler), z(scaler) {};
    CTypex(float x, float y, float z) :x(x), y(y), z(z) {};
    CTypex(const CTypex& rhs) : x(rhs.x), y(rhs.y), z(rhs.z) { cout << "copied... the less the better" << endl; };
    CTypex(CTypex&& rhs) : x(rhs.x), y(rhs.y), z(rhs.z) {}
    CTypex& operator=(const CTypex& rhs) { CTypex t;  t.x = rhs.x; t.y = rhs.y; t.z = rhs.z; cout << "assignment = ... the less the better" << endl; return t; }
    CTypex& operator=(CTypex&& rhs) { CTypex t;  t.x = rhs.x; t.y = rhs.y; t.z = rhs.z;  return t; }

    ~CTypex() {}
};

struct CTypexRsc
{
    float x;
    int* m_Heap = nullptr;
    CTypexRsc() :x(0) { m_Heap = new int[10]; };
    CTypexRsc(float x) :x(x) { m_Heap = new int[10]; };
    CTypexRsc(const CTypexRsc& rhs) = delete;
    CTypexRsc(CTypexRsc&& rhs) : x(rhs.x) { this->m_Heap = rhs.m_Heap;  rhs.m_Heap = nullptr; }
    CTypexRsc& operator=(const CTypexRsc& rhs) = delete;
    CTypexRsc& operator=(CTypexRsc&& rhs) { CTypexRsc t;  t.m_Heap = rhs.m_Heap;  rhs.m_Heap = nullptr; return t; }

    ~CTypexRsc() { delete[] m_Heap; }
};

template <>
void print_CVec(const CVector<CTypex>& v)
{
    for (size_t i = 0; i < v.Size(); i++)
    {
        cout << v[i].x << ":" << v[i].y << ":" << v[i].z << endl;
    }
}


int main()
{
    CVector<string> c;
    string s = "s0";
    c.PushBack(s);
    c.PushBack("s1"s);
    c.PushBack("s2"s);
    c.PushBack("s3"s);
    c.PushBack("s4"s);
    c.PushBack("s5"s);
    cout << c[4] << endl;
    c.PushBack("s6"s);
    print_CVec(c);
    
    const CVector<string> c2(c);
    print_CVec(c2);//free func
    //c2.clear(); //can only call const func like size() and operator[]


    CVector<CTypex> cx;
    cx.PushBack(CTypex());
    cx.PushBack(CTypex(1.0f));
    cx.PushBack(CTypex(1.0f, 2.0f, 3.0f));

    cx.EmplaceBack();
    cx.EmplaceBack(1.1f);
    cx.EmplaceBack(1.1f, 2.1f, 3.1f);

    cx.PopBack();
    print_CVec(cx);


    {
        CVector<CTypexRsc> cx2;
        cx2.PushBack(CTypexRsc());
        cx2.PushBack(CTypexRsc(1.0f));
        cx2.PushBack(CTypexRsc(2.0f));
        cx2.PopBack();
        cx2.EmplaceBack();
        cx2.EmplaceBack(1.1f);
        cx2.EmplaceBack(2.1f);
        cx2.Clear();
    }

    CVector<long long> CVLong;
    CVLong.PushBack(1);
    CVLong.PushBack(10);
    CVLong.PushBack(100);
    CVLong.PushBack(1000);
    CVLong.PushBack(10000);

    CVector<long long>::Iterator it2 = CVLong.begin();
    cout << it2[3] << endl;
    it2++;
    ++it2;

    for (CVector<long long>::Iterator it = CVLong.begin();
        it != CVLong.end();
        it++)
    {
        cout << *it << endl;
        if (it == it2)
        {
            cout << "found it2" << endl;
        }
    }    
    return 0;
}
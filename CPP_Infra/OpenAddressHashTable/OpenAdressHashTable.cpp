
#include "getExecutionTime.h"
template<typename K, typename V>
class HashMap
{
    vector<pair<K, V>* > arr;
    int capacity;
    int size;
public:
    HashMap(){
        capacity = 20;
        size = 0;
        arr = vector<pair<K, V>* >(capacity);
        for (int i = 0; i < capacity; i++)
            arr[i] = nullptr;
    }
    ~HashMap() {
        for (pair<K, V>* ptr : arr) {
            delete ptr;
        }
    }
    
    int hashCode(K key)
    {
        return key % capacity;
    }

    void insertNode(K key, V value)
    {
        if (size == capacity) {
            capacity *= 2;
            arr.reserve(capacity); //expand, and keep old data.
        }

        int idx = hashCode(key);
        int cnt = 0;
        while (arr[idx] != nullptr &&
               arr[idx]->first != key &&
               cnt < capacity) //extreme case, the container is full and full with item which have same hash index.
        {   //linear search to avoid collision.
            idx++;
            idx %= capacity;
            cnt++;
        }

        // case1 (overwrite): arr[idx] !=nullptr && arr[idx]->first == key
        // case2: (new) arr[idx] ==nullptr
        if (arr[idx] != nullptr) 
        {
            arr[idx]->first = key;
            arr[idx]->second = value;
        }
        else 
        {
            size++;
            arr[idx] = new pair<K, V>(key, value);
        }
    }

    void deleteNode(int key)
    {
        int idx = hashCode(key);

        int count = 0;
        while (arr[idx] != nullptr)
        {
            count++;
            if (count > capacity)
                return; //avoid worst case repetively look for the item, end in infinity loop

            if (arr[idx]->first == key)
            {
                delete arr[idx];
                arr[idx] = nullptr;
                size--;
                return;
            }

            if (hashCode(arr[idx]->first) != idx)
                return; //means in the next "key" start position.

            idx++;
            idx %= capacity;
        }
    }

    V get(int key)
    {
        int idx = hashCode(key);
        int counter = 0;

        while (counter <= capacity)
        {
            counter++;

            if (arr[idx] != nullptr)
            {
                if (hashCode(arr[idx]->first) != idx)
                    throw invalid_argument("key not found in get() func with line number of: "s + to_string(__LINE__));

                if (arr[idx]->first == key)
                    return arr[idx]->second;
            }
            idx++;
            idx %= capacity;
        }

        throw invalid_argument("key not found in get() func with line number of: "s + to_string(__LINE__));
    }

};

int main()
{
    HashMap<int, int> h = HashMap<int, int>();
    h.insertNode(1, 1);
    h.insertNode(2, 2);
    h.insertNode(2, 3);
    h.insertNode(3, 3);
    h.insertNode(23, 5);
    h.insertNode(26, 260);
    h.insertNode(20, -9);
    h.deleteNode(2);
    
    try {
        cout << h.get(26) <<endl;
        cout << h.get(2) <<endl;
    }
    catch (const invalid_argument& e) {
        cout << "invalid argument exception:" << e.what() << endl;
    }    
    return 0;
}
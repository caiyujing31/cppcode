#include "getExecutionTime.h"

class strCount
{
private:
    int mCount;
    char* str;
    friend class Strings;
    strCount(const char* s)
    {
        int len = strlen(s);
        str = new char[len + 1];
        strcpy_s(str, len+1, s); //destination, size (include the null char), source
        mCount = 1;
    }
    ~strCount()
    {
        delete[] str;
    }
};

class Strings
{
private:
    strCount* pSC;
public:
    Strings() { pSC = new strCount("NULL"); }
    Strings(const char* s) { pSC = new strCount(s); }
    Strings(Strings& s) { pSC = s.pSC; (pSC->mCount)++; }
    ~Strings()
    {
        if (pSC->mCount == 1)
            delete pSC;
        else
            (pSC->mCount)--;
    }
    void display(){cout << pSC->str << " addr:" << pSC << endl;}
    void operator=(Strings& s)
    {
        if (pSC->mCount == 1)
            delete pSC;//delete the caller object's original 
        else
            (pSC->mCount)--;

        pSC = s.pSC;
        (pSC->mCount)++;
    }

};

int main()
{
    Strings s3("when the fox preaches, look to your geese.");
    s3.display();

    Strings s1; 
    s1 = s3;
    s1.display();

    Strings s2(s3);
    s2.display();
    getchar();
}
#include "getExecutionTime.h"
class stack{
	vector<int> vec1={9,8,7,4,5,1,2,4};
public:
	void pop(){vec1.pop_back(); } 
	int& top(){return vec1.back(); }
};

void func_a(stack& st){
	int v = st.top();
	this_thread::sleep_for(100ms);
	st.pop();
}

void func_b(stack& st){
	int v = st.top();
	this_thread::sleep_for(200ms); //with above sleep, it almost guarantee the popped value is NOT same as v. 
	st.pop(); 	
	//with lock in the pop function, also NOT helpful, got to combined the pop/top into one func..
}

class logfile{
	ofstream f;
    mutex mu1;
    mutex mu2;
    once_flag _flag;
public:
	logfile(){
	}
	~logfile(){
		f.close();
	}
	void shared_print(string callerFunc, int value){
        /*  
            1. prefer using SINGLE mutex. if need two lock, make it same order.use lock to play safe
            2. try NOT lock the mutex, then call the USER func(exception - prone code)!!!
            - fine - grained lock : protect small amount of data-- - more exposed to deadlock
            - coarse - grained lock : protect big amount of data-- - lose benefit of parallessim
        */

            ////if func1 lock it, and func2 waited here... then f will open twice.. deadlock
            //if (!f.is_open()){
            //	std::unique_lock<mutex> locker2(mu_file); 
            //	f.open("log.txt"); 
            //}

            ////this is solution to address above issue, however 
            ////it will cause code to do LOT unnecessary action by locking/unlocking just to check if the file is open.
            //	std::unique_lock<mutex> locker2(mu_file); 
            //	if (!f.is_open()){
            //		f.open("log.txt");
            //	}

        call_once(_flag, [&]() {f.open("log.txt"); });
        lock_guard<mutex> locker2(mu2);
        lock_guard<mutex> locker(mu1); //classic deadlock , diff order!

		this_thread::sleep_for(10ms);
		f << "shared_print: " << callerFunc << ": " << value << endl;
	}

    void shared_print2(string callerFunc, int value) {
        call_once(_flag, [&]() {f.open("log.txt"); });
        lock_guard<mutex> locker4(mu2);
        lock_guard<mutex> locker3(mu1); 
        this_thread::sleep_for(10ms);
        f << "shared_print2:" << callerFunc << ": " << value << endl;
    }
	//ofstream& getstream(void){ return f;} //very bad, if return f to outside, others can use f without lock!.
};

void func1(logfile& log1){
	for (int i = 0; i < 10; i++)
		log1.shared_print2("func1", i);
}

int main(int argc, char* argv[]){
	logfile log1;
	thread t1(func1, std::ref(log1));
	t1.detach();
	for (int i = 0; i > -10; i--)
		log1.shared_print("main", i);

	stack s1;
	thread t10(func_a, std::ref(s1));
	thread t11(func_b, std::ref(s1));
	t10.detach();
	t11.detach();
	this_thread::sleep_for(1s);	
	return 0;
}
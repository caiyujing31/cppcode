#include <semaphore.h>
#include <thread>
#include <chrono>
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
using namespace  chrono_literals;

sem_t s;
struct cool
{
	int i;
	double d;
	char c;
	cool(int _i, double _d, char _c):
		i(_i),d(_d),c(_c){}
};

vector<cool> v1;
void child()
{
	for(int i=0; i<100;i++)
	{
		this_thread::sleep_for(10ms);
		v1.emplace_back(i, i+0.001, 64 + i % 26);
		if(i==10)
			sem_post(&s);
	}
}

int main()
{
	sem_init(&s, 0, 0);
	thread t1(child);
	
	
	sem_wait(&s);

	cout<<"called when the child at least filled 10 items"<< endl;
	for(auto& a: v1)
	{
		cout<< a.i << " " <<  a.d << " " <<  a.c << endl;
	}

	t1.join();
	return 0;
}

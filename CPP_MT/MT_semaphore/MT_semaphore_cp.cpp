#include <semaphore.h>
#include <thread>
#include <chrono>
#include <iostream>
#include <deque>
#include <algorithm>
#include "getExecutionTime.h"
using namespace std;
using namespace  chrono_literals;

deque<int> v1;
int loop=100;

sem_t empty;
sem_t full;

void consumer()
{
	for(int i=0; i<loop;i++)
	{
		sem_wait(&full);
		this_thread::sleep_for(10ms);
		int j = v1.front();
		v1.pop_front();
		cout<< "+" <<  j <<endl;
		sem_post(&empty);
	}
	
	cout<< "consumer is done!" <<endl;
}

void producer()
{
	for(int i=0; i<= loop;i++)
	{
		sem_wait(&empty);
		this_thread::sleep_for(9ms);
		cout<< "-" <<  i <<endl;
		v1.push_back(i);
		sem_post(&full);
	}

	cout<< "producer is done!" <<endl;
}

int main()
{
	cGetTime t;
	sem_init(&full, 0, 0);
	sem_init(&empty, 0, loop);
	thread t1(consumer);
	thread t2(producer);
	
	t1.join();
	t2.join();

	cout<< "main exit" <<endl;
	return 0;
}

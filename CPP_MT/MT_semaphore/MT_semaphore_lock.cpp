#include <semaphore.h>
#include <thread>
#include <chrono>
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
using namespace  chrono_literals;

sem_t s;
vector<int> v1={0};

void func1()
{	//ideally in the main (it will print out to 0-200//
	//but without that sem pair, it could end with 199 or other number
	for(int i=0; i<100;i++)
	{
		this_thread::sleep_for(10ms);
		sem_wait(&s);
	 	int temp = v1.back();
		v1.push_back(temp+1);
		sem_post(&s);
	}
}

void func2()
{
	for(int i=0; i<100;i++)
	{
		this_thread::sleep_for(10ms);
		sem_wait(&s);
		int temp = v1.back();
		v1.push_back(temp+1);
		sem_post(&s);
	}
}


int main()
{
	sem_init(&s, 0,1);
	thread t1(func1);
	
	thread t2(func2);
	t1.join();
	t2.join();
		
	cout<<"main exit"<< endl;
	for(int& a: v1)
	{
		cout<< a << endl;
	}

}

#include <semaphore.h>
#include <thread>
#include <chrono>
#include <iostream>
#include <deque>
#include <algorithm>
#include "getExecutionTime.h"
#include <signal.h>
using namespace std;
using namespace  chrono_literals;

deque<long> v1;
int th=100;

bool stop;
sem_t l;
sem_t e;
sem_t f;

void consumer()
{
	while(true && !stop)
	{
		int x=RndEng()%100;
		auto xt = std::chrono::milliseconds(x);

		sem_wait(&f);
			this_thread::sleep_for(xt);
			sem_wait(&l);
				int j = v1.front();
				v1.pop_front();
			sem_post(&l);
			cout<< "+" <<  j <<endl;
		sem_post(&e);
	}
	
	cout<< "consumer exit" <<endl;
}

void producer()
{
	long i=0;
	while(true && !stop)
	{
		int x=RndEng()%40;
		auto xt = std::chrono::milliseconds(x);
		while((int)v1.size()>=th)//stop producer when back pressured
		{
			cout << "producer stopped" << endl;	
			this_thread::sleep_for(xt);
		}
		sem_wait(&e);
			this_thread::sleep_for(xt);
			cout<< "-" <<  i <<endl;
			i++;
			sem_wait(&l);
				v1.push_back(i);
			sem_post(&l);
		sem_post(&f);
	}

	cout<< "producer exit" <<endl;
}

void handle_sg(int arg)
{
	cout << "stop program" << endl;
	stop = true;
}

int main()
{	//issue single hangup to the process, kill -HUP PID
	signal(SIGHUP, handle_sg);

	cGetTime t;
	stop=false;
	sem_init(&l, 0, 1);//locker
	sem_init(&f, 0, 0);
	sem_init(&e, 0, INT_MAX);
	thread c1(consumer);
	thread p1(producer);
	thread c2(consumer);

	while(true && !stop)
		this_thread::sleep_for(100ms);

	c1.join();
	p1.join();
	c2.join();

	cout<< "main exit" <<endl;
	return 0;
}

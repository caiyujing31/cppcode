#include "Pool.hpp"

WorkerPool::Task::Task(const std::function<void()>& handler,int task_id)
: m_handler(handler)
, m_task_id(task_id)
, m_time_created(chrono::high_resolution_clock::now())
, m_time_processed()
{
}

WorkerPool::Task::~Task()
{
}

void WorkerPool::Task::operator()()
{
  m_time_processed = chrono::high_resolution_clock::now();
  return m_handler();
}

chrono::nanoseconds WorkerPool::Task::get_task_handle_delay()
{
  return m_time_processed - m_time_created;
}


WorkerPool::Worker::Worker(
    const std::string& name,
    WorkerPool& worker_pool)
: m_name(name)
, m_worker_pool(worker_pool)
, m_shutdown_flag(false)
, m_future_stop()
{
}

WorkerPool::Worker::~Worker()
{
}

bool WorkerPool::Worker::start()
{
  std::promise<bool> promise_start;
  std::future<bool> future_start = promise_start.get_future();

  m_future_stop = std::async(
      std::launch::async,
      [this, &promise_start]()
      {
        promise_start.set_value(true);

        this->process_tasks(); 

        return true;
      }
    );
  future_start.wait();
  return true;
}

bool WorkerPool::Worker::stop()
{
  m_shutdown_flag = true;
  m_worker_pool.m_cv.notify_all();
  //worker is friend of WorkerPool, so can access m_cv directly! wakeUp everybody in below func, for individual thread. when they wake up they will continue and found (m_shutdonw_flag) then return
  if (m_future_stop.valid())
  {
    m_future_stop.wait(); // wait till this->process_tasks() is finished by the m_shutdown_flag! and return true value to set this future value;
  }

  return true;
}

void WorkerPool::Worker::process_tasks()
{
  while (!m_shutdown_flag)
  {
    //get task
    Task::ptr task;
    {
      std::unique_lock<std::mutex> lock(m_worker_pool.m_mutex);
      m_worker_pool.m_cv.wait( //this is the only place got the wait
          lock,
          [this, &task]()
          {
            task = m_worker_pool.get_task();
            return (task != nullptr || this->m_shutdown_flag); 
            //when worker::stop called, the flag is set to true, it can jump out the wait() loop, and proceed to return!!!!
            //so those right now is in the wait(), will return/stop from here.
          }
        );
    }

    if (m_shutdown_flag)
      return;
    {
      stringstream ss;
      ss<<"thread::id()"<< this_thread::get_id()<<" processing task id:" << (*task).getTask_id();
    }
    //for those thread is busy procesing, then it will return/stop form the outter while loop when check the flag 
    //after the task is finished.
    (*task)();
  }
}

WorkerPool::WorkerPool(
    const std::string& name,
    const size_t num_workers)
: m_name(name)
, m_workers()
, m_mutex()
, m_cv()
, m_tasks()
{
  // Reserve space for workers.
  m_workers.reserve(num_workers);

  // Create the workers
  for (int i = 0; i < num_workers; ++i)
  {
    m_workers.emplace_back(std::make_unique<Worker>(
        name + "_" + std::to_string(i),
        *this
      ));
  }
}

/// Destructor.
WorkerPool::~WorkerPool()
{
}

const std::string& WorkerPool::get_name( ) const
{
  return m_name;
}

/// Starts the WorkerPool.
bool WorkerPool::start()
{
  bool success = true;  
  for (auto& worker : m_workers){
     success &=  worker->start();  
  }
  return success;
}

/// Stops the WorkerPool.
bool WorkerPool::stop()
{
  bool success = true;
  for (auto& worker : m_workers)
    success &= worker->stop();
  return success;
}

void WorkerPool::post(const Task::ptr& task)
{
  if (!task)
    return;
  {
    std::lock_guard<std::mutex> lock(m_mutex);
    m_tasks.push_back(task);
  }
  m_cv.notify_one();
}

void WorkerPool::signal()
{
  m_cv.notify_one();
}

WorkerPool::Task::ptr WorkerPool::get_task()
{
  if (m_tasks.empty())
    return nullptr;

  Task::ptr task = m_tasks.front(); 
  m_tasks.pop_front();

  return task;
}
bool WorkerPool::Worker::isTasksEmpty(){
  return m_worker_pool.m_tasks.size()==0;
}
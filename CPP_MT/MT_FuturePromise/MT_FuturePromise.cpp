// MT6_Future.cpp : Defines the entry point for the console application.
//

#include "getExecutionTime.h"

mutex mu1;
condition_variable cv1;

void factorial(int N, int& res){
	unique_lock<mutex> locker(mu1);
	res = 1;
	for (int i = N; i > 1; i--){
		res *= i;
	}
	locker.unlock();
	cv1.notify_one();
}

double factorial2(int N){
	int res = 1;
	for (int i = N; i > 1; i--){
		res *= i;
	}
	return (double)res;
}

int factorial3(future<int>& f){
	int res = 1;
	int N = f.get();
	for (int i = N; i > 1; i--){
		res *= i;
	}
	return res;
}

void doFirstAction(promise<string>& p1, string& s1) {
    auto pos = s1.find(",");
    s1.erase(pos,1);
	this_thread::sleep_for(100ms); //producer,takes 100msec.
    p1.set_value(s1);
}

void doSecondAction(future<string>& fp) {
    string s = fp.get(); //been blocked here.
    cout << s << endl;
    for (auto& a : s)
        a = toupper(a);
    cout << s<< endl;
}

int main(int argc, char* argv[])
{
	{
		int res = 0;
		int N = 8;
		thread t1(factorial, N, ref(res)); 
		//if pass in res without ref(),even though func signature got &, it still treated by value.
		t1.detach();

		unique_lock<mutex> locker(mu1);
		cv1.wait(locker, [&](){return res != 0; });//prevent spurious wakeup;
		locker.unlock();
	}

	{
		int N = 8;
		future<double> f = std::async(std::launch::async, factorial2, N);
		cout << f.get() << endl;////future is child thread return value mechanison, it is blocking call.
	}

	{
		promise<int> p;
		future<int> fp = p.get_future();//use shared_future, if need to broadcast. this future is part of promise in the main thread.
		future<int> f2 = std::async(std::launch::async, factorial3, ref(fp)); //f2 to get return value of the thread func...
		this_thread::sleep_for(500ms);
		p.set_value(7); //thread .get() blocking call will be cleared,and continue to run.
		cout << f2.get() << endl;
	}

    {
        promise<string> p;
        future<string> fp = p.get_future();
        string s1 = "Hello, world";
        thread t1(doFirstAction, ref(p), ref(s1));
        thread t2(doSecondAction, ref(fp));
        t1.join();
        t2.join();
    }

	return 0;
}


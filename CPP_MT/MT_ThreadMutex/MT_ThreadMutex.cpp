// MT.cpp : Defines the entry point for the console application.
//
#include "getExecutionTime.h"

mutex mu1;
void shared_print(string callerFunc, int i){
	mu1.lock();
	cout << "from " << callerFunc << ":" << i << endl;
	mu1.unlock();
}

void func1(void){
	for (int i = 0; i < 150; i++)
		shared_print("func1", i);
}

static std::exception_ptr eP = nullptr;

void func2Expt(void) {
    try {
        this_thread::sleep_for(2s);
        throw runtime_error("catch me at Main");
    }
    catch(...){
        eP = current_exception();
    }
}

void CallBack1(int* p){

	this_thread::sleep_for(1000ms);
    cout << "CallBack1 p = " << p << " with value of = " << *p << endl;
	int i = *p;
}

void startNewThread(void){
	int p = 10;
	thread t(CallBack1, &p);
	t.detach(); 
	//the p could be destoryed before CallBack1() use it, 
	//make sure use t.join() to keep p alive till the CallBack1 finished  
	cout << p;
} 

void CallBack2(int* p){
	this_thread::sleep_for(1000ms);
	int i = *p;// random number, not 20.
	*p = 30;
}

void startNewThread2(){
	int* p = new int(20);
	thread t(CallBack2, p);
	t.detach();
	this_thread::sleep_for(500ms);
	delete p; //main will destroy the p faster than call back finished.
}

int main(int argc, char* argv[]){
	thread t1(func1);	
	//it will immediately run upon instantiation 
	//if main finished earlier, it will have NO chance execute its own code.
	//so it is undeterministic when two/more threads running independantly; 
	//either join() or detach() can only be called once, and only one of them. 
	//once detached, it is not joinabled, once joined, not detachable!!
	//t1.detach();

	for (int i = 0; i > -100; i--)
		shared_print("main", i);

	t1.join();//without join() or below wait, the excution will crash, bcos the thread takes longer time to finish than the main;

	startNewThread();
	startNewThread2();

    thread t2(func2Expt);
    t2.join();
    if (eP != nullptr) {
        try {
            rethrow_exception(eP);
        }
        catch (const exception& ePm) {
            cout << "exception is:" << ePm.what() << endl;
        }
    }
	return 0;
}
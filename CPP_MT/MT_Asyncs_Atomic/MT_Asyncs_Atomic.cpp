// MT7.cpp : Defines the entry point for the console application.

#include "getExecutionTime.h"
atomic<bool> ready(false);
atomic<bool> winner(false);
void countlm(int id)
{
	while (!ready) 
    {
        //cout << "waiting for start" << endl;
    } //wait for start signal.
	
    for (int i = 0; i < 100000; ++i) 
    {
    
    }
	
    if (!winner.exchange(true)) //if winner is false, then set it to true, and declared as winner in the {}
		cout << "thread with ids:" << id << " is winner" << endl;
}

static std::mutex revStrMutex;
static void flipString(vector<string>* revStrs, string s) {

	this_thread::sleep_for(2s);
    reverse(s.begin(), s.end());

	std::lock_guard<std::mutex> lock(revStrMutex);
	revStrs->push_back(s);
}

static std::mutex newFilesMutex;
static int processFiles(vector<char>* newFiles, char c) {
	int ret = 0;
	for (int i = 0; i < c - '@'; i++){
		ret++;
		this_thread::sleep_for(1s);
	}

	std::lock_guard<std::mutex> lock(newFilesMutex);
	newFiles->push_back(c);
	return ret;
}

double comp(double* beg, double* end, double init) {
    while (beg != end) 
    {
        *beg += 0.001;
        init += (*beg*0.999);
        beg++;
		this_thread::sleep_for(1ms);
    }
    return init;
}


void taskTakeLongTime(string s)
{
    for (int i = 0; i < s.length(); i++)
        this_thread::sleep_for(1s);
    cout << s << ": " << this_thread::get_id() << endl;
}

int main(){
    {
        // it will take 7 second , 4+2+1; if NOT assinged to the auto varaible.
        // the returned future is temporary variable, and need to destroyed (immediately, not at closing }).
        // and it requires the thread finished running,so essentially become in sequence..
        {
            cGetTime t;
            std::async(std::launch::async, taskTakeLongTime, "----");
            std::async(std::launch::async, taskTakeLongTime, "--");
            std::async(std::launch::async, taskTakeLongTime, "-");
        }

        //with below "auto a2/3 =", it will allowed to put the value temporary in the varible, and destroyed at closing };
        //so the three threads running in parallel. and it only take the 4 seconds.
        {
            cGetTime t;
            auto a = std::async(std::launch::async, taskTakeLongTime, "----");
            auto a2 = std::async(std::launch::async, taskTakeLongTime, "--");
            auto a3 = std::async(std::launch::async, taskTakeLongTime, "-");
        }
    }

	{
		cGetTime t;
		vector<string> revStrs;
		vector<future<void> > v;
		v.push_back(std::async(std::launch::async, flipString, &revStrs, string("string Number 1")));
		v.push_back(std::async(std::launch::async, flipString, &revStrs, string("string Number 2")));
		v.push_back(std::async(std::launch::async, flipString, &revStrs, string("string Number 3")));

		//the follwoing code will make sure the future already been returned by all the threads. so the revStrs got filled 
		for (auto& e : v)
		    e.get();
		print_info(revStrs, "reversed string");
	}

	{
		cGetTime t;
		vector<char> newFiles;
		vector<future<int> > futures;
		vector<char> oldFiles = { 'A','E','C' };
		for (char& c : oldFiles)
			futures.push_back(std::async(std::launch::async, processFiles, &newFiles, c));

		for (auto& f : futures)
			cout << "run time is: " << f.get() <<endl;
	}

	{
		cGetTime t;
		vector<double> v2(1000);
		generate_vector(v2);
		double* l = &v2[0];
		int siz = v2.size();
		////method1; 1sec
		//double res = comp(l, l + siz, 0.0);

		////method2, 0.5sec
		double sum1 = 0;
		double sum2 = 0;
		thread t1(comp, l, l + siz / 2, ref(sum1));
		thread t2(comp, l + siz / 2, l + siz, ref(sum2));
		t1.join();
		t2.join();
		double res = sum1 + sum2;

		//method3, 0.5sec
		//future<double> f1 = async(std::launch::async, comp, l, l + siz /2, 0.0);
		//future<double> f2 = async(std::launch::async, comp, l + siz / 2, l + siz, 0.0);
		//double res = f1.get() + f2.get();		
	}

    {
        vector<thread> v2;
        for (int i = 0; i < 10; i++)
            v2.push_back(thread(countlm, i));

        ready = true;
        for (auto& th : v2)
            th.join();
    }
    return 0;
}